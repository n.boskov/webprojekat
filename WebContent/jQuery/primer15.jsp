<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
<script src="jquery-1.11.0.js">
</script>
<script>
$(document).ready(function(){
    $("button#b_1").click(function(){
        $("div#d_1").load("${pageContext.request.contextPath}/GetHTML");
    });   
});
</script>
</head>

<body>
<h1>AJAX load</h1>
<div id="d_1">    
</div>
<button id="b_1">učitaj</button>
</body>
</html>
