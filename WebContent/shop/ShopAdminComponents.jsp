<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="beans.shop.ShopUser"%>
<%@page import="beans.shop.ShopUser.Role"%>
<jsp:useBean id="loggedUser" class="beans.shop.ShopUser" scope="session"/>
<%
//DEBUG uncomment this
if (loggedUser != null && loggedUser.getRole() == Role.user) {
loggedUser = (ShopUser) new ShopUser();
}

if (request.getSession().getAttribute("loggedUser") == null || loggedUser.getUserName().equals(""))
	response.sendRedirect("ShopAdminLogin.jsp");
%>
<jsp:useBean id="imagePathOnUser" class="java.lang.String" scope="session"/>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin components</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script src="../jQuery/jquery-1.11.0.js"></script>

	<!-- Bootstrap core CSS and JS-->
	<link href="../bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="../bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

	<!-- Dependencies for calendar-for-bootstrap -->
	<link href="../jquery-ui-1.11.1.custom/jquery-ui.min.css" rel="stylesheet">
	<script src="../jquery-ui-1.11.1.custom/jquery-ui.js"></script>

	<!-- Custom styles for this template -->
	<link href="css/navbar-top-fixed.css" rel="stylesheet">

	<!-- Aditional css -->
	<link href="css/areas.css" rel="stylesheet">

	<!-- CRUD operations by Ajax -->
	<script src="js/operations.js"></script>

	<!-- New script -->
	<script type="text/javascript">
		window.contextPath = "${pageContext.request.contextPath}";

		$(document).ready(function() {

			//username in navbar adjust
			if ($('div#userBean').text().trim() == '') {
				$('li#loggedUser').addClass('hidden');
				$('li#logOutInNavbar').addClass('hidden');
			} else {
				$('li#loggedUser').removeClass('hidden');
				$('li#loggedUser a b').append(" " + $('div#userBean').text().trim());
				$('li#logOutInNavbar').removeClass('hidden');
			}

			$('div#componentTable tbody').on('click', '.delete', function(event){
				var decison = confirm("Are you sure you want to delete component: " + $(this).attr("value") + " ?");
				if (decison == true){
					$.ajax({
						url: window.contextPath + "/StoreServlet" + '?' + $.param({"ajaxId": '2', "toDelete" : $(this).attr("value")}),
						type: "delete",
						success: function(data){
							populateComponentsTable();
						},    
						error: function(status) { 
						}
					});
				}
			});

			$('div#componentTable tbody').on('click', '.details', function(){
				$('#editModal.modal.in').modal('show'); //detailsModal
				hideErrorsEditModal();
				populateComponentDetails($(this).attr("value"));

				// amount and price validation
				$('div#editModal div#itemAmount').off('input');
				$('div#editModal div#itemAmount input').on('input', function(){
					validateAmountEditModal($(this).val());
				});

				$('div#editModal div#itemPrice').off('input');
				$('div#editModal div#itemPrice input').on('input', function(){
					validatePriceEditModal($(this).val());
				});
				// END amount and price validation

				$('div#searchCategories').off('click', '.btn');
				$('div#searchCategories').on('click', '.btn', function(event) {
					event.preventDefault(); // otherwise closing modal is performed
					searchCategoriesByName($('div#editModal #searchQuery').val());
				});
				$('div#searchCategories #searchQuery').off('input');
				$('div#searchCategories #searchQuery').on('input', function(){
					searchCategoriesByName($(this).val());
				});

				$('div#editModal div.modal-footer').off('click', '.detailsCancel');
				$('div#editModal div.modal-footer').on('click', '.detailsCancel', function(){
					$('#editModal.modal.in').modal('hide');
				});

				$('div#editModal div.modal-footer').off('click', '.detailsEdit');
				$('div#editModal div.modal-footer').on('click', '.detailsEdit', function(){
					processFormEditComponent();
					$('#editModal.modal.in').modal('hide');
				});
			});

$('div#showAllComponentsTotalSearch').on('click', '.btn', function(){
	$('div#componentTable table tbody tr').each(function (index, element) {
		$(this).show();
	});
	clearComponentSearchArea();
});

$('div#totalSearchName').on('input', ':text', function(){
	totalSearchComponents($(this).val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());

});

$('div#totalSearchDescription').on('input', ':text', function(){
	totalSearchComponents($('div#totalSearchName input').val(), $(this).val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());
});

$('div#totalSearchPriceRange > select').on('change', function(){
	totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $(this).val(), $('div#totalSearchAmountRange select').val());
});

$('div#totalSearchAmountRange > select').on('change', function(){
	totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $(this).val());
});

$('div#ChoooseCategoryTableTotalSearch tbody').on('change', ':checkbox', function(){
	totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());
});

$('div#totalSearchSearchCategory #searchQuery').on('input', function(){
	searchCategoriesByNameTotalSearch($(this).val());
});

$('div.searchArea form.form-horizontal div#addComponent').on('click', '.addNewItem', function(e){

				e.preventDefault(); // presume - this button(in form) have predefined behaviour wich is in conflict with modal default behaviour	 	
				$('#addModal.modal.in').modal('show');

				clearFieldsAddComponent();
				populateChooseCategoryTableAddComponent();

				// amount and price validation
				$('div#addModal div#itemAmount').off('input');
				$('div#addModal div#itemAmount input').on('input', function(){
					validateAmountAddModal($(this).val());
				});

				$('div#addModal div#itemPrice').off('input');
				$('div#addModal div#itemPrice input').on('input', function(){
					validatePriceAddModal($(this).val());
				});
				// END amount and price validation

				$('div#addModal div#searchCategories').off('click', '.btn');
				$('div#addModal div#searchCategories').on('click', '.btn', function(event) {
					event.preventDefault(); // otherwise closing modal is performed
					searchCategoriesByNameAddComponent($('div#addModal #searchQuery').val());
				});
				$('div#addModal div#searchCategories #searchQuery').off('input');
				$('div#addModal div#searchCategories #searchQuery').on('input', function(){
					searchCategoriesByNameAddComponent($(this).val());
				});

				$('div#addModal div.modal-footer').off('click', '.addCancel');
				$('div#addModal div.modal-footer').on('click', '.addCancel', function(){
					$('div#addModal.modal.in').modal('hide');
				});

				$('div#addModal div.modal-footer').off('click', '.addAdd');
				$('div#addModal div.modal-footer').on('click', '.addAdd', function(){
					processFormAddComponent();
					$('div#addModal.modal.in').modal('hide');
				});

				// validation of form

				$("div#addModal div#itemName").on('input', ':text', function(){
					validateInputAddComponentNameFieldReal();
				});

				//  END validation of form
			});

		// report modal
		$('li.chartLi a.reportButton').on('click', function(){
			$('div#reportModal.modal').modal('show');

			$('div#reportModal.modal input#startDateStandard').datepicker({ });
			$('div#reportModal.modal input#endDateStandard').datepicker({ });
			$('div#reportModal.modal input#startDateCategory').datepicker({ });
			$('div#reportModal.modal input#endDateCategory').datepicker({ });

			//fill category sellect
			fillCategoriesSelectInReportModal();

			$('div#reportModal div.modal-footer').off('click', '.reportCancel');
			$('div#reportModal div.modal-footer').on('click', '.reportCancel', function(){
				$('div#reportModal.modal.in').modal('hide');
			});

			$('div#reportModal').off('click', 'button.generateStandardReport');
			$('div#reportModal').on('click', 'button.generateStandardReport', function(event){
				event.preventDefault();
				var startDate = $('div#reportModal.modal input#startDateStandard').datepicker("getDate");
				var endDate = $('div#reportModal.modal input#endDateStandard').datepicker("getDate");
				var sStartDate = startDate.getMonth()+1 + '-' + startDate.getDate() + '-' + startDate.getFullYear();
				var sEndDate = endDate.getMonth()+1 + '-' + endDate.getDate() + '-' + endDate.getFullYear();
				window.open('reports/standardReport.jsp?startDate=' + sStartDate + '&endDate=' + sEndDate + '&category=');
			});

			$('div#reportModal').off('click', 'button.generateCategoryReport');
			$('div#reportModal').on('click', 'button.generateCategoryReport', function(event){
				event.preventDefault();
				var startDate = $('div#reportModal.modal input#startDateCategory').datepicker("getDate");
				var endDate = $('div#reportModal.modal input#endDateCategory').datepicker("getDate");
				var sStartDate = startDate.getMonth()+1 + '-' + startDate.getDate() + '-' + startDate.getFullYear();
				var sEndDate = endDate.getMonth()+1 + '-' + endDate.getDate() + '-' + endDate.getFullYear();
				var sCategory = $('div#reportModal.modal div#categorySelectDiv select#categorySelect').val();
				window.open('reports/categoryReport.jsp?startDate=' + sStartDate + '&endDate=' + sEndDate + '&category=' + sCategory);
			});
		});
});


</script>

<style type="text/css">
	/* for fancy image choose button */
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}
	
	#itemDescription textarea {
		height: 100%;
		width: 100%;
	}
	#itemComponents textarea {
		height: 100%;
		width: 100%;
	}

	li.chartLi {
		background-color: #D3D3D0;
	}

	div#verticalAlignIt {
		vertical-align: bottom;
	}

</style>

</head>
<body onload="populateComponentsTable(); populateChooseCategoryTableTotalSearch()">
	<!-- Navigation bar -->
	<div class="navbar-wrapper">
		<div class="container">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><i><b>Small Shop</b></i></a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li><a href="ShopAdminIndex.jsp">Device</a></li>
							<li class="active"><a href="">Component</a></li>
							<li><a href="ShopAdminCategories.jsp">Category</a></li>
							<li class="chartLi"><a class="btn btn-link reportButton" data-toggle='modal'><b>Generate report</b></a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="" id="loggedUser">
								<a>Logged in as:<b></b></a>
								<div id="userBean" hidden><%= loggedUser.getUserName() %></div>
							</li>
							<li class="" id="logOutInNavbar">
								<a href="../ShopLoginServlet?loggedUser=<%= loggedUser.getUserName() %>&logoff=logoff">Log out</a>
							</li>
						</ul>
					</div><!--/.nav-collapse -->

				</div>
			</div>
		</div> <!-- container end -->
	</div> <!-- /Navigation bar -->

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3 col-lg-2">
				<div class="searchArea">
					<!-- Search form total search -->
					<form class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-9" id="addComponent"> 
								<button class="btn btn-primary addNewItem" data-toggle='modal' data-target='#addModal'>Add</button>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-9">Search below</label>
						</div>
						<div class="form-group">
							<div class="col-xs-2" id="showAllComponentsTotalSearch"> 
								<button class="btn btn-mini">Show all</button>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-10" id="totalSearchName">
								<input type="text" class="span2" placeholder='component name'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="totalSearchDescription">
								<input type="text" class="span2" placeholder='component description'>
							</div>
						</div>
						<div class="form-group">
							<label for="priceRange" class="control-label col-xs-2">Price range</label>
							<div class="col-xs-10" id="totalSearchPriceRange">
								<select class="combobox input-large" name="normal">
									<option value="">no selection</option>
									<option value="r1">0 - 300 &#36;</option>
									<option value="r2">300 - 600 &#36;</option>
									<option value="r3">600 - 900 &#36;</option>
									<option value="r4">900 - 1200 &#36;</option>
									<option value="r5">1200+ &#36;</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="amountRange" class="control-label col-xs-2">Amount range</label>
							<div class="col-xs-10" id="totalSearchAmountRange">
								<select class="combobox input-large" name="normal">
									<option value="">no selection</option>
									<option value="r1">0 - 10</option>
									<option value="r2">10 - 20</option>
									<option value="r3">20 - 50</option>
									<option value="r4">50+</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-9">Category</label>
						</div>
						<div class="form-group">
							<div class="col-xs-9" id="totalSearchSearchCategory">
								<input id='searchQuery' type="text" class="span2" placeholder='search name...'>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseCategoryTableTotalSearch">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form> <!-- /Search form total search -->
				</div>
			</div>
			<div class="col-sm-6 col-md-9 col-lg-10">
				<div class="viewArea">
					<div id="componentTable">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Price(&#36;)</th>
									<th>Amount</th>
									<th>Category</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div> <!-- /container -->

	<!-- Modal dialog for details -->
	<div id="editModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-10" id="itemName">
								<p class="form-control-static"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="itemAmount" class="control-label col-xs-2">Amount</label>
							<div class="col-xs-10" id="itemAmount">
								<input type="number" placeholder='0'>
							</div>
							<span id='AmountError' class='error' hidden='true'><p>Must be number</p></span>
						</div>
						<div class="form-group">
							<label for="itemPrice" class="control-label col-xs-2">Price(&#36;)</label>
							<div class="col-xs-10" id="itemPrice">
								<input type="number" placeholder='0'>
							</div>
							<span id='PriceError' class='error' hidden='true'><p>Must be price</p></span>
						</div>
						<div class="form-group">
							<label for="itemLink" class="control-label col-xs-2">Link</label>
							<div class="col-xs-10" id="itemLink">
								<input type="text" placeholder='unknown'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="itemDescription">
								<textarea rows="10" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="image" class="control-label col-xs-2">Image</label>
							<div class="col-xs-8" id="itemImage">
								<img src="" class="img-rounded img-responsive" alt="Rounded Image">
							</div>
							<span class="col-xs-2 btn btn-default btn-file">
								Browse <input type="file" id="componentimageInput" accept="image/*">
							</span>
							<div hidden id="componentimageBase64"> </div>
							<div hidden id="componentimageExtension"> </div>
							<script type="text/javascript">
								function readImageEditModal(input) {
									if ( input.files && input.files[0] ) {
										var FR= new FileReader();
										FR.onload = function(e) {
											$('div#editModal div#itemImage > img').attr( "src", e.target.result );
											$('div#editModal #componentimageBase64').html( e.target.result );
											$('div#editModal #componentimageExtension').html($('div#editModal #componentimageBase64').html().split('image/')[1].split(';base64')[0]);
											$('div#editModal #componentimageBase64').html( $('div#editModal #componentimageBase64').html().split('base64,')[1] );
										};       
										FR.readAsDataURL( input.files[0] );
									}
								}
								$("div#editModal #componentimageInput").change(function(){
									readImageEditModal( this );
								});
							</script>
						</div>
						<div class="form-group">
							<label for="itemCategory" class="control-label col-xs-2">Category</label>
							<div class="col-xs-10" id="itemCategory">
								<input type="text" placeholder='no category' readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="searchCategories" class="control-label col-xs-2">Search</label>
							<div class="col-xs-10" id="searchCategories">
								<div class="input-append">
									<input id='searchQuery' type="text" class="span2" placeholder='category name'>
									<button class="btn btn-info btn-small">Search...</button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseCategoriesTable">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger detailsCancel">Cancel</button>
					<button class="btn btn-success detailsEdit">Edit</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for details -->


	<!-- Modal dialog for add device -->
	<div id="addModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"><b>Add new component</b></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-8" id="itemName">
								<input type="text" class="span2" placeholder='component name'>
							</div>
							<span id='nameError' class='error' hidden='true'><p>Name already exists</p></span>
						</div>
						<div class="form-group">
							<label for="itemAmount" class="control-label col-xs-2">Amount</label>
							<div class="col-xs-10" id="itemAmount">
								<input type="number" placeholder='0'>
							</div>
							<span id='AmountError' class='error' hidden='true'><p>Must be number</p></span>
						</div>
						<div class="form-group">
							<label for="itemPrice" class="control-label col-xs-2">Price (&#36;)</label>
							<div class="col-xs-10" id="itemPrice">
								<input type="number" placeholder='0'>
							</div>
							<span id='PriceError' class='error' hidden='true'><p>Must be price</p></span>
						</div>
						<div class="form-group">
							<label for="itemLink" class="control-label col-xs-2">Link</label>
							<div class="col-xs-10" id="itemLink">
								<input type="text" placeholder='unknown'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="itemDescription">
								<textarea rows="10" placeholder='device description'></textarea>
							</div>
						</div>
						<!-- slika -->
						<div class="form-group">
							<label for="image" class="control-label col-xs-2">Image</label>
							<div class="col-xs-8" id="itemImage">
								<img src="" class="img-rounded img-responsive" alt="Rounded Image">
							</div>
							<span class="col-xs-2 btn btn-default btn-file">
								Browse <input type="file" id="componentimageInput" accept="image/*">
							</span>
							<div hidden id="componentimageBase64"> </div>
							<div hidden id="componentimageExtension"> </div>
							<script type="text/javascript">
								function readImageAddModal(input) {
									if ( input.files && input.files[0] ) {
										var FR= new FileReader();
										FR.onload = function(e) {
											$('div#addModal div#itemImage > img').attr( "src", e.target.result );
											$('div#addModal #componentimageBase64').html( e.target.result );
											$('div#addModal #componentimageExtension').html($('div#addModal #componentimageBase64').html().split('image/')[1].split(';base64')[0]);
											$('div#addModal #componentimageBase64').html( $('div#addModal #componentimageBase64').html().split('base64,')[1] );
										};       
										FR.readAsDataURL( input.files[0] );
									}
								}

								$("div#addModal #componentimageInput").change(function(){
									readImageAddModal( this );
								});
							</script>
						</div>
						<!-- slika -->
						<div class="form-group">
							<label for="categories" class="control-label col-xs-6">Categories</label>
						</div>
						<div class="form-group">
							<label for="searchCategories" class="control-label col-xs-2">Search</label>
							<div class="col-xs-10" id="searchCategories">
								<div class="input-append">
									<input id='searchQuery' type="text" class="span2" placeholder='category name'>
									<button class="btn btn-info btn-small">Search...</button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseCategoriesTable">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger addCancel">Cancel</button>
					<button class="btn btn-success addAdd" disabled='true'>Add</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for add device -->

	<!-- Modal dialog for generate report -->
	<div id="reportModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader">Generating report</h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="standardReport" class="control-label col-xs-6">Standart report</label>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">From:</label>
							<div class="col-xs-8" id="startDateStandardDiv">
								<input id='startDateStandard' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">To:</label>
							<div class="col-xs-8" id="endDateStandardDiv">
								<input id='endDateStandard' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-6" id="generateStandardReport">
								<button class="btn btn-info btn-small generateStandardReport">Generate</button>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label for="categoryReport" class="control-label col-xs-6">Category report</label>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">From:</label>
							<div class="col-xs-8" id="startDateCategoryDiv">
								<input id='startDateCategory' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">To:</label>
							<div class="col-xs-8" id="endDateCategoryDiv">
								<input id='endDateCategory' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group" id="verticalAlignIt">
							<label for="" class="control-label col-xs-4">Category:</label>
							<div class="col-xs-8" id="categorySelectDiv" >
								<select id='categorySelect'readonly="true"> </select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-6" id="generateCategoryReport">
								<button class="btn btn-info btn-small generateCategoryReport">Generate</button>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger reportCancel">Cancel</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for generate report -->

</body>
</html>