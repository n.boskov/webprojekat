<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Admin registration</title>

	<script src="../../jQuery/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<!-- Bootstrap Core JavaScript -->
	<script src="../js/bootstrap.min.js"></script>

	<style type="text/css">

		body { padding-top:30px; }
		.form-control { margin-bottom: 10px; }

	</style>

	<script type="text/javascript">

		function validateEmail(email) { 
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		} 

		function validatePhoneNumber(number) {
			var re = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
			return re.test(number);
		}

		function validateEmailonBlur() {
			console.log('DEBUG validira = ' + $('input#email').val());
			if (!validateEmail($('input#email').val())) {
				console.log('pokazi email not valid!');
				$('label#errorLabelEmail').show();
				$('button.submitButton').prop("disabled", true);
			} else {
				console.log('skloni email not valid!');
				$('label#errorLabelEmail').hide();
				$('button.submitButton').prop("disabled", false);
			}
		}

		function validateTelephoneOnBlur() {
			console.log('DEBUG validira telefon = ' + $('input#phone').val());
			if (!validatePhoneNumber($('input#phone').val())) {
				console.log('pokazi phone not valid!');
				$('label#errorLabelTelNumber').show();
				$('button.submitButton').prop("disabled", true);
			} else {
				console.log('skloni phone not valid!');
				$('label#errorLabelTelNumber').hide();
				$('button.submitButton').prop("disabled", false);
			}
		}

		$(document).ready(function(){
			$('input#reEmail').on('input', function() {
				if ($('input#email').val() != $(this).val()) {
					console.log('pokazi!');
					$('label#errorLabel').show();
					$('button.submitButton').prop("disabled", true);
				} else {
					console.log('sakrij!');
					$('label#errorLabel').hide();
					$('button.submitButton').prop("disabled", false);
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4 well col-lg-4 col-lg-offset-4 well well-sm">
				<legend><a href="http://www.jquery2dotnet.com"><i class="glyphicon glyphicon-globe"></i></a> Sign up!</legend>
				<form action="../../ShopLoginServlet" method="post" class="form" role="form">
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<input class="form-control" name="firstname" placeholder="First Name" type="text"
							required autofocus />
						</div>
						<div class="col-xs-6 col-md-6">
							<input class="form-control" name="lastname" placeholder="Last Name" type="text" required />
						</div>
					</div>
					<input name="role" type="text" value="admin" hidden/>
					<input class="form-control" name="youremail" id="email" placeholder="Your Email" type="email" onblur="validateEmailonBlur()" />
					<label id="errorLabelEmail" hidden="true"><span style="color:red">Email not valid</span></label>
					<input class="form-control" name="reenteremail" id="reEmail" placeholder="Re-enter Email" type="email" />
					<label id="errorLabel" hidden="true"><span style="color:red">Email don't match</span></label>
					<input class="form-control" name="telnumber" id="phone" placeholder="Telephone number" type="text" onblur="validateTelephoneOnBlur()" />
					<label id="errorLabelTelNumber" hidden="true"><span style="color:red">Bad telephone number use : +?{2}-{3}[-]{5}</span></label>
					<input class="form-control" name="username" placeholder="Username" type="text" />
					<input class="form-control" name="password" placeholder="Password" type="password" />

					<br />
					<button class="btn btn-lg btn-primary btn-block submitButton" type="submit">
						Sign up</button>
					</form>
				</div>
			</div>
		</div>
	</body>
	</html>