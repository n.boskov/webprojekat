<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="beans.shop.ShopUser"%>
<%@page import="beans.shop.ShopUser.Role"%>
<jsp:useBean id="loggedUser" class="beans.shop.ShopUser" scope="session"/>
<%
//DEBUG uncomment this
if (loggedUser != null && loggedUser.getRole() == Role.user) {
loggedUser = (ShopUser) new ShopUser();
}

if (request.getSession().getAttribute("loggedUser") == null || loggedUser.getUserName().equals(""))
	response.sendRedirect("../ShopAdminLogin.jsp");
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Category report</title>

	<script src="../../jQuery/jquery-1.11.0.js"></script>
	<script src="../js/categoryReportOperations.js"></script>

	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td {
			padding: 5px;
		}
		th {
			text-align: left;
		}
	</style>

	<script type="text/javascript">
		window.contextPath = "${pageContext.request.contextPath}";
	</script>

</head>
<body onload="populate()">
	<p>
		<h3>Category report requested by <b><%= loggedUser.getUserName() %></b> on <i><span id="nowDateTime"></span></i></h3>
		<h4>Period: <span id="startPeriod"></span> to <span id="endPeriod"></span></h4>
		<h4>Category: <span id="selectedCategory"></span></h4>
	</p>
	<table style="">
		<tr>
			<td>#</td>
			<th>Component</th>
			<th>Salary</th>		
		</tr>
		<tr>
		</table>
	</body>
	</html>