function populate() {
	var sStartDate = location.search.split('startDate=')[1].split('&')[0].split('-').join('/').trim(); 
	var sEndDate = location.search.split('endDate=')[1].split('&')[0].split('-').join('/').trim();
	var sCategory = location.search.split('category=')[1].trim();
	var dateReporting = new Date(); 
	console.log('DEBUG sStartDate = ' + sStartDate);
	console.log('DEBUG sEndDate = ' + sEndDate);

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "100", startDate : sStartDate, endDate: sEndDate, category: sCategory},
		dataType: "json",
		success: function(data) {
			$('span#nowDateTime').html('');
			$('span#startPeriod').html('');
			$('span#endPeriod').html('');
			$('span#selectedCategory').html('');
			$('span#nowDateTime').html(dateReporting.getMonth()+1 + '/' + dateReporting.getDate() + '/' + dateReporting.getFullYear());
			$('span#startPeriod').html(sStartDate);
			$('span#endPeriod').html(sEndDate);
			$('span#selectedCategory').html(sCategory);
			$('table tr.tableData').remove();

			var soldUnits = 0;
			var componentSalary = {};
			for (var i = 0; i<data.length; i++) {
				console.log('RACUN ' + i + '. :');
				for (var component in data[i].components) {
					console.log('DEBUG OVO GORE');
					var componentCategoryArray = extractComponentCategory(component);
					var componentName = component.split('ShopComponent [name=')[1].split(', price=')[0].trim();
					var sComponentPrice = component.split(' price=')[1].split(', amount=')[0].trim();
					console.log('	KOMPONENTA ' + componentName);
					console.log('	DEBUG sComponentPrice = ' + sComponentPrice);
					console.log('	DEBUG componentCategoryArray = ' + componentCategoryArray);

					console.log('$.inArray('+sCategory+', '+componentCategoryArray+') = ' + $.inArray(sCategory, componentCategoryArray));

					if ($.inArray(sCategory, componentCategoryArray) != -1) {
						soldUnits += parseInt(data[i].components[component]);
						if (!(componentName in componentSalary)) {
							componentSalary[componentName] = parseFloat(sComponentPrice) * parseFloat(data[i].components[component]);
						} else if (componentName in componentSalary) {
							componentSalary[componentName] += parseFloat(sComponentPrice) * parseFloat(data[i].components[component]);
						}
					}
				}

				for (var genDev in data[i].generatedDevices) {
					var componentsArray = extracktComponentsFromDevice(genDev);
					console.log('COMPONENTARRAY = ' + componentsArray);
					for (var jj = 0; jj<componentsArray.length; jj++) {
						var component = componentsArray[jj];
						var componentCategoryArray = extractComponentCategory(component);
						var componentName = component.split('ShopComponent [name=')[1].split(', price=')[0].trim();
						var sComponentPrice = component.split(' price=')[1].split(', amount=')[0].trim();
						console.log('	KOMPONENTA ' + componentName);
						console.log('	DEBUG sComponentPrice = ' + sComponentPrice);
						console.log('	DEBUG componentCategoryArray = ' + componentCategoryArray);

						console.log('$.inArray('+sCategory+', '+componentCategoryArray+') = ' + $.inArray(sCategory, componentCategoryArray));

						if ($.inArray(sCategory, componentCategoryArray) != -1) {
							soldUnits += parseInt(data[i].generatedDevices[genDev]);
							if (!(componentName in componentSalary)) {
								componentSalary[componentName] = parseFloat(sComponentPrice) * parseFloat(data[i].generatedDevices[genDev]);
							} else if (componentName in componentSalary) {
								componentSalary[componentName] += parseFloat(sComponentPrice) * parseFloat(data[i].generatedDevices[genDev]);
							}
						}
					}
				}
			}

			var ii = 1;
			var total = 0;
			for (var key in componentSalary) {
				$('table').append("<tr class='tableData'><td>" + ii + "</td><td>" + key + "</td><td>$" + parseFloat(componentSalary[key]).toFixed(2) + "</td></tr>");
				total += parseFloat(componentSalary[key]);
				ii++;
			}

			$('table').append("<tr class='tableData'><td>" + "<b>Sold units</b>" + "</td><td><b>" + soldUnits + "</b></td></tr>");
			$('table').append("<tr class='tableData'><td>" + "<b>Total</b>" + "</td><td><b>$" + total.toFixed(2) + "</b></td></tr>");
		},
		error: function(xhr, status) {
			if (xhr.status == '409') {
				alert("Sorry, your start date and end date are not in the right order. Try again. ");
				window.close();
			}
			console.log('error in generateStandardReport, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}

function extractComponentCategory(sComponent) {
	var retVal = [];
	var base  = sComponent.split(', CategoryNames=[')[1].split(']]')[0].split(',');
	for (var i = 0; i<base.length; i++) {
		if(retVal.indexOf(base[i]) == -1) {
			retVal.push(base[i].trim());
		}
	}
	return retVal;
}

function extracktComponentsFromDevice(sDevice) {
	var sComponentArray = sDevice.substring(sDevice.indexOf("components=") + "components=".length, sDevice.length - 1);
	var componentsArray = sComponentArray.split('ShopComponent [');
	for (var i = 1; i<componentsArray.length; i++) {
		componentsArray[i] = 'ShopComponent [' + componentsArray[i];
		if (componentsArray[i].substring(componentsArray[i].length - 2, componentsArray[i].length) == ', ') {
			componentsArray[i] = componentsArray[i].substring(0, componentsArray[i].length-2);
		}
		if (i == componentsArray.length - 1) {
			componentsArray[i] = componentsArray[i].substring(0, componentsArray[i].length-1);
		}
	}

	var retVal = [];
	for (i=1; i<componentsArray.length; i++) {
		retVal.push(componentsArray[i]);
	}

	return retVal;
}