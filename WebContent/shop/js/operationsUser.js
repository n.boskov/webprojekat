function populateThumbnails() {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
			$("div#thumbnailsArea").find("div.aThumbnail").remove();
			for(var i = 0; i<data.length; i++) {
				$("div#thumbnailsArea").append("<div class='col-sm-4 col-lg-4 col-md-4 aThumbnail'><div class='thumbnail'><img class='img-responsive' src='data:image/jpeg;base64," + data[i].imageFile + "' alt=''><h4 class='pull-right'>$" + data[i].price + "</h4><div class='caption'><h4><a class='btn btn-link details' data-toggle='modal' data-target='#editModal'>" + data[i].name + " </a></h4><p>" + data[i].description + "</p></div><div class='ratings'><p class='pull-right'>15 reviews</p><p><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span></p></div></div></div>");
			}

			// populate carousel

			var arr = []
			if (data.length > 3) {
				while(arr.length < 3){
					var randomnumber=Math.ceil(Math.random()*data.length-1)
					var found=false;
					for(var i=0;i<arr.length;i++){
						if(arr[i]==randomnumber){found=true;break}
					}
					if(!found)arr[arr.length]=randomnumber;
				}
			}

			console.log('DEBUG arr = ' + arr);

			for (var i = 0; i<arr.length; i++) {
				if (i == 0) {
					$('div#carousel-example-generic div#item1 img').prop('src', 'data:image/jpeg;base64,' + data[arr[i]].imageFile);
					$('div#carousel-example-generic div#item1 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item1 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item1 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				} else if (i == 1) {
					$('div#carousel-example-generic div#item2 img').prop('src', 'data:image/jpeg;base64,' + data[arr[i]].imageFile);
					$('div#carousel-example-generic div#item2 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item2 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item2 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				} else if (i == 2) {
					$('div#carousel-example-generic div#item3 img').prop('src', 'data:image/jpeg;base64,' + data[arr[i]].imageFile);
					$('div#carousel-example-generic div#item3 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item3 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item3 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				}
			}

		},
		error: function(status) {
			console.log('DEBUG populateThumbnails ERROR');
		}
	});
}

function populateComponentDetails(name) {
	$('div#editModal div#itemName > p').html(name);
	$('div#editModal #modalHeader').html('Details for ' + '<b>' + name + '</b>');
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "21", toEdit : name},
		dataType: "json",
		success: function(data) {
			$("div#editModal div#itemAmount > p").html(data.amount);
			$("div#editModal div#itemPrice > p").html(data.price);
			$("div#editModal div#itemLink > p > a").html(data.link);
			$("div#editModal div#itemLink > p > a").prop('href', data.link);
			$("div#editModal div#itemDescription > textarea").val(data.description);
            //fill image
            $('div#editModal div#itemImage > img').attr( "src", 'data:image/jpeg;base64,' + data.imageFile );
            // end fill image 

            $('div#editModal div#itemCategory input').val('');
            for (var i = 0; i<data.category.length; i++) {
            	$('div#editModal div#itemCategory > input').val($('div#editModal div#itemCategory > input').val() + data.category[i].name + ";");
            }

            //if there is no logged user disable addToChart
            console.log('DEBUG $(div#userBean).text() = ' + $('div#userBean').text());
            if ($('div#userBean').text() == '') {
            	$('div#editModal div.modal-footer button.detailsAddToChart').prop('disabled', true);
            } else {
            	$('div#editModal div.modal-footer button.detailsAddToChart').prop('disabled', false);
            }
        },
        error: function(status) {
                    //Do something  
                }
            });
}

function populateChooseCategoryTableTotalSearch() {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "3"},
		dataType: "json",
		success: function(data) {
			$("div#ChoooseCategoryTableTotalSearch tbody").find("td").remove();
			for(var i = 0; i<data.length; i++) {
				$("div#ChoooseCategoryTableTotalSearch tbody").append("<tr>" + "<td>" + data[i].name + "</td>"+ "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
			}
		},
		error: function(status) {
                //Do something  
            }
        });
}

function clearComponentSearchArea() {
	$('div#totalSearchName input').val('');
	$('div#totalSearchDescription input').val('');
	$('div#totalSearchPriceRange select>option:eq(0)').prop('selected', true);
	$('div#totalSearchAmountRange select>option:eq(0)').prop('selected', true);
	$("div#ChoooseCategoryTableTotalSearch tbody td input:checked").prop('checked', false);
}

function totalSearchComponents(name, desc, priceRange, amountRange) {
	console.log('DEBUG priceRange = ' + priceRange);
	console.log('DEBUG amountRange = ' + amountRange);

	var choosenCategories = new Array();

	$("div#ChoooseCategoryTableTotalSearch tbody td input:checked").each(function(){
		choosenCategories.push($(this).val());
	});

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
			for (var ii = 0; ii<data.length; ii++) {
				var nameFilter = false;
				var descFilter = false;
				var categoriesFilter = false;
				var priceFilter = false;
				var amountFilter = false;
				var compCtry = new Array();

				for (var i = 0; i<data[ii].category.length; i++) {
					compCtry.push(data[ii].category[i].name);
				}

				if (containsArray(compCtry, choosenCategories)) {
					categoriesFilter = true;
				}

				if ((data[ii].name.toLowerCase().indexOf(name.toLowerCase()) > -1) || name == '') {
					nameFilter = true;
				}

				if ((data[ii].description.toLowerCase().indexOf(desc.toLowerCase()) > -1) || desc == '') {
					descFilter = true;
				}

				switch (priceRange) {
					case 'r1':
					if ( Number(data[ii].price) >= 0 && Number(data[ii].price) < 300){
						priceFilter = true;
					}
					break;
					case 'r2':
					if ( Number(data[ii].price) >= 300 && Number(data[ii].price) < 600) {
						console.log('DEBUG ' + Number(data[ii].price) + '\n a Number(data[ii].price) >= 300 && Number(data[ii].price) < 600 = true');
						priceFilter = true;
					}
					break;
					case 'r3':
					if ( Number(data[ii].price) >= 600 && Number(data[ii].price) < 900) {
						priceFilter = true;
					}
					break;
					case 'r4':
					if ( Number(data[ii].price) >= 900 && Number(data[ii].price) < 1200) {
						priceFilter = true;
					}
					break;
					case 'r5':
					if ( Number(data[ii].price) >= 1200) {
						priceFilter = true;
					}
					break;
					case '':
					console.log('DEBUG OTISAO NA PRAZAN STRING CASE price range');
					priceFilter = true;
					break;
				}

				switch (amountRange) {
					case 'r1':
					if ( Number(data[ii].amount) >= 0 && Number(data[ii].amount) < 10) {
						amountFilter = true;
					}
					break;
					case 'r2':
					if ( Number(data[ii].amount) >= 10 && Number(data[ii].amount) < 20) {
						amountFilter = true;
					}
					break;
					case 'r3':
					if ( Number(data[ii].amount) >= 20 && Number(data[ii].amount) < 50) {
						amountFilter = true;
					}
					break;
					case 'r4':
					if (Number(data[ii].amount) >= 50) {
						amountFilter = true;
					}
					break;
					case '':
					amountFilter = true;
					break;
				}
				console.log('DEBUG za ' + data[ii].name + '\nnameFilter = ' + nameFilter + '\ndescFilter = ' + descFilter + '\namountFilter = ' + amountFilter + '\npriveFilter = ' + priceFilter + '\ncategoriesFilter = ' + categoriesFilter);

				if (nameFilter && descFilter && categoriesFilter && priceFilter && amountFilter) {
					$('div#thumbnailsArea div.aThumbnail').each(function(index, element){
						var $nameA = $(this).find('a.btn.btn-link.details');
						if ($nameA.text().trim().toLowerCase() == data[ii].name.trim().toLowerCase()){ // trim, without it produce problems
							console.log('DEBUG ' + data[ii].name + ' treba show');

							$(this).show();
							return; // DEBUG to speed up iterations
						}
					});
				} else {
					$('div#thumbnailsArea div.aThumbnail').each(function(index, element){
						var $nameA = $(this).find('a.btn.btn-link.details');
						if ($nameA.text().trim().toLowerCase() == data[ii].name.trim().toLowerCase()){ // trim, without it produce problems
							console.log('DEBUG ' + data[ii].name + ' treba hide');

							$(this).hide();
							return; // DEBUG to speed up iterations
						}
					});
				}
			}
		},
		error: function(status) {
			console.log("Error in totalSearchCopmponents: + " + status);  
		}
	});
}

function containsArray(bigArray, smallArray){
	for (var i = 0; i<smallArray.length; i++) {
		if (bigArray.indexOf(smallArray[i]) <= -1) {
			return false;
		}
	}
	return true;
}

function searchCategoriesByNameTotalSearch(searchQuery) {
	$('div#ChoooseCategoryTableTotalSearch table tbody tr').each(function (index, element) {
		if (searchQuery == '') {
			$(this).show();
		} else {
			var $tds = $(this).find('td');
			if (!($tds.eq(0).text().toLowerCase().indexOf(searchQuery.toLowerCase()) > -1)) {
				$(this).hide();
			} else {
				$(this).show();
			}
		}
	});
}

/* Device user operations */

function populateThumbnailsDevices() {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "1"},
		dataType: "json",
		success: function(data) {
			$("div#thumbnailsArea").find("div.aThumbnail").remove();
			for(var i = 0; i<data.length; i++) {
				$("div#thumbnailsArea").append("<div class='col-sm-4 col-lg-4 col-md-4 aThumbnail'><div class='thumbnail'><div class='caption'><h4><a class='btn btn-link details' data-toggle='modal' data-target='#editModal'>" + data[i].name + " </a></h4><p>" + data[i].description + "</p></div><div class='ratings'><p class='pull-right'>15 reviews</p><p><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span></p></div></div></div>");
			}

			// populate carousel

			var arr = []
			if (data.length > 3) {
				while(arr.length < 3){
					var randomnumber=Math.ceil(Math.random()*data.length-1)
					var found=false;
					for(var i=0;i<arr.length;i++){
						if(arr[i]==randomnumber){found=true;break}
					}
					if(!found)arr[arr.length]=randomnumber;
				}
			} else if (data.length == 3) {
				arr = [0, 1, 2];
			}

			console.log('DEBUG arr = ' + arr);

			for (var i = 0; i<arr.length; i++) {
				if (i == 0) {
					$('div#carousel-example-generic div#item1 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item1 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item1 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				} else if (i == 1) {
					$('div#carousel-example-generic div#item2 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item2 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item2 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				} else if (i == 2) {
					$('div#carousel-example-generic div#item3 div.carousel-caption h1').html(data[arr[i]].name);
					$('div#carousel-example-generic div#item3 div.carousel-caption p.lead').html(data[arr[i]].description);
					$('div#carousel-example-generic div#item3 div.carousel-caption a.btn').prop('value', data[arr[i]].name);
				}
			}

		},
		error: function(status) {
			console.log('DEBUG populateThumbnailsDevices ERROR');
		}
	});
}

function populateChooseComponentsTableTotalSearch() {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
            $("div#ChoooseComponentsTableTotalSearch tbody").find("td").remove(); // $("ChoooseComponentsTableTotalSearch tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
            	$("div#ChoooseComponentsTableTotalSearch tbody").append("<tr>" + "<td>" + data[i].name + "</td>"+ "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function populateDeviceDetails(name) {
	$('div#editModal div#itemName > p').html(name);
	$('div#editModal #modalHeader').html('Details for ' + '<b>' + name + '</b>');
    $('div#editModal div#searchComponents input#searchQuery').val(''); // to avoid dirty search field
    $.ajax({
    	url: window.contextPath + "/StoreServlet",
    	type: "get",
    	data: {ajaxId: "11", toEdit : name},
    	dataType: "json",
    	success: function(data) {
                    // fill description
                    $("div#editModal div#itemDescription > textarea").val(data.description);
                    console.log('DEBUG data.components = ' + data.components);
                    populateComponentsInEditModal(data.components);

                    //if there is no logged user disable addToChart
                    console.log('DEBUG $(div#userBean).text() = ' + $('div#userBean').text());
                    if ($('div#userBean').text() == '') {
                    	$('div#editModal div.modal-footer button.detailsAddToChart').prop('disabled', true);
                    } else {
                    	$('div#editModal div.modal-footer button.detailsAddToChart').prop('disabled', false);
                    }

                },
                error: function(status) {
					//Do something  
				}
			});
}

function populateComponentsInEditModal(deviceComponents) {
	console.log('DEBUG populateChooseComponentTable is called');
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
            // check components that are actual
            $('div#editModal div#itemComponents textarea').html('');
            console.log('DEBUG deviceComponents.length = ' + deviceComponents.length);

            for (var i = 0; i<deviceComponents.length; i++){
                    // add components to actual components text area
                    console.log('DEBUG ' + i + ' sad cu da dodam u comps text area : ' + deviceComponents[i].name);
                    $('div#editModal div#itemComponents textarea').append(deviceComponents[i].name + "; \n");
                }
            },
            error: function(status) {
                //Do something  
            }
        });
}

function clearDeviceSearchArea() {
	$('div#totalSearchName input').val('');
	$('div#totalSearchDescription input').val('');
	$("div#ChoooseComponentsTableTotalSearch tbody td input:checked").attr('checked', false);
}

function totalSearchDevices(name, desc) {
	var choosenComponents = new Array();

	$("div#ChoooseComponentsTableTotalSearch tbody td input:checked").each(function(){
		choosenComponents.push($(this).val());
	});

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "1"},
		dataType: "json",
		success: function(data) {
			for (var ii = 0; ii<data.length; ii++) {
				var nameFilter = false;
				var descFilter = false;
				var compsFilter = false;
				var devCmps = new Array();

				for (var i = 0; i<data[ii].components.length; i++) {
					devCmps.push(data[ii].components[i].name);
				}

				if (containsArray(devCmps, choosenComponents)) {
					compsFilter = true;
				}

				if ((data[ii].name.toLowerCase().indexOf(name.toLowerCase()) > -1) || name == '') {
					nameFilter = true;
				}

				if ((data[ii].description.toLowerCase().indexOf(desc.toLowerCase()) > -1) || desc == '') {
					descFilter = true;
				}

				if (nameFilter && descFilter && compsFilter) {
					$('div#thumbnailsArea div.aThumbnail').each(function(index, element){
						var $nameA = $(this).find('a.btn.btn-link.details');
						if ($nameA.text().trim().toLowerCase() == data[ii].name.trim().toLowerCase()){ // trim, without it produce problems
							console.log('DEBUG ' + data[ii].name + ' treba show');

							$(this).show();
							return; // DEBUG to speed up iterations
						}
					});
				} else {
					$('div#thumbnailsArea div.aThumbnail').each(function(index, element){
						var $nameA = $(this).find('a.btn.btn-link.details');
						if ($nameA.text().trim().toLowerCase() == data[ii].name.trim().toLowerCase()){ // trim, without it produce problems
							console.log('DEBUG ' + data[ii].name + ' treba hide');

							$(this).hide();
							return; // DEBUG to speed up iterations
						}
					});
				}
			}
		},
		error: function(status) {
			console.log("Error in totalSearchDevices: + " + status);  
		}
	});
}

function searchComponentsByNameTotalSearch(searchQuery) {
	$('div#ChoooseComponentsTableTotalSearch table tbody tr').each(function (index, element) {
		if (searchQuery == '') {
			$(this).show();
		} else {
			var $tds = $(this).find('td');
			if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
				$(this).hide();
			} else {
				$(this).show();
			}
		}
	});
} 

function clearFieldsAddDevice() {
	$('div#addModal #itemDescription > textarea').val('');
    $('div#addModal div#searchComponents input#searchQuery').val(''); // to avoid dirty search field

    //if there is no logged user disable addToChart
    console.log('DEBUG $(div#userBean).text() = ' + $('div#userBean').text());
    if ($('div#userBean').text() == '') {
    	$('div#addModal div.modal-footer button.addAddToChart').prop('disabled', true);
    } else {
    	$('div#addModal div.modal-footer button.addAddToChart').prop('disabled', false);
    	validateInputAddDevice(); // if user is logged in and there are no components disable add device to chart
    }
}

function populateChooseComponentTableAddDevice() {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
			$("div#addModal div#ChoooseComponentsTable tbody").find("td").remove();
			for(var i = 0; i<data.length; i++) {
				$("div#addModal div#ChoooseComponentsTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
			}
		},
		error: function(status) {
                //Do something  
            }
        });
}

function searchComponentsByNameAddDevice(searchQuery) {
	$('div#addModal div#ChoooseComponentsTable table tbody tr').each(function (index, element) {
		if (searchQuery == '') {
			$(this).show();
		} else {
			var $tds = $(this).find('td');
			if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
				$(this).hide();
			} else {
				$(this).show();
			}
		}
	});
}

function processFormBuildDevice() {
	var compNamesToAdd = [];
	$("div#addModal div#ChoooseComponentsTable tbody td input:checked").each(function(){
		compNamesToAdd.push($(this).val());
	});
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "post",
		data:{
			description: $("div#addModal div#itemDescription > textarea").val(),
			componentNames: JSON.stringify(compNamesToAdd),
			ajaxId: 333

		},
		success: function(data,status){
			alert("You're added a new device to your chart.");
		},
		error: function(xhr, status){
			console.log('error in processFormBuildDevice, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}

function validateInputAddDevice() {
	var deviceIsValid = false;
	$("div#addModal div#ChoooseComponentsTable tbody td input:checked").each(function(){
		deviceIsValid = true;
		return;
	});

	if (deviceIsValid == true && $('div#userBean').text() != '') { //added condition for loged user
		$('div#addModal .btn.addAddToChart').attr('disabled', false);
	} else if (deviceIsValid == false) {
		$('div#addModal .btn.addAddToChart').attr('disabled', true);
	}
}

function testFunction() {
	asocArray = {};
	asocArray["compo1"] = 12;
	asocArray["compo32"] = 4;
	asocArray["compo45"] = 757;
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "post",
		data:{
			components:JSON.stringify(asocArray),
            ajaxId: 0 // "2"

        },
        success: function(data,status){
        },
        error: function(xhr, status){
        }
    });
}

function addComponentToChart(compName) {
	console.log('DEBUG compName = ' + compName);
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "post",
		data:{
			compName: compName.trim(),
			ajaxId: 222

		},
		success: function(data,status){
			alert("Component " + $("div#editModal div#itemName > p").html() + " is successfully added to chart.");
		},
		error: function(xhr, status){
			console.log('error in addComponentToChart, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}

function addDeviceToChart(deviceName) {
	console.log('DEBUG deviceName = ' + deviceName);
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "post",
		data:{
			deviceName: deviceName.trim(),
			ajaxId: 111

		},
		success: function(data,status){
			alert("Device " + $("div#editModal div#itemName > p").html() + " is successfully added to chart.");
		},
		error: function(xhr, status){
			console.log('error in addDeviceToChart, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}

function populateChartModal() {
	$('#yourChartModal div.modal-header h3#modalHeader').html('<b>' + $('div#userBean').text() + '</b>' + "'s shopping chart");

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "0"},
		dataType: "json",
		success: function(data) {
			$("div#yourChartModal.modal div#articlesTable tbody").find("td").remove();
			var devicesDict = {};
			var yourDevicesDict = {};

			for (var key in data.components) {
				keyName = key.split('name=')[1].split(',')[0].trim();
				keyPrice = key.split('price=')[1].split(',')[0].trim();
				$("div#yourChartModal.modal div#articlesTable tbody").append("<tr>" + "<td> component </td>"+ "<td>" + keyName + "</td>" + "<td>" + "<select id='amountPickerComp'></select>" + "</td>" + "<td>" + keyPrice +  "</td>" +   "<td>" + "<button id='chartItemRemoveComp' class='btn btn-danger removeChartItem' value='" + keyName + "'>" + "Remove</button>" + "</td>" + "</tr>");
			}

			for (var key in data.devices) {
				keyName = key.split('name=')[1].split(',')[0].trim();
				keyCompsString = key.split('components=')[1].split('ShopComponent [name=');
				keyCompsNames = [];
				for (var i = 1; i<keyCompsString.length; i++) {
					keyCompsNames.push(keyCompsString[i].split(',')[0].trim());
				}
				devicesDict[keyName] = keyCompsNames;
				$("div#yourChartModal.modal div#articlesTable tbody").append("<tr>" + "<td> device </td>"+ "<td>" + keyName + "</td>" + "<td>" + "<select id='amountPickerDev'><option value='1'>1</option></select>" + "</td>" + "<td>" + "</td>" + "<td>" + "<button id='chartItemRemoveDev' class='btn btn-danger removeChartItem' value='" + keyName + "'>" + "Remove</button>" + "</td>" + "</tr>");
			}

			for (var key in data.generatedDevices) {
				keyName = key.split('name=')[1].split(',')[0].trim();
				keyCompsString = key.split('components=')[1].split('ShopComponent [name=');
				keyCompsNames = [];
				for (var i = 1; i<keyCompsString.length; i++) {
					keyCompsNames.push(keyCompsString[i].split(',')[0].trim());
				}
				yourDevicesDict[keyName] = keyCompsNames;
				$("div#yourChartModal.modal div#articlesTable tbody").append("<tr>" + "<td> your device </td>"+ "<td>" + keyName + "</td>" + "<td>" + "<select id='amountPickerDev'></select>" + "</td>" + "<td>" + "</td>" + "<td>" + "<button id='chartItemRemoveYourDev' class='btn btn-danger removeChartItem' value='" + keyName + "'>" + "Remove</button>" + "</td>" + "</tr>");
			}

			console.log('DEBUG sada poziv fillMissingPricesInShopChartTable');
			fillMissingPricesInShopChartTable(devicesDict, yourDevicesDict);

			// decide visibility of buy button
			if ($("div#yourChartModal.modal div#articlesTable tbody tr").length == 0) {
				$('div#yourChartModal div.modal-footer button.yourChartBuy').prop('disabled', true);
			} else {
				$('div#yourChartModal div.modal-footer button.yourChartBuy').prop('disabled', false);
			}
		},
		error: function(status) {
                //Do something  
            }
        });
}

function fillMissingPricesInShopChartTable(devicesDict, yourDevicesDict) {
	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "2"},
		dataType: "json",
		success: function(data) {
			for (var key in devicesDict) {
				priceSum = 0;
				for (var ii = 0; ii<devicesDict[key].length; ii++) {
					for(var i = 0; i<data.length; i++) {
						if (data[i].name.trim() == devicesDict[key][ii].trim()) {
							priceSum += parseFloat(data[i].price);
						}
					}
				}
				// find apropriate column in table and fill the price
				$("div#yourChartModal.modal div#articlesTable tbody tr").each(function(index, element){
					$tds = $(this).find('td');
					if ($tds.eq(0).text().trim() == 'device' && $tds.eq(1).text().trim() == key) {
						console.log('DEBUG postavljam za ' + $tds.eq(1).text().trim() + " cenu " + priceSum.toFixed(2));
						$tds.eq(3).text(priceSum.toFixed(2));
						return false;
					}
				});
			}

			// ovde ovaj for je dobar nabaci tako i onaj gore
			for (var key in yourDevicesDict) {
				priceSum = 0;
				for (var ii = 0; ii<yourDevicesDict[key].length; ii++) {
					for(var i = 0; i<data.length; i++) {
						if (data[i].name.trim() == yourDevicesDict[key][ii].trim()) {
							console.log('DEBUG dodajem ' + data[i].price + ' na ' + priceSum);
							priceSum += parseFloat(data[i].price);
							break; // nemoj vise vrteti po svim komp predji na sledecu u yourDevicesDict
						}
					}
				}
				// find apropriate column in table and fill the price
				$("div#yourChartModal.modal div#articlesTable tbody tr").each(function(index, element){
					$tds = $(this).find('td');
					console.log('DEBUG poredim ' + $tds.eq(1).text().trim() + ' = ' + key);
					if ($tds.eq(0).text().trim() == 'your device' && $tds.eq(1).text().trim() == key) {
						console.log('DEBUG postavljam za ' + $tds.eq(1).text().trim() + " cenu " + priceSum.toFixed(2));
						$tds.eq(3).text(priceSum.toFixed(2));
						return false;
					}
				});
			}


			fillMissingAmountPickers(data, devicesDict, yourDevicesDict);

		},
		error: function(status) {
            //Do something  
        }
    });
}

function fillMissingAmountPickers(allComponents, devicesDict, yourDevicesDict) {

	// fill missing amount picker for components
	$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function() {
		$tds = $(this).find('td');
		return $tds.eq(0).text().trim() == 'component';
	}).each(function(index, element){
		var tableRowItem = $(this).find('td').eq(1).text().trim();
		for (var i = 0; i<allComponents.length; i++) {
			if (tableRowItem == allComponents[i].name.trim()) {
				for (var ii = 0; ii<parseInt(allComponents[i].amount); ii++) {
					$(this).find('td').eq(2).find('select').append($("<option></option>").prop('value', ii+1).text(ii+1));
				}
				break;
			}
		}
	});

	// fill missing amount picker for your devices
	$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function() {
		$tds = $(this).find('td');
		return $tds.eq(0).text().trim() == 'your device';
	}).each(function(index, element){
		var tableRowItem = $(this).find('td').eq(1).text().trim();
		var actualAprovedAmount = 0;
		var actualAprovedAmountSet = false;
		for (var j = 0; j<yourDevicesDict[tableRowItem].length; j++) {
			if (actualAprovedAmountSet)
				break;
			for (var i = 0; i<allComponents.length; i++) {
				if (yourDevicesDict[tableRowItem][j].trim() == allComponents[i].name.trim()) {
					actualAprovedAmount = parseInt(allComponents[i].amount);
					actualAprovedAmountSet = true;
					break;
				}
			}
		}
		console.log('DEBUG inicijalno actualAprovedAmount = ' + actualAprovedAmount);
		for (var j = 0; j<yourDevicesDict[tableRowItem].length; j++){
			for (var i = 0; i<allComponents.length; i++) {
				if (yourDevicesDict[tableRowItem][j].trim() == allComponents[i].name.trim()) {
					if (actualAprovedAmount >= parseInt(allComponents[i].amount)) {
						actualAprovedAmount = parseInt(allComponents[i].amount);
						console.log('DEBUG sada je actualAprovedAmount = ' + actualAprovedAmount + '\njer je radjeno za ' + yourDevicesDict[tableRowItem][j].trim());
					}
					break;
				}
			}
		}

		for (var ii = 0; ii<actualAprovedAmount; ii++) {
			$(this).find('td').eq(2).find('select').append($("<option></option>").prop('value', ii+1).text(ii+1));
		}
		// END fill missing amount picker for your devices
	});
}

function deleteItemFromUserChart(itemType, itemName) {
	if (itemType.trim() == 'chartItemRemoveComp') {
		$.ajax({
			url: window.contextPath + "/StoreServlet" + '?' + $.param({"ajaxId": '222', "toDelete" : itemName}),
			type: "delete",
			success: function(data){
				$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
					$tds = $(this).find('td');
					return $tds.eq(0).text().trim() == 'component' && $tds.eq(1).text().trim() == itemName.trim()
				}).remove();
			},    
			error: function(status) { 
				console.leg('ERROR in deleteItemFromUserChart');
			}
		});
	} else if (itemType.trim() == 'chartItemRemoveDev') {
		$.ajax({
			url: window.contextPath + "/StoreServlet" + '?' + $.param({"ajaxId": '111', "toDelete" : itemName}),
			type: "delete",
			success: function(data){
				console.log('DEBUG succes od ajaxa od chartItemRemoveComp');

				$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
					$tds = $(this).find('td');
					console.log('DEBUG tds.eq(0).text().trim() = ' + $tds.eq(0).text().trim());
					console.log('DEBUG tds.eq(1).text().trim() = ' + $tds.eq(1).text().trim());

					return $tds.eq(0).text().trim() == 'device' && $tds.eq(1).text().trim() == itemName.trim()
				}).remove();
			},    
			error: function(status) { 
				console.leg('ERROR in deleteItemFromUserChart');
			}
		});
	} else if (itemType.trim() == 'chartItemRemoveYourDev') {
		$.ajax({
			url: window.contextPath + "/StoreServlet" + '?' + $.param({"ajaxId": '333', "toDelete" : itemName}),
			type: "delete",
			success: function(data){
				$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
					$tds = $(this).find('td');
					return $tds.eq(0).text().trim() == 'your device' && $tds.eq(1).text().trim() == itemName.trim()
				}).remove();
			},    
			error: function(status) { 
				console.leg('ERROR in deleteItemFromUserChart');
			}
		});
	}
}

function processBuyingForm() {
	var components = {};
	var devices = {};
	var generatedDevices = {};

	$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
		$tds = $(this).find('td');
		return $tds.eq(0).text().trim() == 'component'
	}).each(function(index, element){
		$tds = $(this).find('td');
		$select = $(this).find('select');
		//console.log('DEBUG dodaj par za COMPO' + $tds.eq(1).text().trim() + ' : ' + $select[0].options[$select[0].selectedIndex].value);
		console.log('DEBUG $select.find(option).length = ' + $select.find('option').length);
		if ($select.find('option').length == 0) {
			components[$tds.eq(1).text().trim()] = "0";
		} else {
			components[$tds.eq(1).text().trim()] = $select[0].options[$select[0].selectedIndex].value;
		}
	});

	$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
		$tds = $(this).find('td');
		return $tds.eq(0).text().trim() == 'device'
	}).each(function(index, element){
		$tds = $(this).find('td');
		$select = $(this).find('select');
		//console.log('DEBUG dodaj par za DEV' + $tds.eq(1).text().trim() + ' : ' + $select[0].options[$select[0].selectedIndex].value);
		if ($select.find('option').length == 0) {
			devices[$tds.eq(1).text().trim()] = "0";
		} else {
			devices[$tds.eq(1).text().trim()] = $select[0].options[$select[0].selectedIndex].value;
		}
	});

	$("div#yourChartModal.modal div#articlesTable tbody tr").filter(function(){
		$tds = $(this).find('td');
		return $tds.eq(0).text().trim() == 'your device'
	}).each(function(index, element){
		$tds = $(this).find('td');
		$select = $(this).find('select');
		//console.log('DEBUG dodaj par za GENDEV' + $tds.eq(1).text().trim() + ' : ' + $select[0].options[$select[0].selectedIndex].value);
		if ($select.find('option').length == 0) {
			generatedDevices[$tds.eq(1).text().trim()] = "0";
		} else {
			generatedDevices[$tds.eq(1).text().trim()] = $select[0].options[$select[0].selectedIndex].value;
		}
	});

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "post",
		data:{
			components: JSON.stringify(components),
			devices: JSON.stringify(devices),
			generatedDevices : JSON.stringify(generatedDevices),
			ajaxId: 0

		},
		success: function(data,status){
			alert("Thank you for shopping in our SmallShop.");
			location.reload();
		},
		error: function(xhr, status){
			console.log('DEBUG status is = ' + xhr.status);
			if (xhr.status == '409') {
				alert("Sorry, your buying request can't be processed by our server, please try buy articles one by one. ");
			}
			console.log('error in processBuyingForm, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}