function populateDeviceTable(){
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "1"},
        dataType: "json",
        success: function(data) {
            $("div#deviceTable > table > tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#deviceTable > table > tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<button value='" + data[i].name + "' class='btn btn-info details' data-toggle='modal' data-target='#editModal'>Details</button>" + "</td>" + "<td>"+ "<button value='" + data[i].name + "' class='btn btn-danger delete'>Delete</button>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
            console.log("Error in populateDeviceTable: + " + status);  
        }
    });
}

function populateDeviceDetails(name) {
    $('div#editModal div#itemName > p').html(name);
    $('div#editModal #modalHeader').html('Details for ' + '<b>' + name + '</b>');
    $('div#editModal div#searchComponents input#searchQuery').val(''); // to avoid dirty search field
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "11", toEdit : name},
        dataType: "json",
        success: function(data) {
                    // fill description
                    $("div#editModal div#itemDescription > textarea").val(data.description);
                    console.log('DEBUG data.components = ' + data.components);
                    populateChooseComponentTable(data.components);
                },
                error: function(status) {
                    //Do something  
                }
            });
}

function populateChooseComponentTable(deviceComponents){
    console.log('DEBUG populateChooseComponentTable is called');
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "2"},
        dataType: "json",
        success: function(data) {
            $("div#editModal div#ChoooseComponentsTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#editModal div#ChoooseComponentsTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
            // check components that are actual
            $('div#editModal div#itemComponents textarea').html('');
            console.log('DEBUG deviceComponents.length = ' + deviceComponents.length);

            for (var i = 0; i<deviceComponents.length; i++){
                $("div#editModal div#ChoooseComponentsTable tbody td input").filter(function(){
                    // add checked components to actual components text area
                    console.log('DEBUG ' + this.value + '*****' + deviceComponents[i].name);
                    if (this.value == deviceComponents[i].name) {
                        console.log('DEBUG ' + i + ' sad cu da dodam u comps text area : ' + deviceComponents[i].name);
                        $('div#editModal div#itemComponents textarea').append(deviceComponents[i].name + "; \n");
                    }
                    return this.value == deviceComponents[i].name}).attr("checked", true);
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function populateChooseComponentTableAddDevice(){
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "2"},
        dataType: "json",
        success: function(data) {
            $("div#addModal div#ChoooseComponentsTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#addModal div#ChoooseComponentsTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function searchComponentsByName(searchQuery) {
    $('div#editModal div#ChoooseComponentsTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function searchComponentsByNameAddDevice(searchQuery) {
    $('div#addModal div#ChoooseComponentsTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function processForm() {
    console.log('DEBUG processForm called');
    var compNamesToAdd = [];
    $("div#editModal div#ChoooseComponentsTable tbody td input:checked").each(function(){
        compNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name:$("div#editModal div#itemName > p").html(),
                description:$("div#editModal div#itemDescription > textarea").val(),
                components:null
            }),
            componentNames:JSON.stringify(compNamesToAdd),
            ajaxId: 11 // "11"

        },
        success: function(data,status){
            alert("Device " + $("div#editModal div#itemName > p").html() + " is successfully edited");
            populateDeviceTable();
        },
        error: function(xhr, status){
            console.log('error in processForm, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function processFormAddDevice() {
    console.log('DEBUG processFormAddDevice called');
    var compNamesToAdd = [];
    $("div#addModal div#ChoooseComponentsTable tbody td input:checked").each(function(){
        compNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name:$("div#addModal div#itemName > input:text").val().split(',').join(' ').split('[').join('(').split(']').join(')'), // DEBUG server side char problems
                description:$("div#addModal div#itemDescription > textarea").val(),
                components:null
            }),
            componentNames:JSON.stringify(compNamesToAdd),
            ajaxId: 1 // "1"

        },
        success: function(data,status){
            alert("Device " + $("div#addModal div#itemName > input:text").val() + " is successfully added");
            populateDeviceTable();
        },
        error: function(xhr, status){
            console.log('error in processFormAddDevice, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function populateChooseComponentTableTotalSearch() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "2"},
        dataType: "json",
        success: function(data) {
            $("div#ChoooseComponentsTableTotalSearch tbody").find("td").remove(); // $("ChoooseComponentsTableTotalSearch tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#ChoooseComponentsTableTotalSearch tbody").append("<tr>" + "<td>" + data[i].name + "</td>"+ "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function totalSearch(name, desc) {
    var choosenComponents = new Array();

    $("div#ChoooseComponentsTableTotalSearch tbody td input:checked").each(function(){
        choosenComponents.push($(this).val());
    });

    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "1"},
        dataType: "json",
        success: function(data) {
            for (var ii = 0; ii<data.length; ii++) {
                var nameFilter = false;
                var descFilter = false;
                var compsFilter = false;
                var devCmps = new Array();

                for (var i = 0; i<data[ii].components.length; i++) {
                    devCmps.push(data[ii].components[i].name);
                }

                if (containsArray(devCmps, choosenComponents)) {
                    compsFilter = true;
                }

                if ((data[ii].name.toLowerCase().indexOf(name.toLowerCase()) > -1) || name == '') {
                    nameFilter = true;
                }

                if ((data[ii].description.toLowerCase().indexOf(desc.toLowerCase()) > -1) || desc == '') {
                    descFilter = true;
                }

                if (nameFilter && descFilter && compsFilter) {
                    $('div#deviceTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba show');

                            $(this).show();
                        }
                    });
                } else {
                    $('div#deviceTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba hide');

                            $(this).hide();
                        }
                    });
                }
            }
        },
        error: function(status) {
            console.log("Error in totalSearch: + " + status);  
        }
    });
}

function containsArray(bigArray, smallArray){
    for (var i = 0; i<smallArray.length; i++) {
        if (bigArray.indexOf(smallArray[i]) <= -1) {
            return false;
        }
    }
    return true;
}

function clearDeviceSearchArea() {
    $('div#totalSearchName input').val('');
    $('div#totalSearchDescription input').val('');
    $("div#ChoooseComponentsTableTotalSearch tbody td input:checked").attr('checked', false);
}

function searchComponentsByNameTotalSearch(searchQuery) {
    $('div#ChoooseComponentsTableTotalSearch table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.trim().toLowerCase()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function clearFieldsAddDevice() {
    $('div#addModal #itemDescription > textarea').val('');
    $('div#addModal #itemName > input:text').val('');
    $('span#nameError').attr('hidden', true);
    $('div#addModal div#searchComponents input#searchQuery').val(''); // to avoid dirty search field
}

function validateInputAddDeviceNameField() {
    var notExists = true;
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "1"},
        dataType: "json",
        success: function(data) {
            for(var i = 0; i<data.length; i++) {
                if (data[i].name == $("div#addModal div#itemName > input:text").val()) {
                    notExists = false;
                    $('span#nameError').attr('hidden', false);
                    break;
                }
                if (notExists) {
                    $('span#nameError').attr('hidden', true);
                }
            }
            if ($("div#addModal div#itemName > input:text").val() != '' && notExists) {
                $('div#addModal .btn.addAdd').attr('disabled', false);
            }
            else {
                $('div#addModal .btn.addAdd').attr('disabled', true);
            }
        },
        error: function(status) {
            console.log("Error in validateInputAddDeviceNameField: + " + status);  
        }
    });

}

 // without ajax call
 function validateInputAddDeviceNameFieldReal(){
    var notExists = true;
    $('div#deviceTable table tbody tr').each(function(index, element){
        var $tds = $(this).find('td');
        if ($tds.eq(0).text().trim() == $("div#addModal div#itemName > input:text").val().trim()){
            notExists = false;
            $('span#nameError').attr('hidden', false);
            return false;
        }
    });

    if (notExists) {
        $('span#nameError').attr('hidden', true);
    }

    if ($("div#addModal div#itemName > input:text").val() != '' && notExists) {
        $('div#addModal .btn.addAdd').attr('disabled', false);
    }
    else {
        $('div#addModal .btn.addAdd').attr('disabled', true);
    }
}

function fillCategoriesSelectInReportModal() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $('div#reportModal.modal div#categorySelectDiv select#categorySelect').find("option").remove();
            for(var i = 0; i<data.length; i++) {
                $('div#reportModal.modal div#categorySelectDiv select#categorySelect').append("<option value='" + data[i].name + "''>" + data[i].name + "</option>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

/* OPERATIONS FOR COMPONENTS */

function populateComponentsTable() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "2"},
        dataType: "json",
        success: function(data) {
            $("div#componentTable > table > tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                var categories = '';
                for (var ii = 0; ii<data[i].category.length; ii++) {
                    var categories = categories + data[i].category[ii].name + "; "
                }
                console.log('DEBUG populateComponentsTable categories = ' + categories);
                $("div#componentTable > table > tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].price + "</td>" + "<td>" + data[i].amount + "</td>" + "<td>" + categories + "</td>"+ "<td>" + "<button value='" + data[i].name + "' class='btn btn-info details' data-toggle='modal' data-target='#editModal'>Details</button>" + "</td>" + "<td>" + "<button value='" + data[i].name + "' class='btn btn-danger delete'>Delete</button>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function populateChooseCategoryTableTotalSearch() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#ChoooseCategoryTableTotalSearch tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#ChoooseCategoryTableTotalSearch tbody").append("<tr>" + "<td>" + data[i].name + "</td>"+ "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function populateComponentDetails(name) {
    $('div#editModal div#itemName > p').html(name);
    $('div#editModal #modalHeader').html('Details for ' + '<b>' + name + '</b>');
    console.log('DEBUG ocisti searchQuery polje');
    $('div#editModal div#searchCategories input#searchQuery').val(''); // to avoid dirty search field
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "21", toEdit : name},
        dataType: "json",
        success: function(data) {
            $("div#editModal div#itemAmount > input").val(data.amount);
            $("div#editModal div#itemPrice > input").val(data.price);
            $("div#editModal div#itemLink > input").val(data.link);
            $("div#editModal div#itemDescription > textarea").val(data.description);
            //fill image
            $('div#editModal div#itemImage > img').attr( "src", 'data:image/jpeg;base64,' + data.imageFile );
            $('div#editModal #componentimageBase64').html( data.imageFile );
            $('div#editModal #componentimageExtension').html('jpeg');
            // end fill image 

            populateChooseCategoryTable(data.category);
        },
        error: function(status) {
                    //Do something  
                }
            });
}

function populateChooseCategoryTable(componentCategory) {
    // componentCategory is Java ArrayList<Category>
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#editModal div#ChoooseCategoriesTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#editModal div#ChoooseCategoriesTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
            // check categories that are actual
            $('div#editModal div#itemCategory input').val('');
            for (var i = 0; i<componentCategory.length; i++){
                $("div#editModal div#ChoooseCategoriesTable tbody td input").filter(function(){
                    // add checked components to actual components text area
                    if (this.value == componentCategory[i].name) {
                        console.log('DEBUG ' + i + ' sad cu da dodam u ctgs text area : ' + componentCategory[i].name);
                        $('div#editModal div#itemCategory > input').val($('div#editModal div#itemCategory > input').val() + componentCategory[i].name + ";");
                    }
                    return this.value == componentCategory[i].name}).attr("checked", true);
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function searchCategoriesByName(searchQuery) {
    $('div#editModal div#ChoooseCategoriesTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

// fully duplicate function
// function searchCategoriesByNameAddComponent(searchQuery) {
//     $('div#addModal div#ChoooseCategoriesTable table tbody tr').each(function (index, element) {
//         if (searchQuery == '') {
//             $(this).show();
//         } else {
//             var $tds = $(this).find('td');
//             if (!($tds.eq(0).text().toLowerCase().indexOf(searchQuery.toLowerCase()) > -1)) {
//                 $(this).hide();
//             } else {
//                 $(this).show();
//             }
//         }
//     });
// }

function processFormEditComponent() {
    console.log('DEBUG processFormComponent called');
    var categoryNamesToAdd = [];
    var priceToAdd = 0;
    var amountToAdd = 0;
    if ($("div#editModal div#itemPrice > input").val() != '') {
        priceToAdd = $("div#editModal div#itemPrice > input").val();
    }
    if ($("div#editModal div#itemAmount > input").val() != '') {
        amountToAdd = $("div#editModal div#itemAmount > input").val();
    }
    $("div#editModal div#ChoooseCategoriesTable tbody td input:checked").each(function(){
        categoryNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name:$("div#editModal div#itemName > p").html(),
                price: priceToAdd, //$("div#editModal div#itemPrice > input").val(),
                amount: amountToAdd, //$("div#editModal div#itemAmount > input").val(),
                categoryName: '',
                link: $("div#editModal div#itemLink > input").val(),
                imageFile: $("div#editModal div#componentimageBase64").html(),
                imageExtension: $("div#editModal div#componentimageExtension").html(),
                description: $("div#editModal div#itemDescription > textarea").val(),
                category: null
            }),
            categoryNames:JSON.stringify(categoryNamesToAdd),
            ajaxId: 21 //"21"

        },
        success: function(data,status){
            alert("Component " + $("div#editModal div#itemName > p").html() + " is successfully edited");
            populateComponentsTable();
        },
        error: function(xhr, status){
            console.log('error in processFormEditComponent, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function clearComponentSearchArea() {
    $('div#totalSearchName input').val('');
    $('div#totalSearchDescription input').val('');
    $('div#totalSearchPriceRange select>option:eq(0)').prop('selected', true);
    $('div#totalSearchAmountRange select>option:eq(0)').prop('selected', true);
    $("div#ChoooseCategoryTableTotalSearch tbody td input:checked").prop('checked', false);
}

function totalSearchComponents(name, desc, priceRange, amountRange) {
    console.log('DEBUG priceRange = ' + priceRange);
    console.log('DEBUG amountRange = ' + amountRange);

    var choosenCategories = new Array();

    $("div#ChoooseCategoryTableTotalSearch tbody td input:checked").each(function(){
        choosenCategories.push($(this).val());
    });

    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "2"},
        dataType: "json",
        success: function(data) {
            for (var ii = 0; ii<data.length; ii++) {
                var nameFilter = false;
                var descFilter = false;
                var categoriesFilter = false;
                var priceFilter = false;
                var amountFilter = false;
                var compCtry = new Array();

                for (var i = 0; i<data[ii].category.length; i++) {
                    compCtry.push(data[ii].category[i].name);
                }

                if (containsArray(compCtry, choosenCategories)) {
                    categoriesFilter = true;
                }

                if ((data[ii].name.toLowerCase().indexOf(name.toLowerCase()) > -1) || name == '') {
                    nameFilter = true;
                }

                if ((data[ii].description.toLowerCase().indexOf(desc.toLowerCase()) > -1) || desc == '') {
                    descFilter = true;
                }

                switch (priceRange) {
                    case 'r1':
                    if ( Number(data[ii].price) >= 0 && Number(data[ii].price) < 300){
                        priceFilter = true;
                    }
                    break;
                    case 'r2':
                    if ( Number(data[ii].price) >= 300 && Number(data[ii].price) < 600) {
                        console.log('DEBUG ' + Number(data[ii].price) + '\n a Number(data[ii].price) >= 300 && Number(data[ii].price) < 600 = true');
                        priceFilter = true;
                    }
                    break;
                    case 'r3':
                    if ( Number(data[ii].price) >= 600 && Number(data[ii].price) < 900) {
                        priceFilter = true;
                    }
                    break;
                    case 'r4':
                    if ( Number(data[ii].price) >= 900 && Number(data[ii].price) < 1200) {
                        priceFilter = true;
                    }
                    break;
                    case 'r5':
                    if ( Number(data[ii].price) >= 1200) {
                        priceFilter = true;
                    }
                    break;
                    case '':
                    console.log('DEBUG OTISAO NA PRAZAN STRING CASE price range');
                    priceFilter = true;
                    break;
                }
                console.log('DEBUG priceFilter za ' + data[ii].name + ' = ' + priceFilter);

                switch (amountRange) {
                    case 'r1':
                    if ( Number(data[ii].amount) >= 0 && Number(data[ii].amount) < 10) {
                        amountFilter = true;
                    }
                    break;
                    case 'r2':
                    if ( Number(data[ii].amount) >= 10 && Number(data[ii].amount) < 20) {
                        amountFilter = true;
                    }
                    break;
                    case 'r3':
                    if ( Number(data[ii].amount) >= 20 && Number(data[ii].amount) < 50) {
                        amountFilter = true;
                    }
                    break;
                    case 'r4':
                    if (Number(data[ii].amount) >= 50) {
                        amountFilter = true;
                    }
                    break;
                    case '':
                    amountFilter = true;
                    break;
                }
                console.log('DEBUG amountFilter za ' + data[ii].name + ' = ' + amountFilter);

                if (nameFilter && descFilter && categoriesFilter && priceFilter && amountFilter) {
                    $('div#componentTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba show');

                            $(this).show();
                        }
                    });
                } else {
                    $('div#componentTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba hide');

                            $(this).hide();
                        }
                    });
                }
            }
        },
        error: function(status) {
            console.log("Error in totalSearchCopmponents: + " + status);  
        }
    });
}

function searchCategoriesByNameTotalSearch(searchQuery) {
    $('div#ChoooseCategoryTableTotalSearch table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function clearFieldsAddComponent() {
    $('div#addModal #itemDescription > textarea').val('');
    $('div#addModal #itemName > input:text').val('');
    $('div#addModal #itemAmount > input').val('');
    $('div#addModal #itemPrice > input').val('');
    $('div#addModal #itemLink > input').val('');
    $('div#addModal div#itemImage > img').attr( "src", '' );
    $('span#nameError').attr('hidden', true);
    $('div#addModal div#searchCategories input#searchQuery').val(''); // to avoid dirty search field

    $('div#addModal span#AmountError').attr('hidden', true);
    $('div#addModal span#PriceError').attr('hidden', true);
}

function populateChooseCategoryTableAddComponent() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#addModal div#ChoooseCategoriesTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#addModal div#ChoooseCategoriesTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function searchCategoriesByNameAddComponent(searchQuery) {
    $('div#addModal div#ChoooseCategoriesTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function processFormAddComponent() {
    console.log('DEBUG processFormAddComponent called');
    var categoryNamesToAdd = [];
    var priceToAdd = 0;
    var amountToAdd = 0;
    if ($("div#addModal div#itemPrice > input").val() != '') {
        priceToAdd = $("div#addModal div#itemPrice > input").val();
    }
    if ($("div#addModal div#itemAmount > input").val() != '') {
        amountToAdd = $("div#addModal div#itemAmount > input").val();
    }
    $("div#addModal div#ChoooseCategoriesTable tbody td input:checked").each(function(){
        categoryNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name:$("div#addModal div#itemName > input:text").val().split(',').join(' ').split('[').join('(').split(']').join(')'),
                price: priceToAdd, // $("div#addModal div#itemPrice > input").val(),
                amount: amountToAdd, // $("div#addModal div#itemAmount > input").val(),
                categoryName: '',
                link: $("div#addModal div#itemLink > input").val(),
                imageFile: $("div#addModal div#componentimageBase64").html(),
                imageExtension: $("div#addModal div#componentimageExtension").html(),
                description: $("div#addModal div#itemDescription > textarea").val(),
                category: null
            }),
            categoryNames:JSON.stringify(categoryNamesToAdd),
            ajaxId: 2 // "2"

        },
        success: function(data,status){
            alert("Component " + $("div#addModal div#itemName > input:text").val() + " is successfully added");
            populateComponentsTable();
        },
        error: function(xhr, status){
            console.log('error in processFormAddComponent, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function validateInputAddComponentNameFieldReal() {
    var notExists = true;
    $('div#componentTable table tbody tr').each(function(index, element){
        var $tds = $(this).find('td');
        if ($tds.eq(0).text().trim() == $("div#addModal div#itemName > input:text").val().trim()){
            notExists = false;
            $('span#nameError').attr('hidden', false);
            return false;
        }
    });

    if (notExists) {
        $('span#nameError').attr('hidden', true);
    }

    if ($("div#addModal div#itemName > input:text").val() != '' && notExists) {
        $('div#addModal .btn.addAdd').attr('disabled', false);
    }
    else {
        $('div#addModal .btn.addAdd').attr('disabled', true);
    }
}

function hideErrorsEditModal() {
    $('div#editModal span#AmountError').attr('hidden', true);
    $('div#editModal span#PriceError').attr('hidden', true);
}

function validateAmountEditModal(input) {
    if (isNaN(parseInt(input))) {
        $('div#editModal span#AmountError').attr('hidden', false);
        $('div#editModal button.detailsEdit').prop('disabled', true);
    } else {
        $('div#editModal span#AmountError').attr('hidden', true);
        $('div#editModal button.detailsEdit').prop('disabled', false);
    }

}

function validatePriceEditModal(input) {
    if (isNaN(parseFloat(input))) {
        $('div#editModal span#PriceError').attr('hidden', false);
        $('div#editModal button.detailsEdit').prop('disabled', true);
    } else {
        $('div#editModal span#PriceError').attr('hidden', true);
        $('div#editModal button.detailsEdit').prop('disabled', false);
    }

}

function validateAmountAddModal(input) {
    if (isNaN(parseInt(input))) {
        $('div#addModal span#AmountError').attr('hidden', false);
        $('div#addModal button.addAdd').prop('disabled', true);
    } else {
        $('div#addModal span#AmountError').attr('hidden', true);
        $('div#addModal button.addAdd').prop('disabled', false);
    }
}

function validatePriceAddModal(input) {
    if (isNaN(parseFloat(input))) {
        $('div#addModal span#PriceError').attr('hidden', false);
        $('div#addModal button.addAdd').prop('disabled', true);
    } else {
        $('div#addModal span#PriceError').attr('hidden', true);
        $('div#addModal button.addAdd').prop('disabled', false);
    }
}

/* OPERATIONS FOR CATEGORIES */

function populateCategoryTable() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#categoryTable > table > tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#categoryTable > table > tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<button value='" + data[i].name + "' class='btn btn-info details' data-toggle='modal' data-target='#editModal'>Details</button>" + "</td>" + "<td>"+ "<button value='" + data[i].name + "' class='btn btn-danger delete'>Delete</button>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
            console.log("Error in populateCategoryTable: + " + status);  
        }
    });
}

function populateChooseSubcategoriesTableTotalSearch() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#ChoooseSubcategoriesTableTotalSearch > table > tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#ChoooseSubcategoriesTableTotalSearch > table > tbody").append("<tr>" + "<td>" + data[i].name + "</td>"+ "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
            console.log("Error in populateChooseSubcategoriesTableTotalSearch: + " + status);  
        }
    });
}

function populateCategoryDetails(name) {
    $('div#editModal div#itemName > p').html(name);
    $('div#editModal #modalHeader').html('Details for ' + '<b>' + name + '</b>');
    $('div#editModal div#searchSubcategories input#searchQuery').val(''); // to avoid dirty search field
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "31", toEdit : name},
        dataType: "json",
        success: function(data) {
                    // fill description
                    $("div#editModal div#itemDescription > textarea").val(data.description);
                    populateChooseSubcategoryTableEditModal(data.subCategories);
                },
                error: function(status) {
                    //Do something  
                }
            });
}

function populateChooseSubcategoryTableEditModal(category) {
    console.log('DEBUG populateChooseSubcategoryTableEditModal is called');
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#editModal div#ChoooseSubcategoriesTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#editModal div#ChoooseSubcategoriesTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
            // check components that are actual
            $('div#editModal div#itemSubcategories textarea').html('');
            for (var i = 0; i<category.length; i++){
                $("div#editModal div#ChoooseSubcategoriesTable tbody td input").filter(function(){
                    // add checked components to actual components text area
                    if (this.value == category[i].name) {
                        console.log('DEBUG ' + i + ' sad cu da dodam u subcategories text area : ' + category[i].name);
                        $('div#editModal div#itemSubcategories textarea').append(category[i].name + "; \n");
                    }
                    return this.value == category[i].name}).attr("checked", true);
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function searchSubCategoriesByName(searchQuery) {
    $('div#editModal div#ChoooseSubcategoriesTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function processFormEditCategory() {
    console.log('DEBUG processFormEditCategory called');
    var subctyNamesToAdd = [];
    $("div#editModal div#ChoooseSubcategoriesTable tbody td input:checked").each(function(){
        subctyNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name:$("div#editModal div#itemName > p").html(),
                description:$("div#editModal div#itemDescription > textarea").val(),
                subCategories:null
            }),
            categoryNames:JSON.stringify(subctyNamesToAdd),
            ajaxId: 31 // "31"

        },
        success: function(data,status){
            alert("Category " + $("div#editModal div#itemName > p").html() + " is successfully edited");
            populateCategoryTable();
        },
        error: function(xhr, status){
            console.log('error in processFormEditCategory, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function clearCategorySearchArea() {
    $('div#totalSearchName input').val('');
    $('div#totalSearchDescription input').val('');
    $("div#ChoooseSubcategoriesTableTotalSearch tbody td input:checked").attr('checked', false);
}

function totalSearchCategories(name, desc) {
    var choosenCategories = new Array();

    $("div#ChoooseSubcategoriesTableTotalSearch tbody td input:checked").each(function(){
        choosenCategories.push($(this).val());
    });

    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            for (var ii = 0; ii<data.length; ii++) {
                var nameFilter = false;
                var descFilter = false;
                var ctysFilter = false;
                var ctyCtys = new Array();

                for (var i = 0; i<data[ii].subCategories.length; i++) {
                    ctyCtys.push(data[ii].subCategories[i].name);
                }

                if (containsArray(ctyCtys, choosenCategories)) {
                    ctysFilter = true;
                }

                if ((data[ii].name.toLowerCase().indexOf(name.toLowerCase()) > -1) || name == '') {
                    nameFilter = true;
                }

                if ((data[ii].description.toLowerCase().indexOf(desc.toLowerCase()) > -1) || desc == '') {
                    descFilter = true;
                }

                if (nameFilter && descFilter && ctysFilter) {
                    $('div#categoryTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba show');

                            $(this).show();
                        }
                    });
                } else {
                    $('div#categoryTable table tbody tr').each(function(index, element){
                        var $tds = $(this).find('td');
                        if ($tds.eq(0).text().trim().toLowerCase() == data[ii].name.toLowerCase().trim()){
                            console.log('DEBUG ' + data[ii].name + ' treba hide');

                            $(this).hide();
                        }
                    });
                }
            }
        },
        error: function(status) {
            console.log("Error in totalSearchCategories: + " + status);  
        }
    });
}

function searchSubcategoriesByNameTotalSearch(searchQuery) {
    $('div#ChoooseSubcategoriesTableTotalSearch table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function clearFieldsAddCategory() {
    $('div#addModal #itemDescription > textarea').val('');
    $('div#addModal #itemName > input:text').val('');
    $('span#nameError').attr('hidden', true);
    $('div#addModal div#searchSubcategories input#searchQuery').val(''); // to avoid dirty search field
}

function populateChooseSubcategoriesTableAddCategory() {
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data) {
            $("div#addModal div#ChoooseSubcategoriesTable tbody").find("td").remove();
            for(var i = 0; i<data.length; i++) {
                $("div#addModal div#ChoooseSubcategoriesTable tbody").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
            }
        },
        error: function(status) {
                //Do something  
            }
        });
}

function searchCategoryByNameAddCategory(searchQuery) {
    $('div#addModal div#ChoooseSubcategoriesTable table tbody tr').each(function (index, element) {
        if (searchQuery == '') {
            $(this).show();
        } else {
            var $tds = $(this).find('td');
            if (!($tds.eq(0).text().trim().toLowerCase().indexOf(searchQuery.toLowerCase().trim()) > -1)) {
                $(this).hide();
            } else {
                $(this).show();
            }
        }
    });
}

function processFormAddCategory() {
    console.log('DEBUG processFormAddCategory called');
    var ctysNamesToAdd = [];
    $("div#addModal div#ChoooseSubcategoriesTable tbody td input:checked").each(function(){
        ctysNamesToAdd.push($(this).val());
    });
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "post",
        data:{
            jsonData:JSON.stringify({
                name: $("div#addModal div#itemName > input:text").val().split(',').join(' ').split('[').join('(').split(']').join(')'),
                description: $("div#addModal div#itemDescription > textarea").val(),
                subCategories: null
            }),
            categoryNames:JSON.stringify(ctysNamesToAdd),
            ajaxId: 3 // "3"

        },
        success: function(data,status){
            alert("Category " + $("div#addModal div#itemName > input:text").val() + " is successfully added");
            populateCategoryTable();
            populateChooseSubcategoriesTableTotalSearch();
        },
        error: function(xhr, status){
            console.log('error in processFormAddCategory, xhr : ' + xhr + '\nstatus: ' + status);
        }
    });
}

function validateInputAddCategoryNameFieldReal() {
    var notExists = true;
    $('div#categoryTable table tbody tr').each(function(index, element){
        var $tds = $(this).find('td');
        if ($tds.eq(0).text().trim() == $("div#addModal div#itemName > input:text").val().trim()){
            notExists = false;
            $('span#nameError').attr('hidden', false);
            return false;
        }
    });

    if (notExists) {
        $('span#nameError').attr('hidden', true);
    }

    if ($("div#addModal div#itemName > input:text").val() != '' && notExists) {
        $('div#addModal .btn.addAdd').attr('disabled', false);
    }
    else {
        $('div#addModal .btn.addAdd').attr('disabled', true);
    }

}

/* GENERATE REPORT */

function generateStandardReport() {
    var sStartDate = $('div#reportModal div#startDateStandardDiv input#startDateStandard').text().trim();
    var sEndDate = $('div#reportModal div#endDateStandardDiv input#endDateStandard').text().trim();
    $.ajax({
        url: window.contextPath + "/StoreServlet",
        type: "get",
        data: {ajaxId: "00", startDate : sStartDate, endDate: sEndDate, category: ""},
        dataType: "json",
        success: function(data) {
                    
                },
                error: function(xhr, status) {
                    if (xhr.status == '409') {
                        alert("Sorry, your start date and end date are not in the right order. Try again. ");
                    }
                    console.log('error in generateStandardReport, xhr : ' + xhr + '\nstatus: ' + status);
                }
            });
}

function generateCategoryReport() {

}