function populate() {
	var sStartDate = location.search.split('startDate=')[1].split('&')[0].split('-').join('/').trim(); 
	var sEndDate = location.search.split('endDate=')[1].split('&')[0].split('-').join('/').trim();
	var dateReporting = new Date(); 
	console.log('DEBUG sStartDate = ' + sStartDate);
	console.log('DEBUG sEndDate = ' + sEndDate);

	$.ajax({
		url: window.contextPath + "/StoreServlet",
		type: "get",
		data: {ajaxId: "100", startDate : sStartDate, endDate: sEndDate, category: ""},
		dataType: "json",
		success: function(data) {
			$('span#nowDateTime').html('');
			$('span#startPeriod').html('');
			$('span#endPeriod').html('');
			$('span#nowDateTime').html(dateReporting.getMonth()+1 + '/' + dateReporting.getDate() + '/' + dateReporting.getFullYear());
			$('span#startPeriod').html(sStartDate);
			$('span#endPeriod').html(sEndDate);
			$('table tr.tableData').remove();

			var total = 0;
			var ii = 1;
			var evaluatedDates = [];
			for (var i = 0; i<data.length; i++) {
				var thisDate = data[i].dateOfPurchaseString.split(' ')[0].trim();
				if ($.inArray(thisDate, evaluatedDates) == -1) {
					var totalForThisDate =  parseFloat(data[i].totalPrice);
					for (var j = 0; j<data.length; j++) {
						if (thisDate == data[j].dateOfPurchaseString.split(' ')[0] && j!=i) {
							totalForThisDate += parseFloat(data[j].totalPrice);
						}
					}
					$('table').append("<tr class='tableData'><td>" + ii + "</td><td>" + thisDate + "</td><td>$" + totalForThisDate.toFixed(2) + "</td></tr>");
					ii++;
					total += totalForThisDate;
					evaluatedDates.push(thisDate);
				}

			}
			$('table').append("<tr class='tableData'><td>" + "<b>Total</b>" + "</td><td><b>$" + total.toFixed(2) + "</b></td></tr>");
		},
		error: function(xhr, status) {
			if (xhr.status == '409') {
				alert("Sorry, your start date and end date are not in the right order. Try again. ");
				window.close();
			}
			console.log('error in generateStandardReport, xhr : ' + xhr + '\nstatus: ' + status);
		}
	});
}