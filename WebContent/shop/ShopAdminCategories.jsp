<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="beans.shop.ShopUser"%>
<%@page import="beans.shop.ShopUser.Role"%>
<jsp:useBean id="loggedUser" class="beans.shop.ShopUser" scope="session"/>
<%
//DEBUG uncomment this
if (loggedUser != null && loggedUser.getRole() == Role.user) {
loggedUser = (ShopUser) new ShopUser();
}

if (request.getSession().getAttribute("loggedUser") == null || loggedUser.getUserName().equals(""))
	response.sendRedirect("ShopAdminLogin.jsp");
%>
<jsp:useBean id="imagePathOnUser" class="java.lang.String" scope="session"/>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin categories</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script src="../jQuery/jquery-1.11.0.js"></script>

	<!-- Bootstrap core CSS and JS-->
	<link href="../bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="../bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

	<!-- Dependencies for calendar-for-bootstrap -->
	<link href="../jquery-ui-1.11.1.custom/jquery-ui.min.css" rel="stylesheet">
	<script src="../jquery-ui-1.11.1.custom/jquery-ui.js"></script>

	<!-- Custom styles for this template -->
	<link href="css/navbar-top-fixed.css" rel="stylesheet">

	<!-- Aditional css -->
	<link href="css/areas.css" rel="stylesheet">

	<!-- CRUD operations by Ajax -->
	<script src="js/operations.js"></script>

	<!-- New script -->
	<script type="text/javascript">
		window.contextPath = "${pageContext.request.contextPath}";

		$(document).ready(function() {

	    //username in navbar adjust
	    if ($('div#userBean').text().trim() == '') {
	    	$('li#loggedUser').addClass('hidden');
	    	$('li#logOutInNavbar').addClass('hidden');
	    } else {
	    	$('li#loggedUser').removeClass('hidden');
	    	$('li#loggedUser a b').append(" " + $('div#userBean').text().trim());
	    	$('li#logOutInNavbar').removeClass('hidden');
	    }

	    $('div#categoryTable tbody').on('click', '.delete', function(event){
	    	var decison = confirm("Are you sure you want to delete category: " + $(this).attr("value") + " ?");
	    	if (decison == true){
	    		$.ajax({
	    			url: window.contextPath + "/StoreServlet" + '?' + $.param({"ajaxId": '3', "toDelete" : $(this).attr("value")}),
	    			type: "delete",
	    			success: function(data){
	    				populateCategoryTable();
	    			},    
	    			error: function(status) { 
	    			}
	    		});
	    	}
	    });

	    $('div#categoryTable tbody').on('click', '.details', function(){
			$('#editModal.modal.in').modal('show'); //detailsModal
			populateCategoryDetails($(this).attr("value"));

			$('div#editModal div#searchSubcategories').off('click', '.btn');
			$('div#editModal div#searchSubcategories').on('click', '.btn', function() {
				event.preventDefault(); // otherwise closing modal is performed
				searchSubCategoriesByName($('div#editModal #searchQuery').val());
			});
			$('div#editModal div#searchSubcategories #searchQuery').off('input');
			$('div#editModal div#searchSubcategories #searchQuery').on('input', function(){
				searchSubCategoriesByName($(this).val());
			});

			$('div#editModal div.modal-footer').off('click', '.detailsCancel'); // $(div.modal-footer').off('click', '.detailsCancel');
			$('div#editModal div.modal-footer').on('click', '.detailsCancel', function(){
				$('#editModal.modal.in').modal('hide');
			});

			$('div#editModal div.modal-footer').off('click', '.detailsEdit'); // $('div.modal-footer').off('click', '.detailsEdit');
			$('div#editModal div.modal-footer').on('click', '.detailsEdit', function(){
				processFormEditCategory();
				$('#editModal.modal.in').modal('hide');
			});
		});

$('div#showAllCategoriesTotalSearch').on('click', '.btn', function(){
	$('div#categoryTable table tbody tr').each(function (index, element) {
		$(this).show();
	});
	clearCategorySearchArea();
});

$('div#totalSearchName').on('input', ':text', function(){
	totalSearchCategories($(this).val(), $('div#totalSearchDescription input').val());
});

$('div#totalSearchDescription').on('input', ':text', function(){
	totalSearchCategories($('div#totalSearchName input').val(), $(this).val());
});

$('div#ChoooseSubcategoriesTableTotalSearch tbody').on('change', ':checkbox', function(){
	totalSearchCategories($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val());
});

$('div#totalSearchSearchSubcategories #searchQuery').on('input', function(){
	searchSubcategoriesByNameTotalSearch($(this).val());
});

$('div.searchArea form.form-horizontal div#addCategory').on('click', '.addNewItem', function(e){

			e.preventDefault(); // presume - this button(in form) have predefined behaviour wich is in conflict with modal default behaviour		 	
			$('#addModal.modal.in').modal('show');

			clearFieldsAddCategory();
			populateChooseSubcategoriesTableAddCategory();

			$('div#addModal div#searchSubcategories').off('click', '.btn');
			$('div#addModal div#searchSubcategories').on('click', '.btn', function() {
				event.preventDefault(); // otherwise closing modal is performed
				searchCategoryByNameAddCategory($('div#addModal #searchQuery').val());
			});
			$('div#addModal div#searchSubcategories #searchQuery').off('input');
			$('div#addModal div#searchSubcategories #searchQuery').on('input', function(){
				searchCategoryByNameAddCategory($(this).val());
			});

			$('div#addModal div.modal-footer').off('click', '.addCancel');
			$('div#addModal div.modal-footer').on('click', '.addCancel', function(){
				$('div#addModal.modal.in').modal('hide');
			});

			$('div#addModal div.modal-footer').off('click', '.addAdd');
			$('div#addModal div.modal-footer').on('click', '.addAdd', function(){
				processFormAddCategory();
				$('div#addModal.modal.in').modal('hide');
			});

			// validation of form

			$("div#addModal div#itemName").on('input', ':text', function(){
				validateInputAddCategoryNameFieldReal();
			});

			//  END validation of form
		});

		// report modal
		$('li.chartLi a.reportButton').on('click', function(){
			$('div#reportModal.modal').modal('show');

			$('div#reportModal.modal input#startDateStandard').datepicker({ });
			$('div#reportModal.modal input#endDateStandard').datepicker({ });
			$('div#reportModal.modal input#startDateCategory').datepicker({ });
			$('div#reportModal.modal input#endDateCategory').datepicker({ });

			//fill category sellect
			fillCategoriesSelectInReportModal();

			$('div#reportModal div.modal-footer').off('click', '.reportCancel');
			$('div#reportModal div.modal-footer').on('click', '.reportCancel', function(){
				$('div#reportModal.modal.in').modal('hide');
			});

			$('div#reportModal').off('click', 'button.generateStandardReport');
			$('div#reportModal').on('click', 'button.generateStandardReport', function(event){
				event.preventDefault();
				var startDate = $('div#reportModal.modal input#startDateStandard').datepicker("getDate");
				var endDate = $('div#reportModal.modal input#endDateStandard').datepicker("getDate");
				var sStartDate = startDate.getMonth()+1 + '-' + startDate.getDate() + '-' + startDate.getFullYear();
				var sEndDate = endDate.getMonth()+1 + '-' + endDate.getDate() + '-' + endDate.getFullYear();
				window.open('reports/standardReport.jsp?startDate=' + sStartDate + '&endDate=' + sEndDate + '&category=');
			});

			$('div#reportModal').off('click', 'button.generateCategoryReport');
			$('div#reportModal').on('click', 'button.generateCategoryReport', function(event){
				event.preventDefault();
				var startDate = $('div#reportModal.modal input#startDateCategory').datepicker("getDate");
				var endDate = $('div#reportModal.modal input#endDateCategory').datepicker("getDate");
				var sStartDate = startDate.getMonth()+1 + '-' + startDate.getDate() + '-' + startDate.getFullYear();
				var sEndDate = endDate.getMonth()+1 + '-' + endDate.getDate() + '-' + endDate.getFullYear();
				var sCategory = $('div#reportModal.modal div#categorySelectDiv select#categorySelect').val();
				window.open('reports/categoryReport.jsp?startDate=' + sStartDate + '&endDate=' + sEndDate + '&category=' + sCategory);
			});
		});

});


</script>

<style type="text/css">
	/* for fancy image choose button */
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}
	
	#itemDescription textarea {
		height: 100%;
		width: 100%;
	}
	#itemSubcategories textarea {
		height: 100%;
		width: 100%;
	}

	li.chartLi {
		background-color: #D3D3D0;
	}

	div#verticalAlignIt {
		vertical-align: bottom;
	}

</style>

</head>
<body onload="populateCategoryTable(); populateChooseSubcategoriesTableTotalSearch()">
	<!-- Navigation bar -->
	<div class="navbar-wrapper">
		<div class="container">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><i><b>Small Shop</b></i></a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li><a href="ShopAdminIndex.jsp">Device</a></li>
							<li><a href="ShopAdminComponents.jsp">Component</a></li>
							<li class="active"><a href="#">Category</a></li>
							<li class="chartLi"><a class="btn btn-link reportButton" data-toggle='modal'><b>Generate report</b></a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="" id="loggedUser">
								<a>Logged in as:<b></b></a>
								<div id="userBean" hidden><%= loggedUser.getUserName() %></div>
							</li>
							<li class="" id="logOutInNavbar">
								<a href="../ShopLoginServlet?loggedUser=<%= loggedUser.getUserName() %>&logoff=logoff">Log out</a>
							</li>
						</ul>
					</div><!--/.nav-collapse -->

				</div>
			</div>
		</div> <!-- container end -->
	</div> <!-- /Navigation bar -->

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3 col-lg-2">
				<div class="searchArea">
					<!-- Search form total search -->
					<form class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-9" id="addCategory"> 
								<button class="btn btn-primary addNewItem" data-toggle='modal' data-target='#addModal'>Add</button>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-9">Search below</label>
						</div>
						<div class="form-group">
							<div class="col-xs-2" id="showAllCategoriesTotalSearch"> 
								<button class="btn btn-mini">Show all</button>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-10" id="totalSearchName">
								<input type="text" class="span2" placeholder='category name'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="totalSearchDescription">
								<input type="text" class="span2" placeholder='category description'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-9">Subcategories</label>
						</div>
						<div class="form-group">
							<div class="col-xs-9" id="totalSearchSearchSubcategories">
								<input id='searchQuery' type="text" class="span2" placeholder='search name...'>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseSubcategoriesTableTotalSearch">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form> <!-- /Search form total search -->
				</div>
			</div>
			<div class="col-sm-6 col-md-9 col-lg-10">
				<div class="viewArea">
					<div id="categoryTable">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div> <!-- /container -->

	<!-- Modal dialog for details -->
	<div id="editModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-10" id="itemName">
								<p class="form-control-static"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="itemDescription">
								<textarea rows="10" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="itemSubcategories" class="control-label col-xs-3">Subcategories</label>
							<div class="col-xs-9" id="itemSubcategories">
								<textarea rows="10" readonly ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="searchSubcategories" class="control-label col-xs-2">Search</label>
							<div class="col-xs-10" id="searchSubcategories">
								<div class="input-append">
									<input id='searchQuery' type="text" class="span2" placeholder='subcategory name'>
									<button class="btn btn-info btn-small">Search...</button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseSubcategoriesTable">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger detailsCancel">Cancel</button>
					<button class="btn btn-success detailsEdit">Edit</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for details -->


	<!-- Modal dialog for add device -->
	<div id="addModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"><b>Add new category</b></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-8" id="itemName">
								<input type="text" class="span2" placeholder='category name'>
							</div>
							<span id='nameError' class='error' hidden='true'><p>Name already exists</p></span>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="itemDescription">
								<textarea rows="10" placeholder='category description'></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="categories" class="control-label col-xs-6">Categories</label>
						</div>
						<div class="form-group">
							<label for="searchSubcategories" class="control-label col-xs-2">Search</label>
							<div class="col-xs-10" id="searchSubcategories">
								<div class="input-append">
									<input id='searchQuery' type="text" class="span2" placeholder='subcategory name'>
									<button class="btn btn-info btn-small">Search...</button>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseSubcategoriesTable">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger addCancel">Cancel</button>
					<button class="btn btn-success addAdd" disabled='true'>Add</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for add device -->

	<!-- Modal dialog for generate report -->
	<div id="reportModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader">Generating report</h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="standardReport" class="control-label col-xs-6">Standart report</label>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">From:</label>
							<div class="col-xs-8" id="startDateStandardDiv">
								<input id='startDateStandard' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">To:</label>
							<div class="col-xs-8" id="endDateStandardDiv">
								<input id='endDateStandard' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-6" id="generateStandardReport">
								<button class="btn btn-info btn-small generateStandardReport">Generate</button>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label for="categoryReport" class="control-label col-xs-6">Category report</label>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">From:</label>
							<div class="col-xs-8" id="startDateCategoryDiv">
								<input id='startDateCategory' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-xs-4">To:</label>
							<div class="col-xs-8" id="endDateCategoryDiv">
								<input id='endDateCategory' type="text" readonly="true">
							</div>
						</div>
						<div class="form-group" id="verticalAlignIt">
							<label for="" class="control-label col-xs-4">Category:</label>
							<div class="col-xs-8" id="categorySelectDiv" >
								<select id='categorySelect'readonly="true"> </select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-6" id="generateCategoryReport">
								<button class="btn btn-info btn-small generateCategoryReport">Generate</button>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger reportCancel">Cancel</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for generate report -->

</body>
</html>