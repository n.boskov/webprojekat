<!DOCTYPE html>
<%@page import="beans.shop.ShopUser"%>
<%@page import="beans.shop.ShopUser.Role"%>
<jsp:useBean id="loggedUser" class="beans.shop.ShopUser" scope="session"/>
<%
if (loggedUser != null && loggedUser.getRole() == Role.admin) {
loggedUser = (ShopUser) new ShopUser();
}
%>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>SmallShop Homepage</title>

	<script src="../jQuery/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/shop-homepage.css" rel="stylesheet">

	<!-- Operations on  user side-->
	<script src="js/operationsUser.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<link href="css/areas.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        	window.contextPath = "${pageContext.request.contextPath}";

        	$(document).ready(function(){
        		
        		//username in navbar adjust
        		if ($('div#userBean').text().trim() == '') {
        			$('li#loggedUser').addClass('hidden');
        			$('li#logOutInNavbar').addClass('hidden');
        			$('li#loginFormNavbar').removeClass('hidden');
        		} else {
        			$('li#loggedUser').removeClass('hidden');
        			$('li#loggedUser a b').append(" " + $('div#userBean').text().trim());
        			$('li#logOutInNavbar').removeClass('hidden');
        			$('li#loginFormNavbar').addClass('hidden');
        		}

        		$('div#thumbnailsArea').on('click', '.details', function(){
        			$('#editModal.modal.in').modal('show'); //detailsModal
        			populateComponentDetails($(this).html());

        			$('div#editModal div.modal-footer').off('click', '.detailsCancel');
        			$('div#editModal div.modal-footer').on('click', '.detailsCancel', function(){
        				$('#editModal.modal.in').modal('hide');
        			});

        			$('div#editModal div.modal-footer').off('click', '.detailsAddToChart');
        			$('div#editModal div.modal-footer').on('click', '.detailsAddToChart', function(){
        				addComponentToChart($('div#editModal div#itemName p').text());
        				$('#editModal.modal.in').modal('hide');
        			});
        		});

        		$('div#carousel-example-generic div.carousel-caption a.btn').on('click', function(){
        			$('#editModal.modal.in').modal('show'); //detailsModal
        			populateComponentDetails($(this).val());

        			$('div#editModal div.modal-footer').off('click', '.detailsCancel');
        			$('div#editModal div.modal-footer').on('click', '.detailsCancel', function(){
        				$('#editModal.modal.in').modal('hide');
        			});

        			$('div#editModal div.modal-footer').off('click', '.detailsAddToChart');
        			$('div#editModal div.modal-footer').on('click', '.detailsAddToChart', function(){
        				addComponentToChart($('div#editModal div#itemName p').text());
        				$('#editModal.modal.in').modal('hide');
        			});
        		});

        		// search area
        		$('div#showAllComponentsTotalSearch').on('click', '.btn', function(){
        			$('div#thumbnailsArea div.aThumbnail').each(function (index, element) {
        				$(this).show();
        			});
        			clearComponentSearchArea();
        		});

        		$('div#totalSearchName').on('input', ':text', function(){
        			totalSearchComponents($(this).val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());

        		});

        		$('div#totalSearchDescription').on('input', ':text', function(){
        			totalSearchComponents($('div#totalSearchName input').val(), $(this).val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());
        		});

        		$('div#totalSearchPriceRange > select').on('change', function(){
        			totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $(this).val(), $('div#totalSearchAmountRange select').val());
        		});

        		$('div#totalSearchAmountRange > select').on('change', function(){
        			totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $(this).val());
        		});

        		$('div#ChoooseCategoryTableTotalSearch tbody').on('change', ':checkbox', function(){
        			totalSearchComponents($('div#totalSearchName input').val(), $('div#totalSearchDescription input').val(), $('div#totalSearchPriceRange select').val(), $('div#totalSearchAmountRange select').val());
        		});

        		$('div#totalSearchSearchCategory #searchQuery').on('input', function(){
        			searchCategoriesByNameTotalSearch($(this).val());
        		});

        		// end search area

        		// your chart
        		$('li.chartLi a.chartButton').on('click', function(){
        			if ($('div#userBean').text() == '') {
        				alert('Please first log in.');
        				return;
        			} else {
        				$('div#yourChartModal.modal').modal('show');
        				populateChartModal();
        			}

        			$('div#yourChartModal div#articlesTable tbody').off('click', 'button.removeChartItem');
        			$('div#yourChartModal div#articlesTable tbody').on('click', 'button.removeChartItem', function(event){
        				event.preventDefault();
        				var decison = confirm("Are you sure you want to remove " + $(this).prop("value") + " from your chart ?");
        				if (decison == true){
        					deleteItemFromUserChart($(this).prop('id'), $(this).prop("value"));
        				}
        			});

        			$('div#yourChartModal div.modal-footer').off('click', '.yourChartCancel');
        			$('div#yourChartModal div.modal-footer').on('click', '.yourChartCancel', function(){
        				$('div#yourChartModal.modal.in').modal('hide');
        			});

        			$('div#yourChartModal div.modal-footer').off('click', '.yourChartBuy');
        			$('div#yourChartModal div.modal-footer').on('click', '.yourChartBuy', function(){
        				processBuyingForm();
        				$('div#yourChartModal.modal.in').modal('hide');
        			});
        		});

        		// login dropdown
        		$('.dropdown-toggle').dropdown();
        		$('.dropdown-menu').find('form').click(function (e) {
        			e.stopPropagation();
        		});

        	});

</script>

<style type="text/css">

	#itemDescription textarea {
		height: 100%;
		width: 100%;
	}

	div.thumbnail > img {
		width: 320px;
		height: 220px;
	}

	div.carousel-inner img {
		opacity: 0.4;
		filter: alpha(opacity=40); /* For IE8 and earlier */
	}

	div.carousel-inner div.carousel-caption {
		color: black;
	}

	div.carousel-inner img.slide-image {
		width: 800px;
		height: 300px;
	}

	li.chartLi {
		background-color: #D3D3D0;
	}

</style>
</head>

<body onload="populateThumbnails(); populateChooseCategoryTableTotalSearch();">

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Small Shop</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-left">
					<li class="chartLi"><a class="btn btn-link chartButton" data-toggle='modal'><b>Your chart</b></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="" id="loggedUser">
						<a>Logged in as:<b></b></a>
						<div id="userBean" hidden><%= loggedUser.getUserName() %></div>
					</li>
					<li class="dropdown" id="loginFormNavbar">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#loginFormNavbar">
							Login
							<b class="caret"></b>
						</a>
						<div class="dropdown-menu">
							<form style="margin: 0px" accept-charset="UTF-8" action="../ShopLoginServlet" method="post">
								<fieldset class='textbox' style="padding:10px">
									<input style="margin-top: 8px" type="text" placeholder="User name" name="userName" />
									<input style="margin-top: 8px" type="password" placeholder="Passsword" name="password" />
									<p><input type="checkbox" name="remember" />Remember me<br /></p>
									<input type="hidden" name="isAdmin" value="user">
									<hr><input class="btn-primary" name="commit" type="submit" value="Log In" />
									<a href="signUps/signUpUser.jsp">Sign in</a>
								</fieldset>
							</form>
						</div>
					</li>
					<li class="" id="logOutInNavbar">
						<a href="../ShopLoginServlet?loggedUser=<%= loggedUser.getUserName() %>&logoff=logoff">Log out</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-md-3">
				<p class="lead">Our offer:</p>
				<div class="list-group">
					<a href="" class="list-group-item active">Components</a>
					<a href="IndexDevices.jsp" class="list-group-item">Devices</a>
				</div>

				<div class="searchArea">
					<!-- Search form total search -->
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-xs-9 col-sm-8 col-md-8 col-lg-8">Search below</label>
						</div>
						<div class="form-group">
							<div class="col-xs-2 col-sm-4 col-md-4 col-lg-4" id="showAllComponentsTotalSearch"> 
								<button class="btn btn-mini">Show all</button>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="control-label col-xs-2 col-sm-4 col-md-4 col-lg-4">Name</label>
							<div class="col-xs-10 col-sm-8 col-md-8 col-lg-8" id="totalSearchName">
								<input type="text" class="span2" placeholder='component name'>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2 col-sm-4 col-md-4 col-lg-4">Description</label>
							<div class="col-xs-10 col-sm-8 col-md-8 col-lg-8" id="totalSearchDescription">
								<input type="text" class="span2" placeholder='component description'>
							</div>
						</div>
						<div class="form-group">
							<label for="priceRange" class="control-label col-xs-2 col-sm-4 col-md-4 col-lg-4">Price range</label>
							<div class="col-xs-10 col-sm-8 col-md-8 col-lg-8" id="totalSearchPriceRange">
								<select class="combobox input-large" name="normal">
									<option value="">no selection</option>
									<option value="r1">0 - 300 &#36;</option>
									<option value="r2">300 - 600 &#36;</option>
									<option value="r3">600 - 900 &#36;</option>
									<option value="r4">900 - 1200 &#36;</option>
									<option value="r5">1200+ &#36;</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="amountRange" class="control-label col-xs-2 col-sm-4 col-md-4 col-lg-4">Amount range</label>
							<div class="col-xs-10 col-sm-8 col-md-8 col-lg-8" id="totalSearchAmountRange">
								<select class="combobox input-large" name="normal">
									<option value="">no selection</option>
									<option value="r1">0 - 10</option>
									<option value="r2">10 - 20</option>
									<option value="r3">20 - 50</option>
									<option value="r4">50+</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2 col-sm-4 col-md-4 col-lg-4">Category</label>
						</div>
						<div class="form-group">
							<div class="col-xs-9 col-sm-10 col-md-10 col-lg-10" id="totalSearchSearchCategory">
								<input id='searchQuery' type="text" class="span2" placeholder='search name...'>
							</div>
						</div>
						<div class="form-group">
							<div id="ChoooseCategoryTableTotalSearch">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Name</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form> <!-- /Search form total search -->
				</div> <!-- /Search area -->

			</div>

			<div class="col-md-9">

				<div class="row carousel-holder">

					<div class="col-md-12">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-example-generic" data-slide-to="1"></li>
								<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="item active" id="item1">
									<img class="slide-image" src="http://placehold.it/800x300" alt="">
									<div class="carousel-caption">
										<h1></h1>
										<p class="lead"></p>
										<a class="btn btn-large btn-primary"  data-toggle='modal' data-target='#editModal'>Details</a>
									</div>
								</div>
								<div class="item" id="item2">
									<img class="slide-image" src="http://placehold.it/800x300" alt="">
									<div class="carousel-caption">
										<h1></h1>
										<p class="lead"></p>
										<a class="btn btn-large btn-primary"  data-toggle='modal' data-target='#editModal'>Details</a>
									</div>
								</div>
								<div class="item" id="item3">
									<img class="slide-image" src="http://placehold.it/800x300" alt="">
									<div class="carousel-caption">
										<h1></h1>
										<p class="lead"></p>
										<a class="btn btn-large btn-primary"  data-toggle='modal' data-target='#editModal'>Details</a>
									</div>
								</div>
							</div>
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>

				</div>

				<div class="row" id="thumbnailsArea">

				</div>

			</div>

		</div>

	</div>
	<!-- /.container -->

	<div class="container">

		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="col-lg-10">
					<p><b>This is prototype.</b></p>
					<a href="ShopAdminLogin.jsp">Log in as administrator</a>
				</div>
				<div class="col-lg-2">
					<p>Novak Boškov, 2014.</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

	<!-- Modal dialog for details -->
	<div id="editModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="control-label col-xs-2">Name</label>
							<div class="col-xs-10" id="itemName">
								<p class="form-control-static"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="itemAmount" class="control-label col-xs-2">Amount</label>
							<div class="col-xs-10" id="itemAmount">
								<p class="form-control-static"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="itemPrice" class="control-label col-xs-2">Price(&#36;)</label>
							<div class="col-xs-10" id="itemPrice">
								<p class="form-control-static"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="itemLink" class="control-label col-xs-2">Link</label>
							<div class="col-xs-10" id="itemLink">
								<p class="form-control-static"><a></a></p>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="control-label col-xs-2">Description</label>
							<div class="col-xs-10" id="itemDescription">
								<textarea rows="10" readonly></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="image" class="control-label col-xs-2">Image</label>
							<div class="col-xs-8" id="itemImage">
								<img src="" class="img-rounded img-responsive" alt="Rounded Image">
							</div>
						</div>
						<div class="form-group">
							<label for="itemCategory" class="control-label col-xs-2">Category</label>
							<div class="col-xs-10" id="itemCategory">
								<input type="text" placeholder='no category' readonly>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger detailsCancel">Cancel</button>
					<button class="btn btn-success detailsAddToChart" disabled="true">Add to chart</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for details -->

	<!-- Modal dialog for your chart -->
	<div id="yourChartModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3 id="modalHeader"></h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal">
						<div class="form-group">
							<div id="articlesTable">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Article type</th>
											<th>Name</th>
											<th>Count</th>
											<th>Unit price (&#36;)</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</form>

				</div>

				<div class="modal-footer">
					<button class="btn btn-danger yourChartCancel">Cancel</button>
					<button class="btn btn-success yourChartBuy" disabled="true">Buy</button>
				</div>

			</div>
		</div>
	</div> <!-- end Modal dialog for your chart -->

</body>

</html>
