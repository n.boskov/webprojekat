<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin login page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="../html5-boilerplate-4.3.0/css/normalize.css">
        <link rel="stylesheet" href="../html5-boilerplate-4.3.0/css/main.css">
        <script src="../html5-boilerplate-4.3.0/js/vendor/modernizr-2.6.2.min.js"></script>
        
        <link rel="stylesheet" href="additionalStyles.css">
</head>
<body id="LoginBody">
        <!--[if lt IE 7]>
           <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
       <![endif]-->

       <!-- Add your site or application content here -->
    <div id="loginDiv">
    	<p><b>This is the admin log in page. Please fill required fields below to log in.</b></p>
	    <form action="../ShopLoginServlet" method="POST">
	   	<div id="uNamePass">
		<p>User name:&nbsp;<span class="field"><input type="text" name="userName" /><br /> </span></p>
		<p>Password:&nbsp;<span class="field"><input type="password" name="password" /><br /> </span></p>
		</div>
		<p><input type="checkbox" name="remember" />Remember me<br /></p>
		<input type="hidden" name="isAdmin" value="admin">
		<input type="submit" value="Log in"/>
		</form>
	</div>
	
	<div id="loginAdminDiv">
		<a href="ShopLogin.jsp">Log in as standard user</a>
	</div>

       <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
       <script>window.jQuery || document.write('<script src="../html5-boilerplate-4.3.0/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
       <script src="../html5-boilerplate-4.3.0/js/plugins.js"></script>
       <script src="../html5-boilerplate-4.3.0/js/main.js"></script>

       <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
       <script>
           (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
           function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
           e=o.createElement(i);r=o.getElementsByTagName(i)[0];
           e.src='//www.google-analytics.com/analytics.js';
           r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
           ga('create','UA-XXXXX-X');ga('send','pageview');
       </script>
</body>
</html>