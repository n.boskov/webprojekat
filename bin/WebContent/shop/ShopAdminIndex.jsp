<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<%
if (request.getSession().getAttribute("loggedUser") == null)
	response.sendRedirect("ShopAdminLogin.jsp");
%>
<jsp:useBean id="imagePathOnUser" class="java.lang.String" scope="session"/>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin index</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="../html5-boilerplate-4.3.0/css/normalize.css">
        <link rel="stylesheet" href="../html5-boilerplate-4.3.0/css/main.css">
        <script src="../html5-boilerplate-4.3.0/js/vendor/modernizr-2.6.2.min.js"></script>
        
        <link rel="stylesheet" href="additionalStyles.css">
        
        
<script src="../jQuery/jquery-1.11.0.js"></script>
<script>
// this changes to true if any edit is clicked
window.editIsClicked = false;
    
function bodyOnLoad() {
        if ($("#hiddenImagePath").val() !== ""){
            $("#addComponentDiv").css("visibility", "visible");
            window.location.hash = "#addComponentDiv"
            alert("Update of " + $("#hiddenImagePath").val() + " is done.");
            $("#imageFile").hide();
            $("#submitUpload").hide();
        }
    
        $("#componentsTableDiv").hide();
        $("#categorySearchDiv").hide();
        $("#CategoryTableDiv").hide();
        $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "1"},
            dataType: "json",
            success: function(data) {
                $("table#deviceTable").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    $("table#deviceTable").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<button id='device' value='" + data[i].name + "' class='edit'>Edit</button>" + "</td>" + "<td>"+ "<button id='device' value='" + data[i].name + "' class='delete'>Delete</button>" + "</td>" + "</tr>");
                }
            },
            error: function(status) {
                //Do something  
            }
        });
        }
    
$(document).ready(function(){
    $("#compDevSelect").change(function() {
        if ($("#compDevSelect").val() === "component") {
            updateComponentTable();
            $("#deviceTableDiv").hide();
            $("#CategoryTableDiv").hide();
            
            $("#componentsTableDiv").show();
            
            fillCategorySelection();
            $("#categorySearchDiv").show();
            
        } else if($("#compDevSelect").val() === "device"){
            bodyOnLoad();
            $("#componentsTableDiv").hide();
            $("#deviceTableDiv").show();
        } else if($("#compDevSelect").val() === "category"){
            updateCategoryTable();
            $("#componentsTableDiv").hide();
            $("#deviceTableDiv").hide();
            $("#categorySearchDiv").hide();
            
            $("#searchDiv").show();
            $("#searchDiv input#name").hide();
            $("#choosePriceDiv").hide();
            $("#searcRepealButtins").hide();

            $("#CategoryTableDiv").show();
        }
    });
    
    // post on server new component and update table if success
    window.addButtonOnClickInComponentDiv = function(){
         var sAjaxId = "2";
         if (window.editIsClicked === true){
             sAjaxId = "21";
         }
         $.ajax({
            url:"${pageContext.request.contextPath}/StoreServlet",
            type: "post",
            data:{
                    jsonData:JSON.stringify({
                        name:$("#addComponentFieldName").val(),
                        price:$("#addComponentFieldPrice").val(),
                        amount:$("#addComponentFieldAmount").val(),
                        description:$("#addComponentFieldDescription").val(),
                        category:null,
                        categoryName: $("#categorySelectAddComponent").val(),
                        link:$("#addComponentFieldLink").val(),
                        imagePath:$("#hiddenImagePath").val()
                    }),
                    ajaxId: sAjaxId

                  },
             success: function(data,status){
                alert(status);
                $("span#componentNameError").css("visibility", "hidden");
                alert("Component is successfully added");
                updateComponentTable();
              },
             error: function (xhr, status){
                 alert(status);
                 $("span#componentNameError").css("visibility", "visible");
             }
        });
        
        //to avoid every add to be edit
        window.editIsClicked = false;
        
        //updateComponentTable();
    }
    
    // post on server new device and update table if success
    window.addButtonOnClickInDeviceDiv = function(){
         var sAjaxId = "1";
         if (window.editIsClicked === true){
             sAjaxId = "11";
         }
        var compNamesToAdd = [];
        $("table#choseComponentAddDeviceDiv td input:checked").each(function(){
            compNamesToAdd.push($(this).val());
        });
         $.ajax({
            url:"${pageContext.request.contextPath}/StoreServlet",
            type: "post",
            data:{
                    jsonData:JSON.stringify({
                        name:$("#addDeviceFieldName").val(),
                        description:$("#addDeviceFieldDescription").val(),
                        components:null
                    }),
                    componentNames:JSON.stringify(compNamesToAdd),
                    ajaxId: sAjaxId

                  },
             success: function(data,status){
                alert("Device is successfully added");
                updateDeviceTable();
              },
             error: function(xhr, status){
                 $("span#deviceNameError").css("visibility", "visible");
             }
        });
        
        //to avoid every add to be edit
        window.editIsClicked = false;
    }
    
    // post on server new category and update table if success
    window.addButtonOnClickInCategoryDiv = function(){
         var sAjaxId = "3";
         if (window.editIsClicked === true){
             sAjaxId = "31";
         }
        var catNamesToAdd = [];
        $("table#choseSubCategoriesinAddCategoriesDiv td input:checked").each(function(){
            catNamesToAdd.push($(this).val());
        });
        //debug catNamesToAdd
         $.ajax({
            url:"${pageContext.request.contextPath}/StoreServlet",
            type: "post",
            data:{
                    jsonData:JSON.stringify({
                        name:$("#addCategoryFieldName").val(),
                        description:$("#addCategoryFieldDescription").val(),
                        subCategories:null
                    }),
                    categoryNames:JSON.stringify(catNamesToAdd),
                    ajaxId: sAjaxId

                  },
             success: function(data,status){
                alert("Category is successfully added");
                updateCategoryTable();
              },
             error: function(xhr, status){
                 $("span#categoryNameError").css("visibility", "visible");
             }
        });
        
        //to avoid every add to be edit
        window.editIsClicked = false;
    }
    
    function updateComponentTable(){
            $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "2"},
            dataType: "json",
            success: function(data) {
                $("table#componentsTable").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    $("table#componentsTable").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].price + "</td>" + "<td>" + data[i].amount + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + data[i].categoryName + "</td>" + "<td>" + "<a href='" + data[i].link + "'>" + data[i].link + "</a>" + "</td>" + "<td>" + "<img src = '" + data[i].imageFile + "' />" + "</td>"+ "<td>" + "<button id='component' value='" + data[i].name + "' class='edit'>Edit</button>" + "</td>" + "<td>"+ "<button id='component' value='" + data[i].name + "' class='delete'>Delete</button>" + "</td>" + "</tr>");
                    alert(data[i].imageFile);
                }
            },
            error: function(status) {
                //Do something  
            }
            });
            
            //$("#deviceTableDiv").hide();
            //$("#componentsTableDiv").show();
            
            //fillCategorySelection();
            //$("#categorySearchDiv").show();
            
        //fillCategorySelection() was here
        
    }
    
    function updateDeviceTable(){
        $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "1"},
            dataType: "json",
            success: function(data) {
                $("table#deviceTable").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    $("table#deviceTable").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<button id='device' value='" + data[i].name + "' class='edit'>Edit</button>" + "</td>" + "<td>"+ "<button id='device' value='" + data[i].name + "' class='delete'>Delete</button>" + "</td>" + "</tr>");
                }
            },
            error: function(status) {
                //Do something  
            }
        });
    }
    
    function updateCategoryTable(){
        $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "3"},
            dataType: "json",
            success: function(data) {
                $("table#categoryTable").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    function extractSubCategories(){
                        if (data[i].subCategories !== null){
                            for(var ii = 0; ii<data[i].subCategories.length; ii++){
                                $("table#categoryTable select").append("<option>" + data[i].subCategories[ii].name + "</option>");
                            }
                        }
                    }
                    $("table#categoryTable").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + "<select class='subcategories'>" + "</select>" + "</td>" + "<td>" + "<button id='category' value='" + data[i].name + "' class='edit'>Edit</button>" + "</td>" + "<td>"+ "<button id='category' value='" + data[i].name + "' class='delete'>Delete</button>" + "</td>" + "</tr>");
                    extractSubCategories();
                }
            },
            error: function(status) {
                //Do something  
            }
        });
    }
    
    function fillCategorySelection(){
        $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "3"},
            dataType: "json",
            success: function(data){
                $(".categorySelect").find("option").remove();
                for (var i = 0; i<data.length; i++) {
                    //$(".categorySearchDiv").children("#categorySelect")
                    $(".categorySelect").append("<option value='" + data[i].name + "'>" + data[i].name + "</option>");
                }
            },    
            error: function(status) {
                //Do something  
            }
        });
    }
    
    //any delete clicked in any table
    $("button.delete").click(function(){
        if ($(this).attr("id") === "component"){
            //send name of component to delete
            var decison = confirm("Are you sure you want to delete component: " + $(this).attr("value") + " ?");
            if (decison == true){
                $.ajax({
                    url: "${pageContext.request.contextPath}/StoreServlet",
                    type: "delete",
                    data: {ajaxId: "2", toDelete: $(this).attr("value")},
                    success: function(data){
                        updateComponentTable();
                    },    
                    error: function(status) { 
                    }
                });
            }
            
        } else if ($(this).attr("id") === "device"){
            //send name of device to delete
            var decison = confirm("Are you sure you want to delete device: " + $(this).attr("value") + " ?");
            if (decison == true){
                $.ajax({
                    url: "${pageContext.request.contextPath}/StoreServlet",
                    type: "delete",
                    data: {ajaxId: "1", toDelete: $(this).attr("value")},
                    success: function(data){
                        updateDeviceTable();
                    },    
                    error: function(status) { 
                    }
                });
            }
            
        } else if ($(this).attr("id") === "category") {
            //send name of category to delete
            var decison = confirm("Are you sure you want to delete category: " + $(this).attr("value") + " ?");
            if (decison == true){
                $.ajax({
                    url: "${pageContext.request.contextPath}/StoreServlet",
                    type: "delete",
                    data: {ajaxId: "3", toDelete: $(this).attr("value")},
                    success: function(data){
                        updateCategoryTable();
                    },    
                    error: function(status) { 
                    }
                });
            }
        }
    });
    
    //any edit clicked in eny table
    $("button.edit").click(function(){
        window.editIsClicked = true;
        
        if ($(this).attr("id") === "device"){
            $("#addDeviceDiv").css("visibility", "visible");
            window.location.hash = "#addDeviceDiv";
            
            $.ajax({
                url: "${pageContext.request.contextPath}/StoreServlet",
                type: "get",
                data: {ajaxId: "11"},
                dataType: "json",
                success: function(data) {
                    // puni text area
                    $("#addDeviceFieldDescription").val(data.description);
                    fillChooseCompInAddDeviceDiv();
                    // cekira odgovarajuce komponente, jedan samo data se ocekuje, ne data[]
                    for (var i = 1; i<data.components.length; i++){
                        $("table#choseComponentAddDeviceDiv td input").filter(function(){return this.value == data.components[i].name}).attr("checked", true);
                    }
                },
                error: function(status) {
                    //Do something  
                }
            });
            
            //name is unique - cannot be changed
            $("#addDeviceDiv input#addDeviceFieldName").hide();
            
        } else if ($(this).attr("id") === "component"){
            $("#addComponentDiv").css("visibility", "visible");
            window.location.hash = "#addComponentDiv";
            
            $.ajax({
                url: "${pageContext.request.contextPath}/StoreServlet",
                type: "get",
                data: {ajaxId: "21"},
                dataType: "json",
                success: function(data) {
                    $("addComponentFieldPrice").val(data.price);
                    $("addComponentFieldAmount").val(data.amount);
                    $("addComponentFieldDescription").val(data.description);
                    $("addComponentFieldLink").val(data.link);
                    $("form#uploadForm #imageFile").attr("value", data.imagePath);
                    fillCategorySelection();
                    $("#addComponentDiv select.categorySelect option").filter(function(){return this.value == data.categoryName}).attr("selected", "selected");
                    
                },
                error: function(status) {
                    //Do something  
                }
            });
            
            //name is unique - cannot be changed
            $("#addComponentDiv input#addComponentFieldName").hide();
            
        } else if ($(this).attr("id") === "category"){
            $("#addCategoryDiv").css("visibility", "visible");
            window.location.hash = "#addCategoryDiv";
            
            $.ajax({
                url: "${pageContext.request.contextPath}/StoreServlet",
                type: "get",
                data: {ajaxId: "31"},
                dataType: "json",
                success: function(data) {
                    $("#addCategoryFieldDescription").val(data.description);
                    fillChoseSubCategoriesinAddCategoriesDiv();
                    for (var i = 0; i<data.subCategories.length; i++){
                        $("table#choseSubCategoriesinAddCategoriesDiv td input").filter(function(){return this.value == data.subCategories[i].name}).attr("checked", true);
                    }
                },
                error: function(status) {
                    //Do something  
                }
            });
            
            //name is unique - cannot be changed
            $("#addCategoryDiv input#addCategoryFieldName").hide();
            
        }
    });
    
});

// this only fill category select in search span
function fillCategorySelection(){
    $.ajax({
        url: "${pageContext.request.contextPath}/StoreServlet",
        type: "get",
        data: {ajaxId: "3"},
        dataType: "json",
        success: function(data){
            $(".categorySelect").find("option").remove();
            for (var i = 0; i<data.length; i++) {
                //$(".categorySearchDiv").children("#categorySelect")
                $(".categorySelect").append("<option value='" + data[i].name + "'>" + data[i].name + "</option>");
            }
        },    
        error: function(status) {
            //Do something  
        }
});
}    
    
function AddSpanAddComponentClick(){
    $("#addComponentDiv").css("visibility", "visible");
    window.location.hash = "#addComponentDiv";
    fillCategorySelection();
}

function AddSpanAddDeviceClick(){
    $("#addDeviceDiv").css("visibility", "visible");
    window.location.hash = "#addDeviceDiv";
    fillChooseCompInAddDeviceDiv();
}
    
function AddSpanAddCategoryClick(){
    $("#addCategoryDiv").css("visibility", "visible");
    window.location.hash = "#addDeviceDiv";
    fillChoseSubCategoriesinAddCategoriesDiv();
}
    
function fillChoseSubCategoriesinAddCategoriesDiv(){
            $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "3"},
            dataType: "json",
            success: function(data) {
                $("table#choseSubCategoriesinAddCategoriesDiv").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    $("table#choseSubCategoriesinAddCategoriesDiv").append("<tr>" + "<td>" + data[i].name + "</td>" + data[i].description + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
                }
            },
            error: function(status) {
                //Do something  
            }
            });
}
    
function fillChooseCompInAddDeviceDiv(){
            $.ajax({
            url: "${pageContext.request.contextPath}/StoreServlet",
            type: "get",
            data: {ajaxId: "2"},
            dataType: "json",
            success: function(data) {
                $("table#choseComponentAddDeviceDiv").find("td").remove();
                for(var i = 0; i<data.length; i++) {
                    $("table#choseComponentAddDeviceDiv").append("<tr>" + "<td>" + data[i].name + "</td>" + "<td>" + data[i].price + "</td>" + "<td>" + data[i].amount + "</td>" + "<td>" + data[i].description + "</td>" + "<td>" + data[i].categoryName + "</td>" + "<td>" + "<a href='" + data[i].link + "'>" + data[i].link + "</a>" + "</td>" + "<td>" + "<img src = '" + data[i].imagePath + "' />" + "</td>" + "<td>" + "<input type='checkbox' class='checkBoxCol' value='" + data[i].name + "'>" + "</td>" + "</tr>");
                }
            },
            error: function(status) {
                //Do something  
            }
            });
}
    
</script>

</head>
<body onload="bodyOnLoad()">
        <!--[if lt IE 7]>
           <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
       <![endif]-->
       
       <!-- Add your site or application content here -->
       <span id="AddSpan">
       <button type="button" id="addComponentButton" onclick="AddSpanAddComponentClick()">Add component</button>
       <button type="button" id="addDeviceButton" onclick="AddSpanAddDeviceClick()">Add device</button>
       <button type="button" id="addCategoryButton" onclick="AddSpanAddCategoryClick()">Add category</button>
       </span>
       
       <span id="reportSpan">
       <a href="" >Generate report</a>
       </span>
       
       <div id="searchDiv">
       <select id="compDevSelect">
           <option value="device">device</option>
	       <option value="component">component</option>
           <option value="category">category</option>
       </select>
       <div id="categorySearchDiv" class="categorySearchDiv">
       <select id="categorySelect" class="categorySelect">
	       <!-- Load from js script if component is selected -->
       </select>
       </div>
       <input type="text" id="name" name="name">
       <div id="choosePriceDiv">
       <input type="number" id="minPrice" name="minPrice">
       <input type="number" id="maxPrice" name="maxPrice">
       </div>
       <div id="searcRepealButtins">
       <button type="button" id="searchButton" >Search</button>
       <button type="button" id="repealButton" >Repeal</button>
       </div>
       </div>
       
       <div id="deviceTableDiv">
       <table border="1" id="deviceTable">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Change</th>
            <th>Delete</th>
        </tr>
       	<!-- Fill this from js script -->
       </table>
       </div>
    
       <div id="componentsTableDiv">
       <table border="1" id="componentsTable">
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Amount</th>
            <th>Description</th>
            <th>Categories</th>
            <th>Link</th>
            <th>Image</th>
            <th>Change</th>
            <th>Delete</th>
        </tr>
       	<!-- Fill this from js script -->
       </table>
       </div>
    
       <div id="CategoryTableDiv">
       <table border="1" id="categoryTable">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Subcategories</th>
            <th>Change</th>
            <th>Delete</th>
        </tr>
       	<!-- Fill this from js script -->
       </table>
       </div>
       
        <div id="addComponentDiv" class="addDiv">
            <p><b>Add component section:</b></p>
            <p>
            <form id="uploadForm" action="../UploadPicServlet" method="post" enctype="multipart/form-data">
                <input id="imageFile" type="file" name="pic" accept="image/*">
                <input id="submitUpload" type="submit" value="Upload">
                <input id="hiddenImagePath" type="hidden" value="${imagePathOnUser}">
                <p>Image for this component is stored at: ${imagePathOnUser}</p>
            </form>
            </p>
            <p>Name:<input type="text" id="addComponentFieldName">
            <span class="error" id="componentNameError" style="color:red">Component with such name already exists!</span></p>
            <p>Price:<input type="number" step="any" id="addComponentFieldPrice"></p>
            <p>Amount:<input type="number" id="addComponentFieldAmount"></p>
            <p>Description:<textarea rows="4" cols="50" id="addComponentFieldDescription"></textarea></p>
            <p>Link:<input type="text" id="addComponentFieldLink"></p>
            <p>Choose Category:
            <div id="categoryAddComponent" class="categorySearchDiv">
               <select id="categorySelectAddComponent" class="categorySelect">
                   <!-- Load from js script if component is selected -->
               </select>
            </div>
            </p>
            <button type="button" id="addComponentButton" onclick="addButtonOnClickInComponentDiv()">Add</button>
        </div>
    
        <div id="addDeviceDiv" class="addDiv">
            <p><b>Add device section:</b></p>
            <p>Name:<input type="text" id="addDeviceFieldName">
            <span class="error" id="deviceNameError" style="color:red">Device with such name already exists!</span></p>
            <p>Description:<textarea rows="4" cols="50" id="addDeviceFieldDescription"></textarea></p>
            <p>
            <div id = "choseComponentTableDiv">
            <table border="1" id="choseComponentAddDeviceDiv">
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Description</th>
                <th>Categories</th>
                <th>Link</th>
                <th>Image</th>
                <th>Choose</th>
            </tr>
            <!-- Fill this from js script -->
            </table>
            </div>
            </p>
            <button type="button" id="addDeviceButton" onclick="addButtonOnClickInDeviceDiv()">Add</button>
        </div>
    
        <div id="addCategoryDiv" class="addDiv">
            <p><b>Add category section:</b></p>
            <p>Name:<input type="text" id="addCategoryFieldName">
            <span class="error" id="categoryNameError" style="color:red">Category with such name already exists!</span></p>
            <p>Description:<textarea rows="4" cols="50" id="addCategoryFieldDescription"></textarea></p>
            <p>
            <div id = "choseSubCategoriesTableDiv">
            <table border="1" id="choseSubCategoriesinAddCategoriesDiv">
            <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
            <!-- Fill this from js script -->
            </table>
            </div>
            </p>
            <button type="button" id="addCategoryButton" onclick="addButtonOnClickInCategoryDiv()">Add</button>
        </div>
    
    
       
       <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
       <script>window.jQuery || document.write('<script src="../html5-boilerplate-4.3.0/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
       <script src="../html5-boilerplate-4.3.0/js/plugins.js"></script>
       <script src="../html5-boilerplate-4.3.0/js/main.js"></script>

       <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
       <script>
           (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
           function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
           e=o.createElement(i);r=o.getElementsByTagName(i)[0];
           e.src='//www.google-analytics.com/analytics.js';
           r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
           ga('create','UA-XXXXX-X');ga('send','pageview');
       </script>
</body>
</html>