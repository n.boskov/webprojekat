package beans.shop;

import java.io.Serializable;

public class ShopUser implements Serializable {
	public static enum Role {admin, user, none}
	
	private String userName;
	private String password;
	private String name;
	private String surname;
	private Role role;
	private String phoneNumber;
	private String email;
	
	public ShopUser() {
		// TODO Auto-generated constructor stub
		this.userName = "";
		this.password = "";
		this.name = "";
		this.surname = "";
		this.role = Role.none;
		this.phoneNumber = "";
		this.email = "";
	}

	public ShopUser(String userName, String password, String name,
			String surname, Role role, String phoneNumber, String email) {
		super();
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.role = role;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ShopUser [userName=" + userName + ", password=" + password
				+ ", name=" + name + ", surname=" + surname + ", role=" + role
				+ ", phoneNumber=" + phoneNumber + ", email=" + email + "]";
	}
	
}
