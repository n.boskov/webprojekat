package beans.shop;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopCategory implements Serializable {
	
	private String name;
	private String description;
	private ArrayList<ShopCategory> subCategories;

	public ShopCategory() {
		// TODO Auto-generated constructor stub
		this.name = "";
		this.description = "";
		this.subCategories = null;
	}

	public ShopCategory(String name, String description,
			ArrayList<ShopCategory> subCategories) {
		super();
		this.name = name;
		this.description = description;
		this.subCategories = subCategories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<ShopCategory> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(ArrayList<ShopCategory> subCategories) {
		this.subCategories = subCategories;
	}

	@Override
	public String toString() {
		return "ShopCategory [name=" + name + ", description=" + description
				+ ", subCategories=" + subCategories + "]";
	}
	

}
