package beans.shop;

import java.io.Serializable;

public class ShopTax implements Serializable {
	
	private String defaultTaxNameValue = "";
	
	private String taxName;
	private Double taxShare;
	private Double taxAmount;

	public ShopTax() {
		// TODO Auto-generated constructor stub
		this.taxName = "";
		this.taxShare = 0.0;
		this.taxAmount = 0.0;
	}

	public ShopTax(Double taxShare, Double priceOfProduct) {
		super();
		this.taxName = defaultTaxNameValue;
		this.taxShare = taxShare;
		calcualateTax(priceOfProduct);
	}
	
	public ShopTax(String taxName, Double taxShare, Double priceOfProduct) {
		super();
		this.taxName = taxName;
		this.taxShare = taxShare;
		calcualateTax(priceOfProduct);
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public Double getTaxShare() {
		return taxShare;
	}

	public void setTaxShare(Double taxShare) {
		this.taxShare = taxShare;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	public void calcualateTax(double amount) {
		this.taxAmount = amount * taxShare;
	}

	@Override
	public String toString() {
		return "ShopTax [defaultTaxNameValue=" + defaultTaxNameValue
				+ ", taxName=" + taxName + ", taxShare=" + taxShare
				+ ", taxAmount=" + taxAmount + "]";
	}

	
}
