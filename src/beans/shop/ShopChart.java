package beans.shop;

import java.io.Serializable;
import java.util.HashMap;

public class ShopChart implements Serializable {

	private HashMap<ShopComponent, Integer> components;
	private HashMap<ShopDevice, Integer> devices;
	private HashMap<ShopDevice, Integer> generatedDevices;
	ShopUser user;

	public ShopChart() {
		components = new HashMap<ShopComponent, Integer>();
		devices = new HashMap<ShopDevice, Integer>();
		generatedDevices = new HashMap<ShopDevice, Integer>();
	}

	public ShopChart(HashMap<ShopComponent, Integer> componets,
			HashMap<ShopDevice, Integer> devices,
			HashMap<ShopDevice, Integer> generatedDevices) {
		this.components = componets;
		this.devices = devices;
		this.generatedDevices = generatedDevices;
	}

	public HashMap<ShopComponent, Integer> getComponents() {
		return components;
	}

	public void setComponents(HashMap<ShopComponent, Integer> components) {
		this.components = components;
	}

	public HashMap<ShopDevice, Integer> getDevices() {
		return devices;
	}

	public void setDevices(HashMap<ShopDevice, Integer> devices) {
		this.devices = devices;
	}

	public ShopUser getUser() {
		return user;
	}

	public void setUser(ShopUser user) {
		this.user = user;
	}

	public HashMap<ShopDevice, Integer> getGeneratedDevices() {
		return generatedDevices;
	}

	public void setGeneratedDevices(
			HashMap<ShopDevice, Integer> generateddevices) {
		this.generatedDevices = generateddevices;
	}

}
