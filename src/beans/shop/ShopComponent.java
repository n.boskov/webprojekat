package beans.shop;

import java.awt.Image;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class ShopComponent implements Serializable {
	
	private String name;
	private Double price;
	private Integer amount;
	private String description;
	private ArrayList<ShopCategory> category;
	private String categoryName;
	private String link;
	private String imagePath;
	private String imageFile;
	private String imageExtension;
	private ArrayList<String> categoryNames;

	public ShopComponent() {
		// TODO Auto-generated constructor stub
		this.name = "";
		this.price = 0.0;
		this.amount = 0;
		this.description = "";
		this.category = null;
		this.categoryName = "";
		this.link = "";
		this.imagePath = "";
		this.imageExtension = "";
		this.imageFile = "";
		this.categoryNames = new ArrayList<String>();
	}

	public ShopComponent(String name, Double price, Integer amount,
			String description, ArrayList<ShopCategory> category, String categoryName, String link,
			String imagePath) {
		super();
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.description = description;
		this.category = category;
		this.categoryName = categoryName;
		this.link = link;
		this.imagePath = imagePath;
		generateCategoryNames();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<ShopCategory> getCategory() {
		generateCategoryNames();
		return category;
	}

	public void setCategory(ArrayList<ShopCategory> category) {
		this.category = category;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	public String getImageExtension() {
		return imageExtension;
	}

	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	
	public ArrayList<String> getCategoryNames() {
		return categoryNames;
	}

	public void setCategoryNames(ArrayList<String> categoryNames) {
		categoryNames = categoryNames;
	}

	@Override
	public String toString() {
		return "ShopComponent [name=" + name + ", price=" + price + ", amount="
				+ amount + ", description=" + description + ", category="
				+ category + ", categoryName=" + categoryName + ", link="
				+ link + ", imagePath=" + imagePath + ", imageFile="
				+ "a big string" + ", imageExtension=" + imageExtension
				+ ", CategoryNames=" + categoryNames + "]";
	}

	private void generateCategoryNames(){
		System.out.println("DEBUG generateCategoryNames");
		ArrayList<String> retVal = new ArrayList<String>();
		for (ShopCategory categ : category) {
			retVal.add(categ.getName().trim());
		}
		this.categoryNames = retVal;
		System.out.println("DEBUG KRAJ generateCategoryNames");
	}

}
