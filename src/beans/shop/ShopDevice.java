package beans.shop;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopDevice implements Serializable {

	private String name;
	private String description;
	private ArrayList<ShopComponent> components;
	
	public ShopDevice() {
		// TODO Auto-generated constructor stub
		this.name = "";
		this.description = "";
		this.components = null;
	}

	public ShopDevice(String name, String description,
			ArrayList<ShopComponent> components) {
		super();
		this.name = name;
		this.description = description;
		this.components = components;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<ShopComponent> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<ShopComponent> components) {
		this.components = components;
	}

	@Override
	public String toString() {
		return "ShopDevice [name=" + name + ", description=" + description
				+ ", components=" + components + "]";
	}

}
