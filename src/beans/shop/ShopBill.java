package beans.shop;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ShopBill implements Serializable {
	
	private HashMap<ShopComponent, Integer> components;
	private HashMap<ShopDevice, Integer> devices;
	private HashMap<ShopDevice, Integer> generatedDevices;
	private ShopTax tax;
	private Double totalPrice;
	private Date dateOfPurchase;
	private String dateOfPurchaseString;
	private ShopUser userOfPurchase;

	public ShopBill() {
		// TODO Auto-generated constructor stub
		this.components = null;
		this.devices = null;
		this.generatedDevices = null;
		this.tax = null;
		this.totalPrice = 0.0;
		this.dateOfPurchase = null;
		this.userOfPurchase = null;
	}

	public ShopBill(HashMap<ShopComponent, Integer> components,
			HashMap<ShopDevice, Integer> devices, HashMap<ShopDevice, Integer> generatedDevices, ShopTax tax, Double totalPrice,
			Date dateOfPurchase, ShopUser userOfPurchase) {
		super();
		this.components = components;
		this.devices = devices;
		this.generatedDevices = generatedDevices;
		this.tax = tax;
		this.totalPrice = totalPrice;
		this.dateOfPurchase = dateOfPurchase;
		this.dateOfPurchaseString = convertDateToString(dateOfPurchase);
		this.userOfPurchase = userOfPurchase;
	}
	
	public ShopBill(HashMap<ShopComponent, Integer> components,
			HashMap<ShopDevice, Integer> devices, HashMap<ShopDevice, Integer> generatedDevices, ShopTax tax,
			Date dateOfPurchase, ShopUser userOfPurchase) {
		super();
		this.components = components;
		this.devices = devices;
		this.generatedDevices = generatedDevices;
		this.tax = tax;
		this.totalPrice = calculateTotalPrice();
		this.dateOfPurchase = dateOfPurchase;
		this.dateOfPurchaseString = convertDateToString(dateOfPurchase);
		this.userOfPurchase = userOfPurchase;
	}

	public HashMap<ShopComponent, Integer> getComponents() {
		return components;
	}

	public void setComponents(HashMap<ShopComponent, Integer> components) {
		this.components = components;
	}

	public HashMap<ShopDevice, Integer> getDevices() {
		return devices;
	}

	public void setDevices(HashMap<ShopDevice, Integer> devices) {
		this.devices = devices;
	}

	public ShopTax getTax() {
		return tax;
	}

	public void setTax(ShopTax tax) {
		this.tax = tax;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getDateOfPurchase() {
		return dateOfPurchase;
	}

	public void setDateOfPurchase(Date dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	public ShopUser getUserOfPurchase() {
		return userOfPurchase;
	}

	public void setUserOfPurchase(ShopUser userOfPurchase) {
		this.userOfPurchase = userOfPurchase;
	}
	
	public HashMap<ShopDevice, Integer> getGeneratedDevices() {
		return generatedDevices;
	}

	public void setGeneratedDevices(HashMap<ShopDevice, Integer> generatedDevices) {
		this.generatedDevices = generatedDevices;
	}

	public String getDateOfPurchaseString() {
		return dateOfPurchaseString;
	}

	public void setDateOfPurchaseString(String dateOfPurchaseString) {
		this.dateOfPurchaseString = dateOfPurchaseString;
	}
	
	@Override
	public String toString() {
		return "ShopBill [components=" + components + ", devices=" + devices
				+ ", generatedDevices=" + generatedDevices + ", tax=" + tax
				+ ", totalPrice=" + totalPrice + ", dateOfPurchase="
				+ dateOfPurchase + ", dateOfPurchaseString="
				+ dateOfPurchaseString + ", userOfPurchase=" + userOfPurchase
				+ "]";
	}

	public double calculateTotalPrice() {
		double totalPrice = 0.0;
		
		for (ShopComponent aComponent : components.keySet()) {
			totalPrice += aComponent.getPrice() * components.get(aComponent);
		}
		
		for (ShopDevice aDevice : devices.keySet()) {
			double pricePerUnit = 0;
			for (ShopComponent aComponent : aDevice.getComponents()) {
				pricePerUnit += aComponent.getPrice();
			}
			totalPrice += devices.get(aDevice) * pricePerUnit;
		}
		
		System.out.println("DEBUG racunam total price");
		for (ShopDevice aDevice : generatedDevices.keySet()) {
			double pricePerUnit = 0;
			for (ShopComponent aComponent : aDevice.getComponents()) {
				pricePerUnit += aComponent.getPrice();
			}
			totalPrice += generatedDevices.get(aDevice) * pricePerUnit;
		}
		
		return totalPrice;
	}
	
	private String convertDateToString(Date date) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		return df.format(date);
	}

}
