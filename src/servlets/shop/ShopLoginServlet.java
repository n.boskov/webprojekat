package servlets.shop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.shop.ShopChart;
import beans.shop.ShopUser;
import beans.shop.ShopUser.Role;

import validators.EmailValidator;

/**
 * Servlet implementation class ShopLoginServlet
 */
public class ShopLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShopLoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		try {
			super.init(config);
		} catch (ServletException e) {
			e.printStackTrace();
		}

		// TODO da bi svi servleti videli registeredUsers
		ArrayList<ShopUser> registeredUsers = new ArrayList<ShopUser>();
		ShopUser defaultAdmin = new ShopUser("admin", "123", "Novak", "Boskov",
				Role.admin, "062/222 222", "mail@mail.com");
		ShopUser defaultAdmin2 = new ShopUser("admin2", "1234", "Novak",
				"Boskov", Role.admin, "062/222 222", "mail@mail.com");
		registeredUsers.add(defaultAdmin);
		registeredUsers.add(defaultAdmin2);
		ShopUser defaultUser = new ShopUser("user", "123", "Novak", "Boskov",
				Role.user, "062/222 222", "mail@mail.com");
		ShopUser defaultUser2 = new ShopUser("user2", "123", "Novak", "Boskov",
				Role.user, "062/222 222", "mail@mail.com");
		registeredUsers.add(defaultUser);
		registeredUsers.add(defaultUser2);

		ArrayList<ShopChart> shopCharts = new ArrayList<ShopChart>();

		ServletContext initctx = getServletContext();
		initctx.setAttribute("registeredUsers", registeredUsers);
		initctx.setAttribute("shopCharts", shopCharts);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ShopUser loggedUser = new ShopUser();
		if (request.getParameter("logoff") != null) {
			ArrayList<ShopUser> users = (ArrayList<ShopUser>) getServletContext()
					.getAttribute("registeredUsers");
			for (ShopUser aUser : users) {
				if (aUser.getUserName().trim()
						.equals(request.getParameter("loggedUser").trim())) {
					loggedUser = aUser;
					break;
				}
			}
		} else {
			loggedUser = (ShopUser) request.getAttribute("loggedUser");
		}
		// TODO patch
		request.getSession().setAttribute("loggedUser", loggedUser);

		// only when user going to log out
		if (request.getParameter("logoff") != null
				&& loggedUser.getRole() == Role.user) {
			// begore user log out save its userChart to shopCharts
			System.out.println("DEBUG logging out in progress");
			if (loggedUser != null) {
				ArrayList<ShopChart> shopCharts = (ArrayList<ShopChart>) getServletContext()
						.getAttribute("shopCharts");
				ShopChart userChart = (ShopChart) request.getSession()
						.getAttribute("userChart");
				ShopChart chartToRemove = new ShopChart();
				for (ShopChart aChart : shopCharts) {
					if (aChart.getUser().getUserName().trim()
							.equals(userChart.getUser().getUserName().trim())) {
						chartToRemove = aChart;
						break;
					}
				}
				shopCharts.remove(chartToRemove);
				shopCharts.add(userChart);
				getServletContext().setAttribute("shopCharts", shopCharts);

				request.getSession().invalidate();
				clearCookie(request, response); // DEBUG
			}

			clearCookie(request, response);

			if (loggedUser.getRole() == Role.admin) {
				response.sendRedirect("shop/ShopAdminLogin.jsp");
			} else if (loggedUser.getRole() == Role.user) {
				response.sendRedirect("shop/");
			}
			return;
		}

		// if admin going to log out
		if (request.getParameter("logoff") != null
				&& loggedUser.getRole() == Role.admin) {
			request.getSession().invalidate();
			clearCookie(request, response);
		}

		Cookie[] cookies = request.getCookies();
		String userIdentificationKey = null;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("userIdentificationKey")) {
					userIdentificationKey = cookies[i].getValue();
					break;
				}
			}
		}

		if (loggedUser == null && userIdentificationKey != null
				&& !userIdentificationKey.equals("-1")) {
			ServletContext ctx = getServletContext();
			ArrayList<ShopUser> registeredUsers = (ArrayList<ShopUser>) ctx
					.getAttribute("registeredUsers");
			for (ShopUser aUser : registeredUsers) {
				if (aUser.getUserName().equals(userIdentificationKey)) {
					request.getSession().setAttribute("loggedUser", aUser);
					break;
				}
			}
		}

		// update this reference in case the code above was changed loggedUser
		// in session
		loggedUser = (ShopUser) request.getSession().getAttribute("loggedUser");

		if (loggedUser != null) {
			if (loggedUser.getRole() == Role.admin)
				response.sendRedirect("shop/ShopAdminIndex.jsp");
			else if (loggedUser.getRole() == Role.user) {
				// TODO make shopChart for the user
				ArrayList<ShopChart> shopCharts = (ArrayList<ShopChart>) getServletContext()
						.getAttribute("shopCharts");
				ShopChart userChart = new ShopChart();
				boolean haveChart = false;
				for (ShopChart aChart : shopCharts) {
					if (aChart.getUser().getUserName().trim()
							.equals(loggedUser.getUserName().trim())) {
						userChart = aChart;
						haveChart = true;
						break;
					}
				}
				if (!haveChart) {
					userChart.setUser(loggedUser);
				}
				request.getSession().setAttribute("userChart", userChart);

				response.sendRedirect("shop/");
			}
		} else {
			response.sendRedirect("shop/ShopLogin.jsp");
			request.setAttribute("wrongParams", "wrongParams");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// if request is sign up, registration
		boolean itWasSignUp = false;
		if (request.getParameter("role") != null) {
			String role = request.getParameter("role");
			if (role.trim().equals("user") || role.trim().equals("admin")) {
				itWasSignUp = true;
			}
		}
		if (itWasSignUp) {
			doPut(request, response);
		} else {

			String userName = request.getParameter("userName");
			String password = request.getParameter("password");
			boolean remmember = request.getParameter("remember") != null;
			System.out.println("DEBUG remmember = " + remmember);

			ShopUser loggedUser = null;
			if (getServletContext().getAttribute("registeredUsers") != null) {
				ArrayList<ShopUser> registeredUsersAtribute = (ArrayList<ShopUser>) getServletContext()
						.getAttribute("registeredUsers");
				String isAdminStr = "";
				if (request.getParameter("isAdmin") != null) {
					isAdminStr = request.getParameter("isAdmin");
				}
				for (ShopUser aUser : registeredUsersAtribute) {
					if (isAdminStr.equals("admin")) {
						if (userName.equals(aUser.getUserName())
								&& aUser.getRole() == Role.admin) {
							loggedUser = aUser;
							break;
						}
					} else if (isAdminStr.equals("user")) {
						if (userName.equals(aUser.getUserName())
								&& aUser.getRole() == Role.user) {
							loggedUser = aUser;
							break;
						}
					}
				}
			}

			if ((loggedUser != null) && remmember) {
				String userIdentificationKey = loggedUser.getUserName();
				Cookie cookie = new Cookie("userIdentificationKey",
						userIdentificationKey);
				cookie.setMaxAge(365 * 24 * 60 * 60);
				response.addCookie(cookie);
			} else if ((loggedUser != null) && !remmember) {
				clearCookie(request, response);
			}
			if (loggedUser != null) {
				request.setAttribute("loggedUser", loggedUser);
			}
			doGet(request, response);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String role = request.getParameter("role").trim();
		String email = request.getParameter("reenteremail").trim();
		String firstName = request.getParameter("firstname").trim();
		String lastName = request.getParameter("lastname").trim();
		String username = request.getParameter("username").trim();
		String password = request.getParameter("password").trim();
		String phone = request.getParameter("telnumber").trim();

		ArrayList<ShopUser> registeredUsersAtribute = (ArrayList<ShopUser>) getServletContext()
				.getAttribute("registeredUsers");

		boolean dataOK = validateData(username, password, firstName, lastName, phone, email);
		if (!dataOK) {
			response.sendRedirect("shop/signUps/SingUpError.html");
			return;
		}

		if (role.equals("user")) {
			for (ShopUser user : registeredUsersAtribute) {
				if (user.getRole() == Role.user) {
					if (user.getUserName().equals(username)) {
						dataOK = false;
						break;
					}
				}
			}
			
			if (dataOK) {
				ShopUser newUser = new ShopUser(username, password, firstName,
						lastName, Role.user, phone, email);
				registeredUsersAtribute.add(newUser);
				getServletContext().setAttribute("registeredUsers", registeredUsersAtribute);
				response.sendRedirect("shop/index.jsp");
				return;
			} else {
				response.sendRedirect("shop/signUps/SingUpUsernameExists.html");
				return;
			}
		} else if (role.equals("admin")) {
			for (ShopUser user : registeredUsersAtribute) {
				if (user.getRole() == Role.admin) {
					if (user.getUserName().equals(username)) {
						dataOK = false;
						break;
					}
				}
			}
			
			if (dataOK) {
				ShopUser newUser = new ShopUser(username, password, firstName,
						lastName, Role.admin, phone, email);
				registeredUsersAtribute.add(newUser);
				getServletContext().setAttribute("registeredUsers", registeredUsersAtribute);
				response.sendRedirect("shop/ShopAdminLogin.jsp");
				return;
			} else {
				response.sendRedirect("shop/signUps/SingUpUsernameExists.html");
				return;
			}
		} else {
			// chnged hidden div
		}
	}

	private void clearCookie(HttpServletRequest request,
			HttpServletResponse response) {
		Cookie cookies[] = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				System.out.println(cookies[i].getName());
				if (cookies[i].getName().equals("userIdentificationKey")) {
					cookies[i].setValue("-1");
					response.addCookie(cookies[i]);
					break;
				}
			}
		}
	}

	private boolean validateData(String userName, String password,
			String firstName, String lastName, String phone, String email) {
		
		EmailValidator emailValidator = new EmailValidator();
		
		if (userName == null || userName.equals("")) {
			return false;
		}
		if(password == null || password.equals(""))
		{
			return false;
		}
		if (firstName == null || firstName.equals(""))
		{
			return false;
		}
		if (lastName == null || lastName.equals(""))
		{
			return false;
		}
		
		if (phone == null || phone.equals(""))
		{
			return false;
		}
		
		if (email == null || email.equals("") || emailValidator.validate(email) == false)
		{
			return false;
		}
		
		return true;
	}

}
