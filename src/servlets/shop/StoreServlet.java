package servlets.shop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.Request;

import beans.shop.ShopBill;
import beans.shop.ShopCategory;
import beans.shop.ShopChart;
import beans.shop.ShopComponent;
import beans.shop.ShopDevice;
import beans.shop.ShopTax;
import beans.shop.ShopUser;

import com.fasterxml.jackson.databind.ObjectMapper;


//import org.apache.commons.codec.binary.Base64;
import java.util.Base64;

/**
 * Servlet implementation class StoreServlet
 */
public class StoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StoreServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			super.init(config);
		} catch (ServletException e) {
			e.printStackTrace();
		}

		ArrayList<ShopCategory> shopCategories = new ArrayList<ShopCategory>();
		ArrayList<ShopComponent> shopComponents = new ArrayList<ShopComponent>();
		ArrayList<ShopDevice> shopDevices = new ArrayList<ShopDevice>();
		populateShop(shopCategories, shopComponents, shopDevices);

		ArrayList<ShopBill> shopBills = new ArrayList<ShopBill>();
		
		// testing
		Date date1 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "2-08-2014 10:20:56";
		try {
			date1 = sdf.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date date2 = new Date();
		String dateInString2 = "7-08-2014 10:20:56";
		try {
			date2 = sdf.parse(dateInString2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date date3 = new Date();
		String dateInString3 = "5-09-2014 10:20:56";
		try {
			date3 = sdf.parse(dateInString3);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date date4 = new Date();
		String dateInString4 = "10-09-2014 10:20:56";
		try {
			date4 = sdf.parse(dateInString4);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ShopBill bill1 = new ShopBill(new HashMap<ShopComponent, Integer>(), new HashMap<ShopDevice, Integer>(), new HashMap<ShopDevice, Integer>(), new ShopTax(), date1, new ShopUser());
		ShopBill bill2 = new ShopBill(new HashMap<ShopComponent, Integer>(), new HashMap<ShopDevice, Integer>(), new HashMap<ShopDevice, Integer>(), new ShopTax(), date2, new ShopUser());
		ShopBill bill3 = new ShopBill(new HashMap<ShopComponent, Integer>(), new HashMap<ShopDevice, Integer>(), new HashMap<ShopDevice, Integer>(), new ShopTax(), date3, new ShopUser());
		ShopBill bill4 = new ShopBill(new HashMap<ShopComponent, Integer>(), new HashMap<ShopDevice, Integer>(), new HashMap<ShopDevice, Integer>(), new ShopTax(), date4, new ShopUser());

		shopBills.add(bill1);
		shopBills.add(bill2);
		shopBills.add(bill3);
		shopBills.add(bill4);
		// end testing
		
		ServletContext ctx = getServletContext();
		ctx.setAttribute("shopDevices", shopDevices);
		ctx.setAttribute("shopComponents", shopComponents);
		ctx.setAttribute("shopCategories", shopCategories);
		ctx.setAttribute("shopBills", shopBills);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jsonRequest = request.getParameter("ajaxId");
		ObjectMapper mapper = new ObjectMapper();
		String ajaxId = mapper.readValue(jsonRequest, String.class);
		if (ajaxId.equals("1")) {
			// respond with devices
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");
			ObjectMapper mapper1 = new ObjectMapper();
			String sShopDevices = mapper1.writeValueAsString(shopDevices);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopDevices);
		} else if (ajaxId.equals("11")) {
			// responde with one device
			String nameToRespond = request.getParameter("toEdit");
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");
			ShopDevice deviceToRespond = null;
			for (ShopDevice aDevice : shopDevices) {
				if (aDevice.getName().trim().equals(nameToRespond.trim())) {
					deviceToRespond = aDevice;
					break;
				}
			}
			ObjectMapper mapper1 = new ObjectMapper();
			String sShopDevice = mapper1.writeValueAsString(deviceToRespond);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopDevice);
		} else if (ajaxId.equals("2")) {
			// respond with components
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");

			// fill components with image files to send it through json object
			for (ShopComponent component : shopComponents) {
				if (!component.getImagePath().equals("")) {
					component.setImageFile(makeBase64FromFile(new File(
							component.getImagePath())));
					component.setImageExtension(component.getImagePath()
							.replace(".", "~DOT~").split("~DOT~")[1]
							.toLowerCase());
				}
			}

			ObjectMapper mapper1 = new ObjectMapper();
			String sShopComponents = mapper1.writeValueAsString(shopComponents);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopComponents);
		} else if (ajaxId.equals("21")) {
			// responde with one component
			String nameToRespond = request.getParameter("toEdit");
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			ShopComponent componentToRespond = null;
			for (ShopComponent aComponent : shopComponents) {
				if (aComponent.getName().trim().equals(nameToRespond.trim())) {
					componentToRespond = aComponent;
					break;
				}
			}

			// fill component with image file to send it through json object
			if (!componentToRespond.getImagePath().equals("")) {
				componentToRespond.setImageFile(makeBase64FromFile(new File(
						componentToRespond.getImagePath())));
				componentToRespond.setImageExtension(componentToRespond
						.getImagePath().replace(".", "~DOT~").split("~DOT~")[1]
						.toLowerCase());
			}

			ObjectMapper mapper1 = new ObjectMapper();
			String sShopComponent = mapper1
					.writeValueAsString(componentToRespond);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopComponent);
		} else if (ajaxId.equals("3")) {
			// respond with categories
			ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
					.getAttribute("shopCategories");
			ObjectMapper mapper1 = new ObjectMapper();
			String sShopCategories = mapper1.writeValueAsString(shopCategories);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopCategories);
		} else if (ajaxId.equals("31")) {
			// responde with one category
			String nameToRespond = request.getParameter("toEdit");
			ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
					.getAttribute("shopCategories");
			ShopCategory categoryToRespond = null;
			for (ShopCategory aCategory : shopCategories) {
				if (aCategory.getName().trim().equals(nameToRespond.trim())) {
					categoryToRespond = aCategory;
					break;
				}
			}
			ObjectMapper mapper1 = new ObjectMapper();
			String sShopCategory = mapper1
					.writeValueAsString(categoryToRespond);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopCategory);
		} else if (ajaxId.equals("0")) {
			// respond with shopChart

			ArrayList<ShopChart> shopCharts = (ArrayList<ShopChart>) getServletContext()
					.getAttribute("shopCharts");
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");

			// ShopChart shopChartToRespond = new ShopChart();
			// for (ShopChart aChart : shopCharts) {
			// if
			// (aChart.getUser().getUserName().trim().equals(loggedUser.getUserName().trim()))
			// {
			// shopChartToRespond = aChart;
			// }
			// }

			System.out.println("DEBUG setChart.getComponents().size() = "
					+ userChart.getComponents().size());

			ObjectMapper mapper1 = new ObjectMapper();
			String sShopChart = mapper1.writeValueAsString(userChart);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopChart);

		} else if (ajaxId.equals("100")) {
			// respond with shopBill/s in range of given dates
						
			String sStartDate = request.getParameter("startDate");
			String sEndDate = request.getParameter("endDate");
			
			String sCategory = request.getParameter("category");
			ArrayList<ShopBill> shopBills = (ArrayList<ShopBill>) getServletContext()
					.getAttribute("shopBills");
			
	        Date startDate = new Date();
	        try {
	        	startDate = new SimpleDateFormat("MM/dd/yyyy").parse(sStartDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("DEBUG EXCEPTION sStartDate = " + sStartDate);
				e.printStackTrace();
			}
	        Date endDate = new Date();
	        try {
	        	endDate = new SimpleDateFormat("MM/dd/yyyy").parse(sEndDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("DEBUG EXCEPTION sEndDate = " + sEndDate);
				e.printStackTrace();
			}
	        
	        if (startDate.after(endDate)) {
	        	response.setStatus(HttpServletResponse.SC_CONFLICT);
	        	return;
	        }
	        
	        System.out.println("DEBUG DATE CHECKING startDate = " + startDate);
	        System.out.println("DEBUG DATE CHECKING endDate = " + endDate);
	        
	        ArrayList<ShopBill> shopBillsToRespond = new ArrayList<ShopBill>();
        	for (ShopBill aBill : shopBills) {
        		if (aBill.getDateOfPurchase().after(startDate) && aBill.getDateOfPurchase().before(endDate)) {
        			if (!shopBillsToRespond.contains(aBill))
        				shopBillsToRespond.add(aBill);
        		}
        	}
        	
        	ArrayList<ShopBill> shopBillsToRespondModified = new ArrayList<ShopBill>();
	        if (!sCategory.trim().equals("")) {
	        	// find all bills with componenets or device or generated device wich contains the category
	        	for (ShopBill aBill : shopBillsToRespond) {
	        		boolean matchedComponentFound = false;
	        		for (ShopComponent aComp : aBill.getComponents().keySet()) {
	        			for (ShopCategory aCateg : aComp.getCategory()) {
	        				if (aCateg.getName().trim().equals(sCategory.trim())) {
	        					if (!shopBillsToRespondModified.contains(aBill))
	        						shopBillsToRespondModified.add(aBill);
	        					matchedComponentFound = true;
	        					break;
	        				}
	        			}
	        		}
	        		
	        		boolean matchedDeviceFound = false;
	        		if (!matchedComponentFound) {
	        			for (ShopDevice aDevice : aBill.getDevices().keySet()) {
	        				for (ShopComponent aComp : aDevice.getComponents()) {
	        					for (ShopCategory aCateg : aComp.getCategory()) {
	    	        				if (aCateg.getName().trim().equals(sCategory.trim())) {
	    	        					if (!shopBillsToRespondModified.contains(aBill))
	    	        						shopBillsToRespondModified.add(aBill);
	    	        					matchedDeviceFound = true;
	    	        					break;
	    	        				}
	        					}
	        				}
	        			}
	        			
		        		if (!matchedDeviceFound) {
		        			for (ShopDevice aDevice : aBill.getGeneratedDevices().keySet()) {
		        				for (ShopComponent aComp : aDevice.getComponents()) {
		        					for (ShopCategory aCateg : aComp.getCategory()) {
		    	        				if (aCateg.getName().trim().equals(sCategory.trim())) {
		    	        					if (!shopBillsToRespondModified.contains(aBill))
		    	        						shopBillsToRespondModified.add(aBill);
		    	        					break;
		    	        				}
		        					}
		        				}
		        			}
		        		}
	        		}
	        		
	        	}
	        	
	        	shopBillsToRespond = shopBillsToRespondModified;
	        }
	        
			ObjectMapper mapper1 = new ObjectMapper();
			String sShopBillsToRespond = mapper1.writeValueAsString(shopBillsToRespond);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			out.write(sShopBillsToRespond);
			
		} else {
			// direct request

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jsonRequest = request.getParameter("jsonData");
		String ajaxId = request.getParameter("ajaxId");
		ObjectMapper mapper = new ObjectMapper();

		if (ajaxId.equals("1")) {
			// need to add device
			ShopDevice shopDevice = mapper.readValue(jsonRequest,
					ShopDevice.class);
			String componentNames = request.getParameter("componentNames");
			ArrayList<ShopComponent> components = makeComponentsList(componentNames);
			shopDevice.setComponents(components);
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");
			boolean nameAlreadyExists = false;
			for (ShopDevice aDevice : shopDevices) {
				if (aDevice.getName().trim()
						.equals(shopDevice.getName().trim())) {
					nameAlreadyExists = true;
					break;
				}
			}
			if (!nameAlreadyExists) {
				shopDevices.add(shopDevice);
				getServletContext().setAttribute("shopDevices", shopDevices);
			} else {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
			}

		} else if (ajaxId.equals("11")) {
			// need to add an updated device
			ShopDevice shopDevice = mapper.readValue(jsonRequest,
					ShopDevice.class);
			String componentNames = request.getParameter("componentNames");
			ArrayList<ShopComponent> components = makeComponentsList(componentNames);
			shopDevice.setComponents(components);
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");
			ShopDevice deviceToRemove = new ShopDevice();
			for (ShopDevice aDevice : shopDevices) {
				if (shopDevice.getName().trim()
						.equals(aDevice.getName().trim())) {
					deviceToRemove = aDevice;
					break;
				}
			}
			shopDevices.remove(deviceToRemove);
			shopDevices.add(shopDevice);
			getServletContext().setAttribute("shopDevices", shopDevices);

		} else if (ajaxId.equals("2")) {
			// need to add component
			ShopComponent shopComponent = mapper.readValue(jsonRequest,
					ShopComponent.class);
			String categoryNames = request.getParameter("categoryNames");
			shopComponent.setCategory(makeCategoriesList(categoryNames));
			// shopComponent.setCategory(makeCategoryList(shopComponent
			// .getCategoryName()));
			System.out.println("DEBUG Kategorija dodate comp : "
					+ shopComponent.getCategory());
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			boolean nameAlreadyExists = false;
			for (ShopComponent aComponent : shopComponents) {
				if (aComponent.getName().trim()
						.equals(shopComponent.getName().trim())) {
					nameAlreadyExists = true;
					break;
				}
			}
			if (!nameAlreadyExists) {
				shopComponents.add(shopComponent);
				getServletContext().setAttribute("shopComponents",
						shopComponents);
			} else {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
			}

		} else if (ajaxId.equals("21")) {
			// need to add an updated component
			ShopComponent shopComponent = mapper.readValue(jsonRequest,
					ShopComponent.class);
			String categoryNames = request.getParameter("categoryNames");
			shopComponent.setCategory(makeCategoriesList(categoryNames));
			// shopComponent.setCategory(makeCategoryList(shopComponent
			// .getCategoryName()));
			System.out.println("Kategorija menjane comp : "
					+ shopComponent.getCategory());
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			ShopComponent componentToRemove = new ShopComponent();
			for (ShopComponent aComponent : shopComponents) {
				if (shopComponent.getName().trim()
						.equals(aComponent.getName().trim())) {
					componentToRemove = aComponent;
				}
			}
			shopComponents.remove(componentToRemove);
			shopComponents.add(shopComponent);
			getServletContext().setAttribute("shopComponents", shopComponents);

		} else if (ajaxId.equals("3")) {
			// need to add category
			ShopCategory shopCategory = mapper.readValue(jsonRequest,
					ShopCategory.class);
			String categoryNames = request.getParameter("categoryNames");
			ArrayList<ShopCategory> categories = makeCategoriesList(categoryNames);
			shopCategory.setSubCategories(categories);
			ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
					.getAttribute("shopCategories");
			boolean nameAlreadyExists = false;
			for (ShopCategory aCategory : shopCategories) {
				if (aCategory.getName().trim()
						.equals(shopCategory.getName().trim())) {
					nameAlreadyExists = true;
					break;
				}
			}
			if (!nameAlreadyExists) {
				shopCategories.add(shopCategory);
				getServletContext().setAttribute("shopCategories",
						shopCategories);
			} else {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
			}

		} else if (ajaxId.equals("31")) {
			// need to add an updated category
			ShopCategory shopCategory = mapper.readValue(jsonRequest,
					ShopCategory.class);
			String categoryNames = request.getParameter("categoryNames");
			ArrayList<ShopCategory> categories = makeCategoriesList(categoryNames);
			shopCategory.setSubCategories(categories);
			System.out.println("DEBUG subctys od editovane cty su + "
					+ shopCategory.getSubCategories());
			ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
					.getAttribute("shopCategories");
			ShopCategory categoryToRemove = new ShopCategory();
			for (ShopCategory aCategory : shopCategories) {
				if (shopCategory.getName().trim()
						.equals(aCategory.getName().trim())) {
					categoryToRemove = aCategory;
				}
			}
			shopCategories.remove(categoryToRemove);
			shopCategories.add(shopCategory);
			getServletContext().setAttribute("shopCategories", shopCategories);

		} else if (ajaxId.equals("111")) {
			// add device to chart
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			if (userChart != null) {
				String deviceName = request.getParameter("deviceName");
				ShopDevice deviceToAdd = new ShopDevice();
				ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
						.getAttribute("shopDevices");
				for (ShopDevice aDevice : shopDevices) {
					if (deviceName.trim().equals(aDevice.getName().trim())) {
						deviceToAdd = aDevice;
						break;
					}
				}

				HashMap<ShopDevice, Integer> actualDevices = userChart
						.getDevices();
				if (actualDevices == null) {
					actualDevices = new HashMap<ShopDevice, Integer>();
				}
				actualDevices.put(deviceToAdd, new Integer(1));
				userChart.setDevices(actualDevices);
				System.out.println("DEBUG userChart.getDevices().size() = "
						+ userChart.getDevices().size());
				request.getSession().setAttribute("userChart", userChart);
			}

		} else if (ajaxId.equals("222")) {
			// add component to chart
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			if (userChart != null) {
				String compName = request.getParameter("compName");
				ShopComponent compToAdd = new ShopComponent();
				ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
						.getAttribute("shopComponents");
				for (ShopComponent aComponent : shopComponents) {
					if (compName.trim().equals(aComponent.getName().trim())) {
						compToAdd = aComponent;
						break;
					}
				}
				System.out.println("DEBUG compToAdd to chart name is "
						+ compToAdd.getName());

				HashMap<ShopComponent, Integer> actualComponents = userChart
						.getComponents();
				if (actualComponents == null) {
					actualComponents = new HashMap<ShopComponent, Integer>();
				}
				actualComponents.put(compToAdd, new Integer(1));
				userChart.setComponents(actualComponents);
				request.getSession().setAttribute("userChart", userChart);
			}

		} else if (ajaxId.equals("333")) {
			// add user generated device
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			if (userChart != null) {
				ShopUser loggedUser = (ShopUser) request.getSession()
						.getAttribute("loggedUser");
				String componentNames = request.getParameter("componentNames");
				String description = request.getParameter("description");
				ShopDevice deviceToAdd = new ShopDevice();
				deviceToAdd.setName(loggedUser.getUserName() + "~"
						+ new Date().toString());
				deviceToAdd.setComponents(makeComponentsList(componentNames));
				deviceToAdd.setDescription(description);

				HashMap<ShopDevice, Integer> actualDevices = userChart
						.getGeneratedDevices();
				if (actualDevices == null) {
					actualDevices = new HashMap<ShopDevice, Integer>();
				}

				System.out.println("DEBUG deviceToAdd.getName() = "
						+ deviceToAdd.getName());
				System.out.println("DEBUG deviceToAdd.getDescription() = "
						+ deviceToAdd.getDescription());
				System.out.println("DEBUG deviceToAdd.getComponents() = "
						+ deviceToAdd.getComponents());

				actualDevices.put(deviceToAdd, new Integer(1));
				userChart.setGeneratedDevices(actualDevices);
				request.getSession().setAttribute("userChart", userChart);

				ShopChart acc = (ShopChart) request.getSession().getAttribute(
						"userChart");
				System.out.println("DEBUG generatedDevices.size() = "
						+ acc.getGeneratedDevices().size());

			}

		} else if (ajaxId.equals("0")) {
			/*
			 * Make purchase. Make bill for administrative use & make update to
			 * the store to register reduction of the amount.
			 */
			ShopUser loggedUser = (ShopUser) request.getSession().getAttribute(
					"loggedUser");
			ShopChart chartInSession = (ShopChart) request.getSession()
					.getAttribute("userChart");
			String components = request.getParameter("components");
			String devices = request.getParameter("devices");
			String generatedDevices = request.getParameter("generatedDevices");
			HashMap<ShopComponent, Integer> componentsHM = makeComponentsHM(components);
			System.out.println("DEBUG componentsHM = " + componentsHM);
			HashMap<ShopDevice, Integer> devicesHM = makeDevicesHM(devices);
			System.out.println("DEBUG devicesHM = " + devicesHM);
			HashMap<ShopDevice, Integer> generatedDevicesHM = makeGenDevicesHM(
					generatedDevices, chartInSession);
			System.out.println("DEBUG generatedDevicesHM = "
					+ generatedDevicesHM);

			// TODO check if buy is possible
			if (!checkIfBuyIsPossible(componentsHM, devicesHM, generatedDevicesHM)) {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				System.out.println("DEBUG nekonzistentna kupovina!!!");
				return;
			}

			// make bill
			ShopBill aBill = new ShopBill(componentsHM, devicesHM,
					generatedDevicesHM, new ShopTax(), new Date(), loggedUser);
			ArrayList<ShopBill> shopBills = (ArrayList<ShopBill>) getServletContext()
					.getAttribute("shopBills");
			shopBills.add(aBill);
			getServletContext().setAttribute("shopBills", shopBills);

			// update shopStore
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");

			for (ShopComponent aComp : componentsHM.keySet()) {
				for (ShopComponent shopComp : shopComponents) {
					if (aComp.getName().trim()
							.equals(shopComp.getName().trim())) {
						if (shopComp.getAmount() - componentsHM.get(aComp) >= 0) {
							shopComp.setAmount(shopComp.getAmount()
									- componentsHM.get(aComp));
						} else {
							shopComp.setAmount(0);
						}
						break;
					}
				}
			}

			for (ShopDevice aDev : generatedDevicesHM.keySet()) {
				Integer units = generatedDevicesHM.get(aDev);
				for (ShopComponent aComp : aDev.getComponents()) {
					for (ShopComponent shopComp : shopComponents) {
						if (aComp.getName().trim()
								.equals(shopComp.getName().trim())) {
							if (shopComp.getAmount() - units >= 0) {
								shopComp.setAmount(shopComp.getAmount() - units);
							} else {
								shopComp.setAmount(0);
							}
							break;
						}
					}
				}
			}

			for (ShopDevice aDev : devicesHM.keySet()) {
				ShopDevice deviceToRemove = new ShopDevice();
				for (ShopDevice shopDev : shopDevices) {
					if (aDev.getName().trim().equals(shopDev.getName().trim())) {
						deviceToRemove = shopDev;
						break;
					}
				}
				shopDevices.remove(deviceToRemove);
			}

			getServletContext().setAttribute("shopComponents", shopComponents);
			getServletContext().setAttribute("shopDevices", shopDevices);
			// end update shopStore

			// clear shopingChart for logged user
			ShopChart cleanChart = new ShopChart();
			cleanChart.setUser(loggedUser);
			request.getSession().setAttribute("userChart", cleanChart);

		} else {
			// direct request
		}
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ajaxId = request.getParameter("ajaxId");
		String nameToDelete = request.getParameter("toDelete");
		System.out.println("DEBUG toDelete je " + nameToDelete);

		if (ajaxId.equals("1")) {
			// delete a device
			ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
					.getAttribute("shopDevices");
			for (ShopDevice aDevice : shopDevices) {
				if (nameToDelete.trim().equals(aDevice.getName().trim())) {
					shopDevices.remove(aDevice);
					break;
				}
			}
			getServletContext().setAttribute("shopDevices", shopDevices);

		} else if (ajaxId.equals("2")) {
			// delete a component
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			for (ShopComponent aComponent : shopComponents) {
				if (nameToDelete.trim().equals(aComponent.getName().trim())) {
					shopComponents.remove(aComponent);
					break;
				}
			}
			getServletContext().setAttribute("shopComponents", shopComponents);

		} else if (ajaxId.equals("3")) {
			// delete a category
			ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
					.getAttribute("shopCategories");
			for (ShopCategory aCategory : shopCategories) {
				if (nameToDelete.trim().equals(aCategory.getName().trim())) {
					shopCategories.remove(aCategory);
					break;
				}
			}
			getServletContext().setAttribute("shopCategories", shopCategories);

		} else if (ajaxId.equals("111")) {
			// delete device from shopChart
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			HashMap<ShopDevice, Integer> devicesInChart = userChart
					.getDevices();
			ShopDevice keyToBeRemoved = new ShopDevice();
			for (ShopDevice aDev : devicesInChart.keySet()) {
				if (nameToDelete.trim().equals(aDev.getName().trim())) {
					keyToBeRemoved = aDev;
				}
			}
			devicesInChart.remove(keyToBeRemoved);
			userChart.setDevices(devicesInChart);
			System.out.println("DEBUG nakon brisanja userChart.getDevices() = "
					+ userChart.getDevices());
			request.getSession().setAttribute("userChart", userChart);

			ShopChart userChartControl = (ShopChart) request.getSession()
					.getAttribute("userChart");
			System.out
					.println("DEBUG nakon brisanja userChartControl.getDevices() = "
							+ userChartControl.getDevices());

		} else if (ajaxId.equals("222")) {
			// delete component from shopChart
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			HashMap<ShopComponent, Integer> compsInChart = userChart
					.getComponents();
			ShopComponent keyToBeRemoved = new ShopComponent();
			for (ShopComponent aComp : compsInChart.keySet()) {
				if (nameToDelete.trim().equals(aComp.getName().trim())) {
					keyToBeRemoved = aComp;
				}
			}
			compsInChart.remove(keyToBeRemoved);
			userChart.setComponents(compsInChart);
			request.getSession().setAttribute("userChart", userChart);

		} else if (ajaxId.equals("333")) {
			// delete generatedDevice from shopChart
			ShopChart userChart = (ShopChart) request.getSession()
					.getAttribute("userChart");
			HashMap<ShopDevice, Integer> generatedDevicesInChart = userChart
					.getGeneratedDevices();
			ShopDevice keyToBeRemoved = new ShopDevice();
			for (ShopDevice aDev : generatedDevicesInChart.keySet()) {
				if (nameToDelete.trim().equals(aDev.getName().trim())) {
					keyToBeRemoved = aDev;
				}
			}
			generatedDevicesInChart.remove(keyToBeRemoved);
			userChart.setGeneratedDevices(generatedDevicesInChart);
			request.getSession().setAttribute("userChart", userChart);

		}
	}

	private ArrayList<ShopCategory> makeCategoryList(String categoryName) {
		ArrayList<ShopCategory> categories = new ArrayList<ShopCategory>();
		ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
				.getAttribute("shopCategories");
		for (ShopCategory aCategory : shopCategories) {
			if (aCategory.getName().trim().equals(categoryName.trim())) {
				categories.add(aCategory);
				break;
			}
		}
		return categories;
	}

	private ArrayList<ShopComponent> makeComponentsList(String componentNames) {
		ArrayList<ShopComponent> components = new ArrayList<ShopComponent>();
		if (!componentNames.equals("[]")) {
			ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
					.getAttribute("shopComponents");
			ArrayList<String> sComponents = new ArrayList<String>();

			String withoutBrackets = componentNames.substring(1,
					componentNames.length() - 1);
			String[] withoutCommas = withoutBrackets.split(",");
			for (String aString : withoutCommas) {
				String stringToAdd = aString.substring(1, aString.length() - 1);
				sComponents.add(stringToAdd);
			}

			for (String aName : sComponents) {
				for (ShopComponent aComponent : shopComponents) {
					if (aName.trim().equals(aComponent.getName().trim())) {
						components.add(aComponent);
					}
				}
			}

			return components;

		} else {
			return components;
		}
	}

	private ArrayList<ShopCategory> makeCategoriesList(String categoryNames) {
		ArrayList<ShopCategory> categories = new ArrayList<ShopCategory>();
		ArrayList<ShopCategory> shopCategories = (ArrayList<ShopCategory>) getServletContext()
				.getAttribute("shopCategories");
		ArrayList<String> sCategories = new ArrayList<String>();

		System.out.println("DEBUG u makeCategoriesList categoryNames = "
				+ categoryNames);
		if (!categoryNames.equals("[]")) {
			String withoutBrackets = categoryNames.substring(1,
					categoryNames.length() - 1);
			String[] withoutCommas = withoutBrackets.split(",");
			for (String aString : withoutCommas) {
				String stringToAdd = aString.substring(1, aString.length() - 1);
				sCategories.add(stringToAdd);
			}

			for (String aName : sCategories) {
				for (ShopCategory aCategory : shopCategories) {
					if (aName.trim().equals(aCategory.getName().trim())) {
						categories.add(aCategory);
					}
				}
			}

			return categories;
		} else {

			return categories;
		}
	}

	private String makeBase64FromFile(File file) {
		try {
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(file);
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			String imageDataString = encodeImage(imageData);

			// /**
			// * Internal server test for encoder and decoder
			// */
			//
			// // Converting a Base64 String into Image byte array
			// byte[] imageByteArray = decodeImage(imageDataString);
			//
			// // Write a image byte array into file system
			// FileOutputStream imageOutFile = new FileOutputStream(
			// "converted/water-after-convertBLA.jpg");
			// imageOutFile.write(imageByteArray);
			//
			// // Write a image base64 string into file system
			// PrintWriter imageFileWriter = new PrintWriter(
			// "converted/base64.txt");
			// imageFileWriter.write(imageDataString);

			imageInFile.close();
			// imageOutFile.close();

			return imageDataString;

		} catch (FileNotFoundException e) {
			System.out.println("Image not found " + e);
			return null;
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
			return null;
		}

	}

	/**
	 * Encodes the byte array into base64 string
	 *
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public static String encodeImage(byte[] imageByteArray) {
		// return Base64.encodeBase64URLSafeString(imageByteArray);
		return Base64.getEncoder().encodeToString(imageByteArray);
	}

	/**
	 * Decodes the base64 string into byte array
	 *
	 * @param imageDataString
	 *            - a {@link java.lang.String}
	 * @return byte array
	 */
	public static byte[] decodeImage(String imageDataString) {
		// return Base64.decodeBase64(imageDataString);
		return Base64.getDecoder().decode(imageDataString);
	}

	private void populateShop(ArrayList<ShopCategory> shopCategories,
			ArrayList<ShopComponent> shopComponents,
			ArrayList<ShopDevice> shopDevices) {

		// categories
		ArrayList<ShopCategory> processors = new ArrayList<ShopCategory>();
		ShopCategory aShopCategory0 = new ShopCategory("AMD", "AMD processors",
				new ArrayList<ShopCategory>());
		ShopCategory aShopCategory1 = new ShopCategory("Intel",
				"Intel processors", new ArrayList<ShopCategory>());
		processors.add(aShopCategory0);
		processors.add(aShopCategory1);
		ShopCategory aShopCategory2 = new ShopCategory("CPUs", "Processors",
				processors);
		shopCategories.add(aShopCategory0);
		shopCategories.add(aShopCategory1);
		shopCategories.add(aShopCategory2);

		ArrayList<ShopCategory> graphicCards = new ArrayList<ShopCategory>();
		ShopCategory aShopCategory3 = new ShopCategory("ASUS", "ASUS GPUs",
				new ArrayList<ShopCategory>());
		ShopCategory aShopCategory4 = new ShopCategory("NVIDIA", "NVIDIA GPUs",
				new ArrayList<ShopCategory>());
		graphicCards.add(aShopCategory3);
		graphicCards.add(aShopCategory4);
		ShopCategory aShopCategory5 = new ShopCategory("GPUs", "Graphic cards",
				graphicCards);
		shopCategories.add(aShopCategory3);
		shopCategories.add(aShopCategory4);
		shopCategories.add(aShopCategory5);
		// end categories

		// components
		ArrayList<ShopCategory> amdcpu = new ArrayList<ShopCategory>();
		amdcpu.add(aShopCategory2);
		amdcpu.add(aShopCategory0);
		ArrayList<ShopCategory> intelcpu = new ArrayList<ShopCategory>();
		intelcpu.add(aShopCategory2);
		intelcpu.add(aShopCategory1);
		ArrayList<ShopCategory> asusgpu = new ArrayList<ShopCategory>();
		asusgpu.add(aShopCategory5);
		asusgpu.add(aShopCategory3);
		ArrayList<ShopCategory> nvidiagpu = new ArrayList<ShopCategory>();
		nvidiagpu.add(aShopCategory5);
		nvidiagpu.add(aShopCategory4);

		ShopComponent aShopComponent0 = new ShopComponent(
				"CPU FM2 AMD Athlon X2 Dual Core 340 3.20GHz 32nm BOX",
				30.34,
				12,
				"processor",
				amdcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesor-cpu-fm2-amd-athlontm-x2-dual-core-340-3-20ghz-32nm-box-1168964.html",
				"uploads/AMDAthlon2.jpg");
		ShopComponent aShopComponent1 = new ShopComponent(
				"APU FM2 AMD A8 6600K 3.9GHz Radeon HD 8570D",
				105.5,
				4,
				"processor",
				amdcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesor-apu-fm2-amd-a8-6600k-3-9ghz-radeontm-hd-8570d-1173415.html",
				"uploads/AMDAPU.jpg");
		ShopComponent aShopComponent2 = new ShopComponent(
				"CPU FM2 AMD Athlon X4 Quad-Core 740 3.20GHz BOX ",
				73.0,
				17,
				"processor",
				amdcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesor-cpu-fm2-amd-athlontm-x4-quad-core-740-3-20ghz-box-32nm-1160071.html",
				"uploads/AmdAthlon.jpg");
		ShopComponent aShopComponent3 = new ShopComponent(
				"Procesori Intel CPU LGA1150 Intel Core i7-4770 3.40GHz BOX 22nm",
				321.0,
				10,
				"processor",
				intelcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesori-intel-cpu-lga1150-intelr-coretm-i7-4770-3-40ghz-box-22nm-1173420.html",
				"uploads/intelLGA1150.jpg");
		ShopComponent aShopComponent4 = new ShopComponent(
				"Procesori Intel CPU LGA2011 Intel Core i7-4930K 3.40GHz BOX 22nm",
				611.34,
				8,
				"processor",
				intelcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesori-intel-cpu-lga2011-intelr-coretm-i7-4930k-3-40ghz-box-22nm-1181257.html",
				"uploads/lga2011.jpg");
		ShopComponent aShopComponent5 = new ShopComponent(
				"Procesori Intel CPU LGA2011 Intel Core i7-4820K 3.70GHz BOX 22nm",
				341.45,
				19,
				"processor",
				intelcpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/procesori/procesori-intel-cpu-lga2011-intelr-coretm-i7-4820k-3-70ghz-box-22nm-1181009.html",
				"uploads/lga2011dva.jpg");
		ShopComponent aShopComponent6 = new ShopComponent(
				"Graficka kartica AMD Radeon R7 240 ASUS 4GB GDDR3 VGA/DVI/HDMI/128bit/R7240-OC-4GD3-L",
				127.8,
				9,
				"graphic card",
				asusgpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/graficke-kartice/graficka-kartica-amd-radeon-r7-240-asus-4gb-gddr3-vga-dvi-hdmi-128bit-r7240-oc-4gd3-l-4713132.html",
				"uploads/asus1.jpg");
		ShopComponent aShopComponent7 = new ShopComponent(
				"Graficka kartica AMD Radeon R9 270X ASUS OC 4GB GDDR5 DVI/HDMI/DP/256bit/R9270X-DC2T-4GD5",
				327.8,
				21,
				"graphic card",
				asusgpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/graficke-kartice/graficka-kartica-amd-radeon-r9-270x-asus-oc-4gb-gddr5-dvi-hdmi-dp-256bit-r9270x-dc2t-4gd5-1193255.html",
				"uploads/asus2.jpg");
		ShopComponent aShopComponent8 = new ShopComponent(
				"Graficka kartica GeForce GTX750 MSI OC 1GB DDR5 DVI/HDMI/DP/128bit/N750-1GD5/OC",
				140.8,
				20,
				"graphic card",
				nvidiagpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/graficke-kartice/graficka-kartica-geforce-gtx750-msi-oc-1gb-ddr5-dvi-hdmi-dp-128bit-n750-1gd5-oc-1188332.html",
				"uploads/nvidia1.jpg");
		ShopComponent aShopComponent9 = new ShopComponent(
				"Graficka kartica GeForce GTX770 MSI GAMING OC 2GB DDR5 DVI/DP/N770 TF 2GD5/OC",
				358.8,
				7,
				"graphic card",
				nvidiagpu,
				"",
				"http://www.winwin.rs/racunari-i-komponente/racunarske-komponente/graficke-kartice/graficka-kartica-geforce-gtx770-msi-gaming-oc-2gb-ddr5-dvi-dp-n770-tf-2gd5-oc-1172192.html",
				"uploads/nvidia2.jpg");
		shopComponents.add(aShopComponent0);
		shopComponents.add(aShopComponent1);
		shopComponents.add(aShopComponent2);
		shopComponents.add(aShopComponent3);
		shopComponents.add(aShopComponent4);
		shopComponents.add(aShopComponent5);
		shopComponents.add(aShopComponent6);
		shopComponents.add(aShopComponent7);
		shopComponents.add(aShopComponent8);
		shopComponents.add(aShopComponent9);
		// end components

		// devices
		ArrayList<ShopComponent> del5547 = new ArrayList<ShopComponent>();
		del5547.add(aShopComponent3);
		del5547.add(aShopComponent8);
		ArrayList<ShopComponent> hpprobook = new ArrayList<ShopComponent>();
		hpprobook.add(aShopComponent4);
		hpprobook.add(aShopComponent9);
		ArrayList<ShopComponent> hp250 = new ArrayList<ShopComponent>();
		hp250.add(aShopComponent1);
		hp250.add(aShopComponent7);
		ArrayList<ShopComponent> lenovo = new ArrayList<ShopComponent>();
		lenovo.add(aShopComponent3);
		lenovo.add(aShopComponent6);

		ShopDevice aShopDevice0 = new ShopDevice("Dell Inspiron 5547",
				"Cheep laptop", del5547);
		shopDevices.add(aShopDevice0);
		ShopDevice aShopDevice1 = new ShopDevice("HP ProBook 4530s",
				"Laptop for standard users", hpprobook);
		shopDevices.add(aShopDevice1);
		ShopDevice aShopDevice2 = new ShopDevice("HP 250",
				"Computer for students", hp250);
		shopDevices.add(aShopDevice2);
		ShopDevice aShopDevice3 = new ShopDevice("Lenovo IdeaPad B590",
				"Solid machine for standard users", lenovo);
		shopDevices.add(aShopDevice3);
		ShopDevice aShopDevice4 = new ShopDevice("Lenovo Yoga11s",
				"Advanced machine for standard users",
				new ArrayList<ShopComponent>());
		shopDevices.add(aShopDevice4);
		ShopDevice aShopDevice5 = new ShopDevice("Toshiba Satelite C50",
				"Great performance and endurance",
				new ArrayList<ShopComponent>());
		shopDevices.add(aShopDevice5);
		ShopDevice aShopDevice6 = new ShopDevice("Toshiba Satelite L35",
				"Good processor performace and stable configurations",
				new ArrayList<ShopComponent>());
		shopDevices.add(aShopDevice6);
		ShopDevice aShopDevice7 = new ShopDevice("Acer Aspire L1",
				"Stable configurations", new ArrayList<ShopComponent>());
		shopDevices.add(aShopDevice7);
		ShopDevice aShopDevice8 = new ShopDevice("Acer Aspire 10SW5",
				"Gamer laptop", new ArrayList<ShopComponent>());
		shopDevices.add(aShopDevice8);
		// end devices

	}

	private HashMap<ShopComponent, Integer> makeComponentsHM(String components) {
		System.out.println("DEBUG u makeHM components = " + components);
		ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
				.getAttribute("shopComponents");
		HashMap<ShopComponent, Integer> componentsHM = new HashMap<ShopComponent, Integer>();
		if (!components.equals("{}")) {
			String withoutBrackets = components.substring(1,
					components.length() - 1);
			String[] withoutCommas = withoutBrackets.split(",");
			for (String item : withoutCommas) {
				String[] parts = item.split("\":\"");
				String compName = parts[0].substring(1, parts[0].length());
				String compAmount = parts[1]
						.substring(0, parts[1].length() - 1);
				System.out.println("DEBUG compamount = " + compAmount);
				ShopComponent compToAdd = new ShopComponent();
				Integer amount = Integer.parseInt(compAmount);
				for (ShopComponent aComp : shopComponents) {
					if (compName.trim().equals(aComp.getName().trim())) {
						compToAdd = aComp;
						break;
					}
				}
				componentsHM.put(compToAdd, amount);
			}
		}

		return componentsHM;
	}

	private HashMap<ShopDevice, Integer> makeDevicesHM(String devices) {
		ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
				.getAttribute("shopDevices");
		HashMap<ShopDevice, Integer> devicesHM = new HashMap<ShopDevice, Integer>();
		if (!devices.equals("{}")) {
			String withoutBrackets = devices.substring(1, devices.length() - 1);
			String[] withoutCommas = withoutBrackets.split(",");
			for (String item : withoutCommas) {
				String[] parts = item.split("\":\"");
				String devName = parts[0].substring(1, parts[0].length());
				String devAmount = parts[1].substring(0, parts[1].length() - 1);
				ShopDevice devToAdd = new ShopDevice();
				Integer amount = Integer.parseInt(devAmount);
				for (ShopDevice aDev : shopDevices) {
					if (devName.trim().equals(aDev.getName().trim())) {
						devToAdd = aDev;
						break;
					}
				}
				devicesHM.put(devToAdd, amount);
			}
		}

		return devicesHM;
	}

	private HashMap<ShopDevice, Integer> makeGenDevicesHM(String devices,
			ShopChart chartInSession) {
		ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
				.getAttribute("shopDevices");
		System.out.println("DEBUG devices koji su gendevs = " + devices);
		HashMap<ShopDevice, Integer> devicesHM = new HashMap<ShopDevice, Integer>();
		if (!devices.equals("{}")) {
			String withoutBrackets = devices.substring(1, devices.length() - 1);
			String[] withoutCommas = withoutBrackets.split(",");
			for (String item : withoutCommas) {
				String[] parts = item.split("\":\"");
				String devName = parts[0].substring(1, parts[0].length());
				String devAmount = parts[1].substring(0, parts[1].length() - 1);
				System.out.println("DEBUG makeGenDevicesHM p0 = " + parts[0]
						+ "\np1 = " + parts[1]);
				ShopDevice devToAdd = new ShopDevice();
				Integer amount = Integer.parseInt(devAmount);
				System.out.println("DEBUG devname u MAKEGENDEV = " + devName);
				System.out.println("DEBUG amount u MAKEGENDEV = " + amount);
				for (ShopDevice aDevice : chartInSession.getGeneratedDevices()
						.keySet()) {
					if (devName.trim().equals(aDevice.getName().trim())) {
						devToAdd = aDevice;
						break;
					}
				}
				devicesHM.put(devToAdd, amount);
			}
		}

		return devicesHM;
	}

	private boolean checkIfBuyIsPossible(
			HashMap<ShopComponent, Integer> componentsHM,
			HashMap<ShopDevice, Integer> devicesHM,
			HashMap<ShopDevice, Integer> generatedDevicesHM) {
		/*
		 * Check if buying query can be executed on correct way from aspect of data consistency 
		 */
		boolean isPossible = false;
		
		//update components and devices amount in appropriate hash maps
		ArrayList<ShopDevice> shopDevices = (ArrayList<ShopDevice>) getServletContext()
				.getAttribute("shopDevices");
		ArrayList<ShopComponent> shopComponents = (ArrayList<ShopComponent>) getServletContext()
				.getAttribute("shopComponents");
		
		for (ShopDevice aDev : generatedDevicesHM.keySet()) {
			for (ShopComponent compInDev : aDev.getComponents()) {
				for (ShopComponent aComp : componentsHM.keySet()) {
					if (aComp.getName().trim().equals(compInDev.getName().trim())) {
						ShopComponent compForExtractAmount = new ShopComponent(); // comp with its newer amount on store
						for (ShopComponent compForAmount : shopComponents) {
							if (compForAmount.getName().trim().equals(aComp.getName().trim())) {
								compForExtractAmount = compForAmount;
							}
						}
						System.out.println("DEBUG radim za comp : " + aComp.getName() + "\n i device : " + aDev.getName());
						System.out.println("DEBUG zahtevam\n" + componentsHM.get(aComp) + " komponenti \n" + generatedDevicesHM.get(aDev) + " devajsa");
						System.out.println("DEBUG stanje " + compForExtractAmount.getName() + " je \n" + compForExtractAmount.getAmount());
						if (componentsHM.get(aComp) + generatedDevicesHM.get(aDev) > compForExtractAmount.getAmount()) {
							isPossible = false;
							return isPossible;
						}
					}
				}
			}
		}
		
		return true;
	}
	


}
